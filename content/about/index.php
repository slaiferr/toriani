<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Toriani занимается прямыми поставками и продажей мебели из Италии по стоимости евро-прайсов.");
$APPLICATION->SetPageProperty("keywords", "О компании Toriani, О компании");
$APPLICATION->SetPageProperty("ROBOTS", "index, follow");
$APPLICATION->SetTitle("О компании Toriani - лучшем продавце элитной мебели");
?><p style="text-align: justify;">
 <span style="font-size: 11pt;">Компания Toriani занимается прямыми поставками и продажей мебели из Италии по стоимости евро-прайсов. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;">
	В каталоге нашего интернет-магазина собраны более </span><span style="font-size: 11pt;">3</span><span style="font-size: 11pt;">0000 наименований от ведущих мебельных фабрик от среднего до премиального уровня. </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;">
	На заказ стоимостью от 100 000 руб. действует </span><span style="color: #ff0000;"><b><span style="font-size: 11pt;">скидка до 10%</span></b></span><span style="font-size: 11pt;"> от стоимости прайса в зависимости от фабрики.</span>
</p>
<p style="text-align: justify;">
 <span style="font-size: 11pt;">Для быстрого поиска необходимого предмета интерьера Вы можете воспользоваться фильтром поиска по параметрам в каждой из категорий товаров или оставить заявку на подбор менеджеру. Если Вы ищете известный Вам артикул, то строка "поиск товара" в верхней части сайта поможет найти нужный товар.</span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;">
	Компания имеет собственное производство по изготовлению <a href="/catalog/italyanskie-kuhni/" title="Итальянские кухни">кухонь</a>. В производстве используются </span><span style="font-size: 11pt; background-color: #ffffff;">итальянские фасады, немецкое экологически чистое ЛДСП Egger и фурнитура австрийской фирмы BLUM - лидера мирового уровня.</span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;">
	В 2015 году мы открыли <a href="/catalog/autlet-rasprodazha-mebeli-skidki/" title="Распродажа итальянской мебели">мебельный аутлет</a>, где представлены выставочные образцы мебельных салонов Санкт-Петербурга и Москвы со скидками до 70%. </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;">
	Мы сотрудничаем с </span><span style="font-size: 11pt; background-color: #ffffff;">дизайнерами и архитекторами Санкт-Петербурга, которые помогут Вам составить дизайн-проект или проведут консультацию.</span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt; background-color: #ffffff;">Также наши специалисты помогут в профессиональной перевозке, сборке, реставрации или перетяжки Вашей мебели.</span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="background-color: #ffffff;"><br>
 <span style="font-size: 11pt;"> </span></span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><br>
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><br>
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p>
 <br>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>