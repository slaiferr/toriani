<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты мебельного салона Ториани");
$APPLICATION->SetPageProperty("keywords", "контакты");
$APPLICATION->SetPageProperty("description", "Добро пожаловать в наш салон в Приморском районе по адресу Лахтинский проспект 85. Здесь вы можете увидеть выставочные образцы, ознакомиться с каталогами и получить консультацию специалиста.");
$APPLICATION->SetPageProperty("ROBOTS", "index, follow");
$APPLICATION->SetTitle("Контакты");?>
	<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "page", 
        "AREA_FILE_SUFFIX" => "contacts-content", 
        "AREA_FILE_RECURSIVE" => "N", 
        "EDIT_TEMPLATE" => "standard.php" 
    )
);?>
<h2>Схема проезда</h2>
 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:59.990898999955014;s:10:\"yandex_lon\";d:30.205779999999983;s:12:\"yandex_scale\";i:13;s:10:\"PLACEMARKS\";a:2:{i:0;a:3:{s:3:\"LON\";d:30.248959025395;s:3:\"LAT\";d:59.92231462734;s:4:\"TEXT\";s:84:\"Салон мебели Toriani###RN###В.О., Кожевенная линия д.30\";}i:1;a:3:{s:3:\"LON\";d:30.14599892547547;s:3:\"LAT\";d:59.994521509161686;s:4:\"TEXT\";s:82:\"Лахтинский пр. 85 лит.В, ТК \"Гарден Сити\" (2 этаж)\";}}}",
		"MAP_WIDTH" => "100%",
		"MAP_HEIGHT" => "305",
		"CONTROLS" => array(
			0 => "ZOOM",
			1 => "TYPECONTROL",
			2 => "SCALELINE",
		),
		"OPTIONS" => array(
			0 => "ENABLE_DBLCLICK_ZOOM",
			1 => "ENABLE_DRAGGING",
		),
		"MAP_ID" => "1",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?> <br>
<h2>Форма обратной связи</h2>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback",
	".default",
	Array(
		"USE_CAPTCHA" => "Y",
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"EMAIL_TO" => "info@toriani.ru, n.shibaeva@raz-vitie.ru",
		"REQUIRED_FIELDS" => array(0=>"NAME",1=>"EMAIL",2=>"MESSAGE",),
		"EVENT_MESSAGE_ID" => array()
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>