<?php

function downloadJs($file_url, $save_to)
{
    $content = file_get_contents($file_url);
    file_put_contents($save_to, $content);
}

downloadJs('https://cdn.retailrocket.ru/content/javascript/api.js', __DIR__ . '/RetailRocketapi.js');

downloadJs('https://www.googletagmanager.com/gtag/js?id=UA-121966545-1', __DIR__ . '/Gtag.js');

downloadJs('https://www.google-analytics.com/analytics.js', __DIR__ . '/analytics.js');

downloadJs('https://mc.yandex.ru/metrika/tag.js', __DIR__ . '/ytag.js');

downloadJs('https://mc.yandex.ru/metrika/watch.js', __DIR__ . '/watch.js');

downloadJs('https://counter.rambler.ru/top100.jcn?4412973', __DIR__ . '/top100jcn.js');

?>