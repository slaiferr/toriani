<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
$this->addExternalCss("/bitrix/css/main/font-awesome.css");
$this->addExternalCss($this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css');
$this->addExternalJS($this->GetFolder().'/partners.js');
?>
<div class="bx-newslist">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<div class="row">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="bx-newslist-container col-lg-12" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<div class="bx-newslist-block">
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<h3 class="bx-newslist-title">
				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
				<?else:?>
					<?echo $arItem["NAME"]?>
				<?endif;?>
			</h3>
		<?endif;?>
		<div class="row">
		<?if (is_array($arItem["PREVIEW_PICTURE"])):?>
			<div class="bx-newslist-img col-lg-2 col-xs-12">
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<? $arFileTmp = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            array("width" => 250, "height" => 250),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );?>

				<img
					src="<?=$arFileTmp["src"];?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					/>
			<?endif;?>
			</div>
		<?endif;?>
		<? if (is_array($arItem["DISPLAY_PROPERTIES"]['SHORT_TEXT'])) {?>
			<div class="bx-newslist-shorttext col-lg-10 col-xs-12">
				<?=$arItem["DISPLAY_PROPERTIES"]['SHORT_TEXT']['~VALUE']['TEXT'];?>
			</div>
		<? }; ?>
		</div>
		<div class="row">
		<? if (is_array($arItem["DISPLAY_PROPERTIES"]['MAIN_TEXT'])) {?>
			<div class="bx-newslist-maintext col-lg-12">
				<?=$arItem["DISPLAY_PROPERTIES"]['MAIN_TEXT']['~VALUE']['TEXT'];?>
			</div>
		<? }; ?>
		</div>
		<div class="row">
			<div class="bx-newslist-gallery">
			<? if ($arItem["DISPLAY_PROPERTIES"]["PARTNERS_GALLERY"]) {?>
				<? foreach ($arItem["DISPLAY_PROPERTIES"]["PARTNERS_GALLERY"]["VALUE"] as $key => $value) {
						$arFileTmp = CFile::ResizeImageGet($value, array("width" => 250, "height" => 250), BX_RESIZE_IMAGE_EXACT, true);?>
					<div class="bx-newslist-gallery-item col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<a rel="lightbox" href="<?=CFile::GetPath($value)?>">
							<img class="img-responsive" width="<?=$arFileTmp['width']?>" height="<?=$arFileTmp['height']?>" src="<?=$arFileTmp['src'];?>" alt="">
						</a>
					</div>
				<? } ?>
			<?}?>
			</div>
		</div>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<!-- <pre>
			<?
				var_dump($arProperty);
			?>
			</pre> -->
		<?endforeach;?>
		<!-- <div class="row">
			<div class="col-xs-5">
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<div class="bx-newslist-more"><a class="btn btn-primary btn-xs" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo GetMessage("CT_BNL_GOTO_DETAIL")?></a></div>
			<?endif;?>
			</div>
		<?
		if ($arParams["USE_SHARE"] == "Y") { ?>
			<div class="col-xs-7 text-right">
				<noindex>
				<?
				$APPLICATION->IncludeComponent("bitrix:main.share", $arParams["SHARE_TEMPLATE"], array(
						"HANDLERS" => $arParams["SHARE_HANDLERS"],
						"PAGE_URL" => $arItem["~DETAIL_PAGE_URL"],
						"PAGE_TITLE" => $arItem["~NAME"],
						"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"HIDE" => $arParams["SHARE_HIDE"],
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);
				?>
				</noindex>
			</div>
			<? } ?>
		</div> -->
	</div>
	</div>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
