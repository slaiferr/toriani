<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"EMAIL_TO" => Array(
			"NAME" => GetMessage("MFP_EMAIL_TO"), 
			"TYPE" => "STRING",
			"DEFAULT" => htmlspecialchars(COption::GetOptionString("main", "email_from")), 
			"PARENT" => "BASE",
		),
		"REQUIRED_FIELDS" => Array(
			"NAME" => GetMessage("MFP_REQUIRED_FIELDS"), 
			"TYPE"=>"LIST", 
			"MULTIPLE"=>"Y", 
			"VALUES" => Array(
				"FIRST_NAME" => GetMessage("MFP_FIRST_NAME"), 
				"PHONE" => GetMessage("MFP_PHONE"), 
				"EMAIL" => GetMessage("MFP_EMAIL"),
				"COMPANY" => GetMessage("MFP_COMPANY"),
				"APPLICATION" => GetMessage("MFP_FILE_APPLICATION"),
				"INN" => GetMessage("MFP_INN"),
				"COMMENT" => GetMessage("MFP_COMMENT"),
			),
			"DEFAULT"=>"",
			"COLS"=>25,
			"PARENT" => "BASE",
		),
	)
);
?>