<?
$MESS ['MFT_FIRST_NAME'] = "Имя";
$MESS ['MFT_PHONE'] = "Телефон";
$MESS ['MFT_EMAIL'] = "Ссылка";
$MESS ['MFT_COMPANY'] = "Название компании";
$MESS ['MFT_FILE_APPLICATION'] = "Файл с заявкой";
$MESS ['MFT_INN'] = "ИНН";
$MESS ['MFT_COMMENT'] = "Комментарий";
$MESS ['MFT_CAPTCHA'] = "Код с картинки";
$MESS ['MFT_SEND'] = "Отправить";

$MESS ['FIRST_NAME_NOT_FILLED'] = "Не заполнено поле «Имя»";
$MESS ['PHONE_NOT_FILLED'] = "Не заполнено поле «Телефон»";
$MESS ['EMAIL_NOT_FILLED'] = "Не заполнено поле «Ссылка»";
$MESS ['COMPANY_NOT_FILLED'] = "Не заполнено поле «Название компании»";
$MESS ['FILE_APPLICATION_NOT_FILLED'] = "Не заполнено поле «Файл с заявкой»";
$MESS ['INN_NOT_FILLED'] = "Не заполнено поле «ИНН»";
$MESS ['COMMENT_NOT_FILLED'] = "Не заполнено поле «Комментарий»";
$MESS ['WRONG_CAPTCHA'] = "Неверно введен «Код с картинки»";

$MESS ['MF_MESSAGE_INFO'] = "Информационное сообщение сайта";
$MESS ['MF_MESSAGE_ZAKAZ'] = "Оставлена заявка";
$MESS ['MF_MESSAGE_FIRST_NAME'] = "Имя:";
$MESS ['MF_MESSAGE_PHONE'] = "Телефон:";
$MESS ['MF_MESSAGE_EMAIL'] = "Ссылка:";
$MESS ['MF_MESSAGE_COMPANY'] = "Название компании:";
$MESS ['MF_MESSAGE_INN'] = "ИНН:";
$MESS ['MF_MESSAGE_COMMENT'] = "Комментарий:";
$MESS ['MF_MESSAGE_GENERAT'] = "Сообщение сгенерировано автоматически.";
$MESS ['MF_MESSAGE_CALLBACK_ZAKAZ'] = "Заказ обратного звонка";
$MESS ['MF_OK_MESSAGE'] = "Спасибо, ваше сообщение принято!";



?>