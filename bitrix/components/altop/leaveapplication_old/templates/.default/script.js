function button_callback(path, template, email_to, required) {
    $.ajax({
        type: "POST",
        url : path+'/component.php',
        data: ({
            FIRST_NAME		: $("#callback_first_name").val(),
            PHONE			: $("#callback_phone").val(),
			EMAIL			: $("#callback_email").val(),
			COMPANY			: $("#callback_company").val(),
			INN				: $("#callback_inn").val(),
			COMMENT			: $("#callback_comment").val(),
NAME: $("#callback_name").val(),
TEL: $("#callback_tel").val(),
TIME: $("#callback_time").val(),
QUESTION: $("#callback_question").val(),
			captcha_word	: $("#callback_captcha_word").val(),
            captcha_sid		: $("#callback_captcha_sid").val(),
			METHOD			: $("#callback_method").val(),
			PATH			: path,
            TEMPLATE		: template,
			EMAIL_TO		: email_to,
			REQUIRED		: required
        }),
        success: function (html) {
            $('#echo_callback_form').html(html);
        }
    });
}

$(function() {

  $('.rf').each(function(){
	// Объявляем переменные (форма и кнопка отправки)
	var form = $(this),
		btn = form.find('.btn_buy.popdef');

	// Добавляем каждому проверяемому полю, указание что поле пустое
	form.find('.rfield').addClass('empty_field');

	// Функция проверки полей формы
	function checkInput(){
	  form.find('.rfield').each(function(){
		if($(this).val() != ''){
		  // Если поле не пустое удаляем класс-указание
		$(this).removeClass('empty_field');
		} else {
		  // Если поле пустое добавляем класс-указание
		$(this).addClass('empty_field');
		}
	  });
	}

	// Функция подсветки незаполненных полей
	function lightEmpty(){
	  form.find('.empty_field').css({'border-color':'#d8512d'});
	  // Через полсекунды удаляем подсветку
	  setTimeout(function(){
		form.find('.empty_field').removeAttr('style');
	  },500);
	}

	// Проверка в режиме реального времени
	setInterval(function(){
	  // Запускаем функцию проверки полей на заполненность
	  checkInput();
	  // Считаем к-во незаполненных полей
	  var sizeEmpty = form.find('.empty_field').size();
	  // Вешаем условие-тригер на кнопку отправки формы
	  if(sizeEmpty > 0){
		if(btn.hasClass('disabled')){
		  return false
		} else {
		  btn.addClass('disabled')
		}
	  } else {
		btn.removeClass('disabled')
	  }
	},500);

	// Событие клика по кнопке отправить
	btn.click(function(){
	  if($(this).hasClass('disabled')){
		// подсвечиваем незаполненные поля и форму не отправляем, если есть незаполненные поля
		lightEmpty();
		return false
	  } else {
		// Все хорошо, все заполнено, отправляем форму
		form.submit();
	  }
	});
  });
});