<?
$MESS ['MFP_EMAIL_TO'] = "Кому";
$MESS ['MFP_REQUIRED_FIELDS'] = "Обязательные для заполнения";
$MESS ['MFP_FIRST_NAME'] = "Имя";
$MESS ['MFP_PHONE'] = "Телефон";
$MESS ['MFP_EMAIL'] = "E-mail";
$MESS ['MFP_COMPANY'] = "Название компании";
$MESS ['MFP_FILE_APPLICATION'] = "Файл с заявкой";
$MESS ['MFP_INN'] = "ИНН";
$MESS ['MFP_COMMENT'] = "Комментарий";
?>