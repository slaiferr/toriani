<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("ALTOP_LEAVEAPPLICATION_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("ALTOP_LEAVEAPPLICATION_COMPONENT_DESCR"),
	"ICON" => "/images/leaveapplication.gif",
	"PATH" => array(
		"ID" => "altop_tools",
		"NAME" => GetMessage("ALTOP_TOOLS")
	),
);
?>