<?
function trash_leaveapplication($message) {
    $message = trim ( $message );
    $message = strip_tags ( $message );
    $message = htmlspecialchars ( $message, ENT_QUOTES );
    return $message;
}

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && htmlspecialchars($_POST['METHOD']) == 'leaveapplication') {

	// ���������� API ��� ���������� �����
	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

	// ���������� �������� ����
	require_once($_SERVER['DOCUMENT_ROOT'].htmlspecialchars($_POST["TEMPLATE"])."/lang/ru/template.php");
		
    $error = '';

	$REQUIRED = array();
	$REQUIRED = explode("/", $_POST["REQUIRED"]);	

	// �������� ������������� �����
	if(empty($REQUIRED) || in_array("FIRST_NAME", $REQUIRED)):
		if (!isset($_POST['FIRST_NAME']) || !strlen($_POST['FIRST_NAME'])) {
			$error .= GetMessage('FIRST_NAME_NOT_FILLED').' <br>';
			$return = true;
		}
	endif;

	// �������� ������������� ��������
	if(empty($REQUIRED) || in_array("PHONE", $REQUIRED)):
		if (!isset($_POST['PHONE']) || !strlen($_POST['PHONE'])) {
			$error .= GetMessage('PHONE_NOT_FILLED').' <br>';
			$return = true;
		}
	endif;
	
	// �������� ������������� EMAIL
	if(empty($REQUIRED) || in_array("EMAIL", $REQUIRED)):
		if (!isset($_POST['EMAIL']) || !strlen($_POST['EMAIL'])) {
			$error .= GetMessage('EMAIL_NOT_FILLED').' <br>';
			$return = true;
		}
	endif;
	
	// �������� ������������� COMPANY
	if(empty($REQUIRED) || in_array("COMPANY", $REQUIRED)):
		if (!isset($_POST['COMPANY']) || !strlen($_POST['COMPANY'])) {
			$error .= GetMessage('COMPANY_NOT_FILLED').' <br>';
			$return = true;
		}
	endif;
	
	// �������� ������������� FILE
	if(empty($REQUIRED) || in_array("FILE_APPLICATION", $REQUIRED)):
		if (!isset($_POST['FILE_APPLICATION']) || !strlen($_FILE['FILE_APPLICATION'])) {
			$error .= GetMessage('FILE_APPLICATION_NOT_FILLED').' <br>';
			$return = true;
		}
	endif;
	
	// �������� ������������� ���
	if(empty($REQUIRED) || in_array("INN", $REQUIRED)):
		if (!isset($_POST['INN']) || !strlen($_FILE['INN'])) {
			$error .= GetMessage('INN_NOT_FILLED').' <br>';
			$return = true;
		}
	endif;
	
	// �������� ������������� COMMENT
	if(empty($REQUIRED) || in_array("COMMENT", $REQUIRED)):
		if (!isset($_POST['COMMENT']) || !strlen($_FILE['COMMENT'])) {
			$error .= GetMessage('COMMENT_NOT_FILLED').' <br>';
			$return = true;
		}
	endif;

	if(!$USER->IsAuthorized()) {
		
		// �������� �������� ��������� �����
		echo '<script>$("#leaveapplication_captcha_word").attr("value", "");</script>';
		
		if(!$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_sid"])) {
	        $error .= GetMessage('WRONG_CAPTCHA').' <br>';
	        $return = true;
	    }
	}

	// ���� ���� ������, �� ������ ����� ������
    if($return == true) {
		// ��������� �����
	    if(!$USER->IsAuthorized()) {
    		$cCode = $APPLICATION->CaptchaGetCode();
			echo '<script>$("#leaveapplication_cImg").attr("src","/bitrix/tools/captcha.php?captcha_sid='.$cCode.'");$("#leaveapplication_captcha_sid").val("'.$cCode.'");</script>';
		}
		echo '<span class="alertMsg bad">'.$error.'</span>';
        return;
    }

	// � ������ ������, ����������� ���� ��������� ���� ��������
    $_POST['NAME']			= iconv("UTF-8", SITE_CHARSET, trash_leaveapplication($_POST['NAME']));
    $_POST['TEL']			= iconv("UTF-8", SITE_CHARSET, trash_leaveapplication($_POST['TEL']));
	$_POST['TIME']			= iconv("UTF-8", SITE_CHARSET, trash_leaveapplication($_POST['TIME']));
	$_POST['QUESTION']		= iconv("UTF-8", SITE_CHARSET, trash_leaveapplication($_POST['QUESTION']));
	
	
	//�������� ������
	$headers = "From: ".$_POST["EMAIL_TO"]."\r\n";
	$headers .= "Content-type: text/plain; charset=KOI8-R\r\n";
	$headers .= "Mime-Version: 1.0\r\n";

	$title = SITE_SERVER_NAME.": ".GetMessage('MF_MESSAGE_LEAVEAPPLICATION_ZAKAZ');

	$message = GetMessage("MF_MESSAGE_INFO")." ".SITE_SERVER_NAME."\r\n";
	$message.= "------------------------------------------\r\n";
	$message.= GetMessage("MF_MESSAGE_ZAKAZ")."\n";
	$message.= GetMessage("MF_MESSAGE_NAME")." ".$_POST['NAME']."\r\n";
	$message.= GetMessage("MF_MESSAGE_TEL")." ".$_POST['TEL']."\r\n";
	$message.= GetMessage("MF_MESSAGE_TIME")." ".$_POST['TIME']."\r\n";
	$message.= GetMessage("MF_MESSAGE_QUESTION")." ".$_POST['QUESTION']."\r\n";
	$message.= GetMessage("MF_MESSAGE_GENERAT")."\r\n";

	if(mail($_POST["EMAIL_TO"], iconv(SITE_CHARSET, 'KOI8-R', $title), iconv(SITE_CHARSET, 'KOI8-R', $message), $headers)) {
		echo '<span class="alertMsg good">'.GetMessage("MF_OK_MESSAGE").'</span>';
	}
	
} elseif(!$_SERVER['HTTP_X_REQUESTED_WITH']) {
	
	$arResult = array();

	if(!$USER->IsAuthorized()) {
		$arResult["CAPTCHA_CODE"] = htmlspecialchars($APPLICATION->CaptchaGetCode());
	}
	
	$arResult["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
	if(strlen($arParams["EMAIL_TO"]) <= 0)
		$arResult["EMAIL_TO"] = COption::GetOptionString("main", "email_from");

	$arResult["REQUIRED"] = implode("/", $arParams["REQUIRED_FIELDS"]);

	if($USER->IsAuthorized())
		$_POST['NAME'] = htmlspecialcharsEx($USER->GetFullName());
	
	$this->IncludeComponentTemplate();
}
?>