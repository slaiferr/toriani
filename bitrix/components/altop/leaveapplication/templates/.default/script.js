function button_leaveapplication(path, template, email_to, required, element_id) {
    $.ajax({
        type: "POST",
        url : path+'/component.php',
        data: ({
            NAME			: $("#leaveapplication_name_" + element_id).val(),            
			TEL				: $("#leaveapplication_tel_" + element_id).val(),
			TIME			: $("#leaveapplication_time_" + element_id).val(),
			MESSAGE			: $("#leaveapplication_message_" + element_id).val(),
			captcha_word	: $("#leaveapplication_captcha_word_" + element_id).val(),
            captcha_sid		: $("#leaveapplication_captcha_sid_" + element_id).val(),
			METHOD			: $("#leaveapplication_method_" + element_id).val(),
			EMAIL			: $("#leaveapplication_email_" + element_id).val(),
			PATH			: path,
            TEMPLATE		: template,
			EMAIL_TO		: email_to,
			REQUIRED		: required,
			ELEMENT_ID		: element_id
        }),
        success: function (html) {
            $('#echo_leaveapplication_form_' + element_id).html(html);
        }
    });
}