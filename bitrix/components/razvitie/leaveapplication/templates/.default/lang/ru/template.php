<?
$MESS ['MFT_ASK_PRICE_TITLE'] = "Хотите дешевле?";
$MESS ['MFT_ASK_PRICE_BUTTON'] = "Отправить";

$MESS ['MFT_ASK_PRICE_NAME'] = "Имя";
$MESS ['MFT_ASK_PRICE_TEL'] = "Телефон";
$MESS ['MFT_ASK_PRICE_EMAIL'] = "Email";
$MESS ['MFT_ASK_PRICE_MESSAGE'] = "Ссылка на товар";
$MESS ['MFT_ASK_PRICE_CAPTCHA'] = "Код с картинки";

$MESS ['NAME_NOT_FILLED'] = "Не заполнено поле «Имя»";
$MESS ['TEL_NOT_FILLED'] = "Не заполнено поле «Телефон»";
$MESS ['EMAIL_NOT_FILLED'] = "Не заполнено поле «Email»";
$MESS ['MESSAGE_NOT_FILLED'] = "Не заполнено поле «Ссылка на товар»";
$MESS ['WRONG_CAPTCHA'] = "Неверно введен «Код с картинки»";

$MESS ['MF_MESSAGE_TITLE'] = "Хотите дешевле?";
$MESS ['MF_MESSAGE_INFO'] = "Информационное сообщение сайта";
$MESS ['MF_MESSAGE_ZAKAZ'] = "Осуществлен запрос цены";
$MESS ['MF_MESSAGE_NAME'] = "Имя:";
$MESS ['MF_MESSAGE_TEL'] = "Телефон:";
$MESS ['MF_MESSAGE_EMAIL'] = "Email:";
$MESS ['MF_MESSAGE_MESSAGE'] = "Ссылка на товар:";
$MESS ['MF_MESSAGE_GENERAT'] = "Сообщение сгенерировано автоматически.";
$MESS ['MF_OK_MESSAGE'] = "Спасибо, ваше сообщение принято!";

$MESS ['DOP'] = "Укажите ссылку на страницу, где вы нашли товар дешевле";
?>