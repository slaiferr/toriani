<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
	<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="yandex-verification" content="7d2d3b82018b7630" />
	<meta property="og:image" content="https://toriani.ru/logo_toriani.png" />
	<meta name="yandex-verification" content="538a2cb247f71f4a" />
	<meta name="google-site-verification" content="9ISuu0BjbLZEXFt_TrHSMsqdPROcbuP8k5h_pu0Jw58" />
	<meta name='wmail-verification' content='c7f16a41cdc5267d5311fa2d95a501d8' />
	<meta name="mailru-verification" content="83862f017a93f626" />
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->SetAdditionalCSS("https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");	
	$APPLICATION->SetAdditionalCSS("https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic-ext");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css");	
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/anythingslider/slider.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/custom-forms/custom-forms.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox-1.3.1.css");	
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/spectrum/spectrum.css");	
	CJSCore::Init(array("jquery"));	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.scrollUp.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/moremenu.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/anythingslider/jquery.easing.1.2.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/anythingslider/jquery.anythingslider.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/custom-forms/jquery.custom-forms.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox-1.3.1.pack.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/spectrum/spectrum.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/countUp.min.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/countdown/jquery.plugin.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/countdown/jquery.countdown.js");	
	$APPLICATION->AddHeadString("
		<script type='text/javascript'>
			$(function() {
				$.countdown.regionalOptions['ru'] = {
					labels: ['".GetMessage("COUNTDOWN_REGIONAL_LABELS_YEAR")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_MONTH")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_WEEK")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_DAY")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_HOUR")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_MIN")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_SEC")."'],
					labels1: ['".GetMessage("COUNTDOWN_REGIONAL_LABELS1_YEAR")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS1_MONTH")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS1_WEEK")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS1_DAY")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS1_HOUR")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_MIN")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_SEC")."'],
					labels2: ['".GetMessage("COUNTDOWN_REGIONAL_LABELS2_YEAR")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS2_MONTH")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS2_WEEK")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS2_DAY")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS2_HOUR")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_MIN")."', '".GetMessage("COUNTDOWN_REGIONAL_LABELS_SEC")."'],
					compactLabels: ['".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS_YEAR")."', '".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS_MONTH")."', '".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS_WEEK")."', '".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS_DAY")."'],
					compactLabels1: ['".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS1_YEAR")."', '".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS_MONTH")."', '".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS_WEEK")."', '".GetMessage("COUNTDOWN_REGIONAL_COMPACT_LABELS_DAY")."'],
					whichLabels: function(amount) {
						var units = amount % 10;
						var tens = Math.floor((amount % 100) / 10);
						return (amount == 1 ? 1 : (units >= 2 && units <= 4 && tens != 1 ? 2 : (units == 1 && tens != 1 ? 1 : 0)));
					},
					digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
					timeSeparator: ':',
					isRTL: false
				};
				$.countdown.setDefaults($.countdown.regionalOptions['ru']);
			});
		</script>
	", true);	
	$APPLICATION->AddHeadString("
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.toriani.ru/externaljs/analytics.js','ga');

  ga('create', 'UA-75057795-1', 'auto');
  ga('send', 'pageview');

</script>");	

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/main.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/script.js");
	$APPLICATION->ShowHead();?>	

	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KR6CSPZ');</script>
<!-- End Google Tag Manager -->
	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49526116 = new Ya.Metrika2({
                    id:49526116,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://www.toriani.ru/externaljs/ytag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49526116" style="; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.toriani.ru/externaljs/Gtag.js?id=UA-121966545-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121966545-1');
</script>
	<script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/710e962a5e30166fbca991d4daf7d8dd_0.js" async></script>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KR6CSPZ";
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<?global $arSetting;?>
	<?$arSetting = $APPLICATION->IncludeComponent("altop:settings", "", array(), false, array("HIDE_ICONS" => "Y"));?>	
	<div class="bx-panel<?=($arSetting['CART_LOCATION']['VALUE'] == 'TOP') ? ' clvt' : ''?>">
		<?$APPLICATION->ShowPanel();?>
	</div>	
	<div class="body<?=($arSetting['CATALOG_LOCATION']['VALUE'] == 'HEADER') ? ' clvh' : ''?><?=($arSetting['CART_LOCATION']['VALUE'] == 'TOP') ? ' clvt' : ''?><?=($arSetting['CART_LOCATION']['VALUE'] == 'RIGHT') ? ' clvr' : ''?><?=($arSetting['CART_LOCATION']['VALUE'] == 'LEFT') ? ' clvl' : ''?><?=($arSetting['SITE_BACKGROUND']['VALUE'] == 'DARK') ? ' sbg_dark' : ''?>">
		<div class="page-wrapper">
			<?if($arSetting["CATALOG_LOCATION"]["VALUE"] == "HEADER"):?>
				<div class="top-menu-header">					
					<div class="top-menu">							
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/top_menu.php"), false, array("HIDE_ICONS" => "Y"));?>
					</div>					
				</div>
			<?endif;?>
			<div class="center">
				<header>					
					<div class="header_1">
						<div class="logo">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_logo.php"), false);?>
						</div>
					</div>
					<div class="header_2">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/header_search.php"), false, array("HIDE_ICONS" => "Y"));?>
					</div>
					<div class="header_3">
						<div class="schedule">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule.php"), false);?>
						</div>
					</div>
					<div class="header_4">
						<div class="telephone">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/telephone.php"), false);?>
							<a class="btn_buy apuo callback_anch" href="javascript:void(0)" onclick="OpenCallbackPopup();"><span class="cont"><i class="fa fa-phone"></i><span class="text"><?=GetMessage("ALTOP_CALL_BACK")?></span></span></a>
							<?$APPLICATION->IncludeComponent("altop:callback", "",
								array(
									"EMAIL_TO" => "info@toriani.ru, n.shibaeva@raz-vitie.ru",
									"REQUIRED_FIELDS" => array("NAME", "TEL", "TIME")
								),
								false
							);?>
						</div>
					</div>
					<?if($arSetting["CATALOG_LOCATION"]["VALUE"] == "LEFT"):?>
						<div class="top-menu">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/top_menu.php"), false, array("HIDE_ICONS" => "Y"));?>
						</div>
					<?elseif($arSetting["CATALOG_LOCATION"]["VALUE"] == "HEADER"):?>
						<div class="top-catalog">							
							<?$APPLICATION->IncludeComponent("bitrix:menu", $arSetting["CATALOG_VIEW"]["VALUE"] == "FOUR_LEVELS" ? "tree" : "sections",
								array(
									"ROOT_MENU_TYPE" => "left",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "86400",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(),
									"MAX_LEVEL" => "4",
									"CHILD_MENU_TYPE" => "left",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N"
								),
								false
							);?>
						</div>
					<?endif;?>					
				</header>
				<div class="top_panel">
					<div class="panel_1">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/sections.php"), false, array("HIDE_ICONS" => "Y"));?>
					</div>
					<div class="panel_2">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "panel", 
							array(
								"ROOT_MENU_TYPE" => "top",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_TIME" => "86400",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(),
								"MAX_LEVEL" => "3",
								"CHILD_MENU_TYPE" => "topchild",
								"USE_EXT" => "N",
								"ALLOW_MULTI_SELECT" => "N"
							),
							false
						);?>
					</div>
					<div class="panel_3">
						<ul class="contacts-vertical">
							<li>
								<a class="showcontacts" href="javascript:void(0)"><i class="fa fa-phone"></i></a>
							</li>
						</ul>
					</div>
					<div class="panel_4">
						<ul class="search-vertical">
							<li>
								<a class="showsearch" href="javascript:void(0)"><i class="fa fa-search"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="content-wrapper">
					<div class="content">
						<div class="left-column">
							<?if($APPLICATION->GetDirProperty("PERSONAL_SECTION")):?>
								<div class="h3"><?=GetMessage("PERSONAL_HEADER");?></div>
								<?$APPLICATION->IncludeComponent("altop:user", ".default", array(), false);?>
								<?$APPLICATION->IncludeComponent("bitrix:menu", "tree",
									array(
										"ROOT_MENU_TYPE" => "personal",
										"MENU_CACHE_TYPE" => "A",
										"MENU_CACHE_TIME" => "86400",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"MENU_CACHE_GET_VARS" => array(),
										"MAX_LEVEL" => "1",
										"CHILD_MENU_TYPE" => "personal",
										"USE_EXT" => "Y",
										"DELAY" => "N",
										"ALLOW_MULTI_SELECT" => "N"
									),
									false
								);?>
							<?else:
								if($arSetting["CATALOG_LOCATION"]["VALUE"] == "LEFT"):?>
									<div class="h3"><?=GetMessage("BASE_HEADER");?></div>
									<?$APPLICATION->IncludeComponent("bitrix:menu", $arSetting["CATALOG_VIEW"]["VALUE"] == "FOUR_LEVELS" ? "tree" : "sections",
										array(
											"ROOT_MENU_TYPE" => "left",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_TIME" => "86400",
											"MENU_CACHE_USE_GROUPS" => "Y",
											"MENU_CACHE_GET_VARS" => array(),
											"MAX_LEVEL" => "4",
											"CHILD_MENU_TYPE" => "left",
											"USE_EXT" => "Y",
											"DELAY" => "N",
											"ALLOW_MULTI_SELECT" => "N"
										),
										false
									);?>									
								<?endif;
							endif;
							if($arSetting["SMART_FILTER_LOCATION"]["VALUE"] == "VERTICAL"):
								$APPLICATION->ShowViewContent("filter_vertical");
							endif;
							if($arSetting["CATALOG_LOCATION"]["VALUE"] == "HEADER"):?>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
									array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/banner_left.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									array("HIDE_ICONS" => "Y")
								);?>													
								<?if($APPLICATION->GetCurPage(true)!= SITE_DIR."index.php") {?>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/discount_left.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										array("HIDE_ICONS" => "Y")
									);?>
								<?}?>
							<?endif;?>
							<ul class="new_leader_disc">
								<li>
									<a class="new" href="<?=SITE_DIR?>catalog/newproduct/">
										<span class="icon"><?=GetMessage("CR_TITLE_ICON_NEWPRODUCT")?></span>
										<span class="text"><?=GetMessage("CR_TITLE_NEWPRODUCT")?></span>
									</a>
								</li>
								<li>
									<a class="saleleader" href="<?=SITE_DIR?>catalog/saleleader/">
										<span class="icon"><?=GetMessage("CR_TITLE_ICON_SALELEADER")?></span>
										<span class="text"><?=GetMessage("CR_TITLE_SALELEADER")?></span>
									</a>
								</li>
								<li>
									<a class="discount" href="<?=SITE_DIR?>catalog/discount/">
										<span class="icon"><?=GetMessage("CR_TITLE_ICON_DISCOUNT")?></span>
										<span class="text"><?=GetMessage("CR_TITLE_DISCOUNT")?></span>
									</a>
								</li>
							</ul>							
							<?if($arSetting["CATALOG_LOCATION"]["VALUE"] == "LEFT"):?>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
									array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/banner_left.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									array("HIDE_ICONS" => "Y")
								);?>													
								<?if($APPLICATION->GetCurPage(true)!= SITE_DIR."index.php") {?>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/discount_left.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										array("HIDE_ICONS" => "Y")
									);?>
								<?}?>
							<?endif;?>
							<div class="vendors">
								<div class="h3"><?=GetMessage("MANUFACTURERS");?></div>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/vendors_left.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									array("HIDE_ICONS" => "Y")
								);?>
							</div>
							<div class="subscribe">
								<div class="h3"><?=GetMessage("SUBSCRIBE");?></div>
								<p><?=GetMessage("SUBSCRIBE_TEXT");?></p>
								<?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "left", 
									array(
										"USE_PERSONALIZATION" => "Y",	
										"PAGE" => SITE_DIR."personal/subscribe/",
										"SHOW_HIDDEN" => "N",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "86400",
										"CACHE_NOTES" => ""
									),
									false
								);?>
							</div>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/stati_left.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "html",
								),
								false,
								array("HIDE_ICONS" => "Y")
							);?>
						</div>
						<div class="workarea">
							<?if($APPLICATION->GetCurPage(true)== SITE_DIR."index.php"):
								if(in_array("SLIDER", $arSetting["HOME_PAGE"]["VALUE"])):?>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/slider.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										array("HIDE_ICONS" => "Y")
									);?>
								<?endif;
								if(in_array("ADVANTAGES", $arSetting["HOME_PAGE"]["VALUE"])):
									global $arAdvFilter;
									$arAdvFilter = array(
										"!PROPERTY_SHOW_HOME" => false
									);?>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/advantages.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										array("HIDE_ICONS" => "Y")
									);?>
								<?endif;
								if(in_array("BANNERS", $arSetting["HOME_PAGE"]["VALUE"])):?>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/banners_main.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										array("HIDE_ICONS" => "Y")
									);?>
								<?endif;
								if(in_array("NEWS", $arSetting["HOME_PAGE"]["VALUE"])):?>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/news_home.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										array("HIDE_ICONS" => "Y")
									);?>
								<?endif;
								if(in_array("TABS", $arSetting["HOME_PAGE"]["VALUE"])):?>
									<div class="ndl_tabs">
										<div class="section">
											<ul class="tabs">
												<li class="new">
													<a href="javascript:void(0)"><span><?=GetMessage("CR_TITLE_NEWPRODUCT")?></span></a>
												</li>
												<li class="hit">
													<a href="javascript:void(0)"><span><?=GetMessage("CR_TITLE_SALELEADER")?></span></a>
												</li>
												<li class="discount">
													<a href="javascript:void(0)"><span><?=GetMessage("CR_TITLE_DISCOUNT")?></span></a>
												</li>
											</ul>
											<div class="new box">
												<div class="catalog-top">
													<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
														array(
															"AREA_FILE_SHOW" => "file",
															"PATH" => SITE_DIR."include/newproduct.php",
															"AREA_FILE_RECURSIVE" => "N",
															"EDIT_MODE" => "html",
														),
														false,
														array("HIDE_ICONS" => "Y")
													);?>
													<a class="all" href="<?=SITE_DIR?>catalog/newproduct/"><?=GetMessage("CR_TITLE_ALL_NEWPRODUCT");?></a>
												</div>
											</div>
											<div class="hit box">
												<div class="catalog-top">
													<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
														array(
															"AREA_FILE_SHOW" => "file",
															"PATH" => SITE_DIR."include/saleleader.php",
															"AREA_FILE_RECURSIVE" => "N",
															"EDIT_MODE" => "html",
														),
														false,
														array("HIDE_ICONS" => "Y")
													);?>
													<a class="all" href="<?=SITE_DIR?>catalog/saleleader/"><?=GetMessage("CR_TITLE_ALL_SALELEADER");?></a>
												</div>
											</div>
											<div class="discount box">
												<div class="catalog-top">
													<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
														array(
															"AREA_FILE_SHOW" => "file",
															"PATH" => SITE_DIR."include/discount.php",
															"AREA_FILE_RECURSIVE" => "N",
															"EDIT_MODE" => "html",
														),
														false,
														array("HIDE_ICONS" => "Y")
													);?>
													<a class="all" href="<?=SITE_DIR?>catalog/discount/"><?=GetMessage("CR_TITLE_ALL_DISCOUNT");?></a>
												</div>
											</div>
										</div>
									</div>
								<?endif;?>
								<div class="clr"></div>
							<?endif;?>
							<div class="body_text" style="<?=($APPLICATION->GetCurPage(true) == SITE_DIR.'index.php') ? 'padding:0px 15px;' : 'padding:0px;';?>">
								<?if($APPLICATION->GetCurPage(true)!= SITE_DIR."index.php"):?>
									<div class="breadcrumb-search">
										<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", 
											array(
												"START_FROM" => "0",
												"PATH" => "",
												"SITE_ID" => "-"
											),
											false,
											array("HIDE_ICONS" => "Y")
										);?>
										<div class="podelitsya">											
											<script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
											<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus" data-yashareTheme="counter"></div>
										</div>
										<div class="clr"></div>
									</div>
									<h1><?=$APPLICATION->ShowTitle(false);?></h1>
								<?endif;?>		