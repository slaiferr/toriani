#!/bin/bash

for i in $(find ./ -type f -name "*.js"); do
	echo item: $i
	yui-compressor $i -o $i
done
