<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(count($arResult["ITEMS"]) < 1)
	return;?>

<div class="news-list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<div class="news-item">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<div class="news-wrap">
					
					<div class="news-title">

						<span><?=$arItem["NAME"]?></span>

	                    <span class="news-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>

					</div>

	                <div class="news-text"><?=$arItem["PREVIEW_TEXT"]?></div>

<!-- 	            <div class="news-date-cont"> </div> -->

				</div>

                <div class="news-img"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" height="200" /></div>

			</a>
		</div>
	<?endforeach;?>
</div>
<div class="clr"></div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>