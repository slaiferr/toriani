<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);?><!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?$APPLICATION->SetAdditionalCSS("//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-1.7.1.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-ui-1.8.16.custom.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.scrollUp.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox-1.3.1.pack.js");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox-1.3.1.css");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/anythingslider/jquery.easing.1.2.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/anythingslider/jquery.anythingslider.js");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/anythingslider/slider.css");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/custom-forms/jquery.custom-forms.js");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/custom-forms/custom-forms.css");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/main.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/script.js");
	$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>
	<div class="body">
		<div id="page-wrapper">
			<div class="pop-up-bg callback_body"></div>
			<div class="pop-up callback">
				<a href="#" class="pop-up-close callback_close"></a>
				<div class="h1"><?=GetMessage("ALTOP_CALL_BACK");?></div>
				<?$APPLICATION->IncludeComponent(
	"altop:callback", 
	".default", 
	array(
		"EMAIL_TO" => "imesergei@inbox.ru",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "TEL",
			2 => "TIME",
		)
	),
	false
);?>
			</div>
			<div class="center">
				<div id="header">
					<div id="header_1">
						<div id="logo">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_logo.php"), false);?>
						</div>
					</div>
					<div id="header_2">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/header_search.php"), false);?>
					</div>
					<div id="header_3">
						<div class="schedule">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule.php"), false);?>
						</div>
					</div>
					<div id="header_4">
						<div class="telephone">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/telephone.php"), false);?>
							<a class="callback_anch" href="#"><?=GetMessage("ALTOP_CALL_BACK")?></a>
						</div>
					</div>
					<div id="top-menu">
						<?$APPLICATION->IncludeComponent('bitrix:menu', "horizontal_multilevel", array(
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "86400",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(),
							"MAX_LEVEL" => "2",
							"CHILD_MENU_TYPE" => "topchild",
							"USE_EXT" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							)
						);?>
					</div>
				</div>
				<div id="top_panel">
					<div id="panel_1">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/sections.php"), false);?>
					</div>
					<div id="panel_2">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "panel", array(
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "86400",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(),
							"MAX_LEVEL" => "3",
							"CHILD_MENU_TYPE" => "topchild",
							"USE_EXT" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							)
						);?>
					</div>
					<div id="panel_3">
						<ul class="contacts-vertical">
							<li>
								<a class="showcontacts" href="javascript:void(0)"></a>
							</li>
						</ul>
					</div>
					<div id="panel_4">
						<ul class="search-vertical">
							<li>
								<a class="showsearch" href="javascript:void(0)"></a>
							</li>
						</ul>
					</div>
				</div>
				<div id="content-wrapper">
					<div id="content">
						<div id="left-column">
							<?if($APPLICATION->GetDirProperty("PERSONAL_SECTION")):?>
								<div class="h3"><?=GetMessage("PERSONAL_HEADER");?></div>
								<?$APPLICATION->IncludeComponent("altop:user", ".default", array(), false);?>
							<?else:?>
								<div class="h3"><?=GetMessage("BASE_HEADER");?></div>
							<?endif;?>
							<?$APPLICATION->IncludeComponent("bitrix:menu", "tree", array(
								"ROOT_MENU_TYPE" => "left",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_TIME" => "86400",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(),
								"MAX_LEVEL" => "4",
								"CHILD_MENU_TYPE" => "left",
								"USE_EXT" => "Y",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N"
								),
								false
							);?>
							<ul id="new_leader_disc">
								<li>
									<a class="new" href="<?=SITE_DIR?>catalog/newproduct/"><?=GetMessage("CR_TITLE_NEWPRODUCT");?></a>
								</li>
								<li>
									<a class="saleleader" href="<?=SITE_DIR?>catalog/saleleader/"><?=GetMessage("CR_TITLE_SALELEADER");?></a>
								</li>
								<li>
									<a class="discount" href="<?=SITE_DIR?>catalog/discount/"><?=GetMessage("CR_TITLE_DISCOUNT");?></a>
								</li>
							</ul>
							<?if(strpos($APPLICATION->GetCurDir(), '/catalog/')===false) {?>
								<div id="banner_left">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
										Array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR."include/banner_left.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "html",
										),
										false,
										Array('HIDE_ICONS' => 'Y')
									);?>
								</div>
							<?}?>
							<?if(strpos($APPLICATION->GetCurDir(), '/catalog/')!==false) {?>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/discount_left.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									Array('HIDE_ICONS' => 'Y')
								);?>
							<?}?>
							<div id="vendors">
								<div class="h3"><?=GetMessage("MANUFACTURERS");?></div>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/vendors_left.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									Array('HIDE_ICONS' => 'Y')
								);?>
							</div>
							<div id="subscribe">
								<div class="h3"><?=GetMessage("SUBSCRIBE");?></div>
								<p><?=GetMessage("SUBSCRIBE_TEXT");?></p>
								<?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "left", Array(
									"USE_PERSONALIZATION" => "Y",	
									"PAGE" => SITE_DIR."personal/subscribe/subscr_edit.php",
									"SHOW_HIDDEN" => "N",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "86400",
									"CACHE_NOTES" => ""
									),
									false
								);?>
							</div>
							<div class="social">
								<a href="<?=GetMessage("VK")?>"><div class="vk"></div></a>
								<a href="<?=GetMessage("FACEBOOK")?>"><div class="fb"></div></a>
								<a href="<?=GetMessage("INSTAGRAM")?>"><div class="in"></div></a>
								<a href="<?=GetMessage("YOUTUBE")?>"><div class="yt"></div></a>
							</div>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/stati_left.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "html",
								),
								false,
								Array('HIDE_ICONS' => 'Y')
							);?>
						</div>
						<div id="workarea">
							<?if($APPLICATION->GetCurPage(true)== SITE_DIR."index.php") {?>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/slider.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									Array('HIDE_ICONS' => 'Y')
								);?>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/news_home.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									Array('HIDE_ICONS' => 'Y')
								);?>
								<div class="ndl_tabs">
									<div class="section">
										<ul class="tabs">
											<li class="discount">
												<img class="nohover" src="<?=SITE_TEMPLATE_PATH?>/images/discount_grey.png" alt="<?=GetMessage("CR_TITLE_DISCOUNT")?>" />
												<img class="hover" src="<?=SITE_TEMPLATE_PATH?>/images/discount_color.png" alt="<?=GetMessage("CR_TITLE_DISCOUNT")?>" />
												<span><?=GetMessage("CR_TITLE_DISCOUNT")?></span>
											</li>
											<li class="hit">
												<img class="nohover" src="<?=SITE_TEMPLATE_PATH?>/images/hit_grey.png" alt="<?=GetMessage("CR_TITLE_SALELEADER")?>" />
												<img class="hover" src="<?=SITE_TEMPLATE_PATH?>/images/hit_color.png" alt="<?=GetMessage("CR_TITLE_SALELEADER")?>" />
												<span><?=GetMessage("CR_TITLE_SALELEADER")?></span>
											</li>
											<li class="new">
												<img class="nohover" src="<?=SITE_TEMPLATE_PATH?>/images/new_grey.png" alt="<?=GetMessage("CR_TITLE_NEWPRODUCT")?>" />
												<img class="hover" src="<?=SITE_TEMPLATE_PATH?>/images/new_color.png" alt="<?=GetMessage("CR_TITLE_NEWPRODUCT")?>" />
												<span><?=GetMessage("CR_TITLE_NEWPRODUCT")?></span>
											</li>
										</ul>
										<div class="discount box">
											<div class="catalog-top">
												<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
													Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR."include/discount.php",
														"AREA_FILE_RECURSIVE" => "N",
														"EDIT_MODE" => "html",
													),
													false,
													Array('HIDE_ICONS' => 'Y')
												);?>
												<a class="all" href="<?=SITE_DIR?>catalog/discount/"><?=GetMessage("CR_TITLE_ALL_DISCOUNT");?></a>
											</div>
										</div>
										<div class="hit box">
											<div class="catalog-top">
												<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
													Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR."include/saleleader.php",
														"AREA_FILE_RECURSIVE" => "N",
														"EDIT_MODE" => "html",
													),
													false,
													Array('HIDE_ICONS' => 'Y')
												);?>
												<a class="all" href="<?=SITE_DIR?>catalog/saleleader/"><?=GetMessage("CR_TITLE_ALL_SALELEADER");?></a>
											</div>
										</div>
										<div class="new box">
											<div class="catalog-top">
												<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
													Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR."include/newproduct.php",
														"AREA_FILE_RECURSIVE" => "N",
														"EDIT_MODE" => "html",
													),
													false,
													Array('HIDE_ICONS' => 'Y')
												);?>
												<a class="all" href="<?=SITE_DIR?>catalog/newproduct/"><?=GetMessage("CR_TITLE_ALL_NEWPRODUCT");?></a>
											</div>
										</div>
									</div>
								</div>
								<div class="clr"></div>
							<?}?>
							<div id="body_text" style="<?if ($APPLICATION->GetCurPage(true)== SITE_DIR."index.php"): echo 'padding:0px 15px;'; else: echo 'padding:0px;'; endif;?>">
								<?if($APPLICATION->GetCurPage(true)!= SITE_DIR."index.php"):?>
									<div id="breadcrumb-search">
										<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", 
											array(
												"START_FROM" => "1",
												"PATH" => "",
												"SITE_ID" => "-"
											),
											false,
											Array('HIDE_ICONS' => 'Y')
										);?>
										<div id="podelitsya">
											<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
											<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus"></div>
										</div>
										<div class="clr"></div>
									</div>
									<h1><?=$APPLICATION->ShowTitle(false);?></h1>
								<?endif;?>