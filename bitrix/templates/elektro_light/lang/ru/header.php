<?
$MESS["HEADER_TO_MAIN_PAGE"] = "На главную страницу";
$MESS["SEARCH_GOODS"] = "Товары";
$MESS["SEARCH_OTHER"] = "Прочее";
$MESS["BASE_HEADER"] = "Каталог товаров";
$MESS["MANUFACTURERS"] = "Производители";
$MESS["PERSONAL_HEADER"] = "Личный кабинет";
$MESS["CR_TITLE_NEWPRODUCT"] = "Новинки";
$MESS["CR_TITLE_SALELEADER"] = "Хиты продаж";
$MESS["CR_TITLE_DISCOUNT"] = "Скидки";
$MESS["CR_TITLE_ALL_NEWPRODUCT"] = "Все новинки";
$MESS["CR_TITLE_ALL_SALELEADER"] = "Все хиты продаж";
$MESS["CR_TITLE_ALL_DISCOUNT"] = "Все скидки";
$MESS["ALTOP_CALL_BACK"] = "Заказать обратный звонок";
$MESS["SUBSCRIBE"] = "Будьте в курсе!";
$MESS["SUBSCRIBE_TEXT"] = "Новости, обзоры и акции";
$MESS["VK"] = "http://vk.com/toriani";
$MESS["FACEBOOK"] = "http://facebook.com";
$MESS["INSTAGRAM"] = "https://instagram.com/toriani.ru/";
$MESS["YOUTUBE"] = "http://youtube.com";
?>