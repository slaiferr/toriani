<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);

if(count($arResult["STORES"]) > 0):
	foreach($arResult["STORES"] as $pid => $arProperty):?>
		<div class="catalog-detail-store">
			<span><?=$arProperty["TITLE"]?></span>
			<?if(isset($arProperty["PHONE"])):?>
				<span>&nbsp;&nbsp;<?=GetMessage('S_PHONE')?></span>
				<span><?=$arProperty["PHONE"]?></span>
			<?endif;?>
			<?if(isset($arProperty["SCHEDULE"])):?>
				<span>&nbsp;&nbsp;<?=GetMessage('S_SCHEDULE')?></span>
				<span><?=$arProperty["SCHEDULE"]?></span>
			<?endif;?>
			<b><?=$arProperty["AMOUNT"]?></b>
		</div>
	<?endforeach;
endif;?>