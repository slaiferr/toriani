<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="cart_line">
	<?$frame = $this->createFrame("cart_line")->begin();?>
		<a href="<?=$arParams["PATH_TO_BASKET"]?>" class="cart" rel="nofollow">
			<?=GetMessage('TSBS')?>
		</a>
	
		<?$price = CCurrencyLang::GetCurrencyFormat($arResult["CURRENCY"], "ru");
		if(empty($price["THOUSANDS_SEP"])):
			$price["THOUSANDS_SEP"] = " ";
		endif;
		$currency = str_replace("#", " ", $price["FORMAT_STRING"]);?>

		<span class="qnt">
			<?if(IntVal($arResult["NUM_PRODUCTS"])>0 && isset($arResult["QUANTITY"])):
				echo $arResult["QUANTITY"];
			else:
				echo "0";
			endif;?>
		</span>

		<span class="sum_curr">
			<?if(IntVal($arResult["NUM_PRODUCTS"])>0 && isset($arResult["QUANTITY"])):
				echo number_format($arResult["SUM"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);
				echo "<span class='curr'>".$currency."</span>";
			else:
				echo "0 <span class='curr'>".$currency."</span>";
			endif;?>
		</span>
	
		<?if((strpos($APPLICATION->GetCurDir(), $arParams["PATH_TO_BASKET"])===false) AND (strpos($APPLICATION->GetCurDir(), $arParams["PATH_TO_ORDER"])===false) && (IntVal($arResult["NUM_PRODUCTS"])>0)):?>
			<form action="<?=$arParams["PATH_TO_BASKET"]?>" method="post">
				<button name="oformit" class="btn_buy oformit" value="<?=GetMessage('BASKET_LINE_CHECKOUT')?>"><?=GetMessage("BASKET_LINE_CHECKOUT")?></button>
			</form>
		<?else:?>
			<div class="btn_buy oformit_dsbl"><?=GetMessage('BASKET_LINE_CHECKOUT')?></div>
		<?endif;
	$frame->end();?>
</div>