<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog")) {
	return;
}

$resBasket = CSaleBasket::GetList(
	array(), 
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL",
		"DELAY" => "N",
		"CAN_BUY" => "Y",
	), 
	false, 
	false, 
	array(
		"ID", 
		"PRICE",
		"QUANTITY"
	)
);

while($ar = $resBasket->Fetch()) {
	$arResult["QUANTITY"] += $ar["QUANTITY"];
	$arResult["SUM"] += $ar["PRICE"] * $ar["QUANTITY"];
}

$arResult["CURRENCY"] = CSaleLang::GetLangCurrency(SITE_ID);
?>