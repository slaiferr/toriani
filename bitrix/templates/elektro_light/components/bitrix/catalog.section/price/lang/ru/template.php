<?
$MESS["CATALOG_EMPTY_RESULT"] = "По вашему запросу ничего не найдено.";
$MESS["CATALOG_ELEMENT_SKIDKA"] = "Экономия";
$MESS["UNIT"] = "за";
$MESS["CATALOG_ASK_PRICE"] = "Запросить цену";
$MESS["CATALOG_ELEMENT_ADD_TO_CART"] = "В корзину";
$MESS["CATALOG_ELEMENT_ADDED"] = "Добавлено";
$MESS["CATALOG_ELEMENT_PRICE_ADDED"] = "Ок";
$MESS["CATALOG_ELEMENT_AVAILABLE"] = "В наличии";
$MESS["CATALOG_ELEMENT_PRICE_AVAILABLE"] = "<div>Нет <span>цены</span></div>";
$MESS["CATALOG_ELEMENT_NOT_AVAILABLE"] = "Нет в наличии";
$MESS["CATALOG_ELEMENT_PRICE_NOT_AVAILABLE"] = "Нет";
$MESS["CATALOG_MORE_OPTIONS"] = "Выберите дополнительные параметры товара";
?>