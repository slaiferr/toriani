<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$frame = $this->createFrame("bigdata")->begin();
	
$injectId = "bigdata_recommeded_products_".rand();?>

<script type="application/javascript">
	//<![CDATA[
	BX.cookie_prefix = "<?=CUtil::JSEscape(COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM'))?>";
	BX.cookie_domain = "<?=$APPLICATION->GetCookieDomain()?>";
	BX.current_server_time = "<?=time()?>";
	
	function bx_rcm_recommndation_event_attaching(rcm_items_cont) {
		var detailLinks = BX.findChildren(rcm_items_cont, {"className":"item-title"}, true);

		if(detailLinks) {
			for(i in detailLinks) {
				BX.bind(detailLinks[i], "click", function(e){
					window.JCCatalogBigdataProducts.prototype.RememberRecommendation(
						BX(this),
						BX(this).getAttribute("data-product-id")
					);
				});
			}
		}
	}
	
	BX.ready(function(){
		bx_rcm_recommndation_event_attaching(BX("<?=$injectId?>_items"));
	});
	//]]>
</script>

<?if(isset($arResult["REQUEST_ITEMS"])) {
	CJSCore::Init(array('ajax'));

	// component parameters
	$signer = new \Bitrix\Main\Security\Sign\Signer;
	$signedParameters = $signer->sign(
		base64_encode(serialize($arResult["_ORIGINAL_PARAMS"])),
		"bx.bd.products.recommendation"
	);
	$signedTemplate = $signer->sign($arResult["RCM_TEMPLATE"], "bx.bd.products.recommendation");?>

	<div id="<?=$injectId?>" class="bigdata_recommended_products_container"></div>

	<script type="application/javascript">
		//<![CDATA[
		BX.ready(function(){
			var params = <?=CUtil::PhpToJSObject($arResult["RCM_PARAMS"])?>;
			var url = "https://analytics.bitrix.info/crecoms/v1_0/recoms.php";
			var data = BX.ajax.prepareData(params);

			if(data) {
				url += (url.indexOf('?') !== -1 ? "&" : "?") + data;
				data = '';
			}

			var onready = function(response) {

				if(!response.items) {
					response.items = [];
				}
				BX.ajax({
					url: "/bitrix/components/bitrix/catalog.bigdata.products/ajax.php?"+BX.ajax.prepareData({"AJAX_ITEMS": response.items, "RID": response.id}),
					method: "POST",
					data: {"parameters":"<?=CUtil::JSEscape($signedParameters)?>", "template": "<?=CUtil::JSEscape($signedTemplate)?>", "rcm": "yes"},
					dataType: "html",
					processData: false,
					start: true,
					onsuccess: function (html) {
						var ob = BX.processHTML(html);

						// inject
						BX("<?=$injectId?>").innerHTML = ob.HTML;
						BX.ajax.processScripts(ob.SCRIPT);
					}
				});
			};

			BX.ajax({
				"method": "GET",
				"dataType": "json",
				"url": url,
				"timeout": 3,
				"onsuccess": onready,
				"onfailure": onready
			});
		});
		//]]>
	</script>
	
	<?$frame->end();
	return;
}

if(!empty($arResult["ITEMS"])):?>
	<script type="text/javascript">
		//<![CDATA[
		$(document).ready(function() {
			$(".add2basket_bigdata_form").submit(function() {
				var form = $(this);
					
				$(".more_options_body").css({"display":"none"});
				$(".more_options").css({"display":"none"});

				imageItem = form.find(".item_image").attr("value");
				$("#addItemInCart .item_image_full").html(imageItem);

				titleItem = form.find(".item_title").attr("value");
				$("#addItemInCart .item_title").text(titleItem);

				descItem = form.find(".item_desc").attr("value");
				$("#addItemInCart .item_desc").text(descItem);

				propsItem = form.find(".item_props").attr("value");
				offer_id = form.find(".offer_id").attr("value");
				if(!!offer_id && 0 < offer_id.length) {
					propsSelectItem = form.find("#item_select_props_bigdata_"+offer_id).attr("value");
					if(!!propsSelectItem && 0 < propsSelectItem.length) {
						propsItem = propsItem + propsSelectItem;
					}
				}
				id = form.find(".id").attr("value");
				if(!!id && 0 < id.length) {
					propsSelectItem = form.find("#item_select_props_bigdata_"+id).attr("value");
					if(!!propsSelectItem && 0 < propsSelectItem.length) {
						propsItem = propsItem + propsSelectItem;
					}
				}
				$("#addItemInCart .item_props").html(propsItem);
					
				countItem = form.find(".quantity").attr("value");
				$("#addItemInCart .item_count").text(countItem);

				var ModalName = $("#addItemInCart");
				CentriredModalWindow(ModalName);
				OpenModalWindow(ModalName);

				$.post($(this).attr("action"), $(this).serialize(), function(data) {
					try {
						$.post("/ajax/basket_line.php", function(data) {
							$("#cart_line").replaceWith(data);
						});
						$.post("/ajax/delay_line.php", function(data) {
							$("#delay").replaceWith(data);
						});
						form.children(".btn_buy").addClass("hidden");
						form.children(".result").removeClass("hidden");
					} catch (e) {}
				});
				return false;
			});
		});
		//]]>
	</script>
	<div id="<?=$injectId?>_items" class="bigdata_recommended_products_items">
		<input type="hidden" name="bigdata_recommendation_id" value="<?=htmlspecialcharsbx($arResult['RID'])?>">
		<div class="bigdata-items">
			<div class="h3"><?=GetMessage("BIGDATA_ITEMS")?></div>
			<div class="catalog-item-cards">
				<?foreach($arResult["ITEMS"] as $key => $arItem):
					if(is_array($arItem)) {
						$strMainID = $this->GetEditAreaId($arItem["ID"]);
						$arItemIDs = array(
							"ID" => $strMainID."_bigdata"
						);

						$bPicture = is_array($arItem["PREVIEW_IMG"]);

						$sticker = "";
						if(array_key_exists("PROPERTIES", $arItem) && is_array($arItem["PROPERTIES"])) {
							if(array_key_exists("NEWPRODUCT", $arItem["PROPERTIES"]) && !$arItem["PROPERTIES"]["NEWPRODUCT"]["VALUE"] == false) {
								$sticker .= "<img class='new' src='".SITE_TEMPLATE_PATH."/images/new.png' alt='newproduct' />";
							}
							if(array_key_exists("SALELEADER", $arItem["PROPERTIES"]) && !$arItem["PROPERTIES"]["SALELEADER"]["VALUE"] == false) {
								$sticker .= "<img class='hit' src='".SITE_TEMPLATE_PATH."/images/hit.png' alt='saleleader' />";
							}
							if(array_key_exists("DISCOUNT", $arItem["PROPERTIES"]) && !$arItem["PROPERTIES"]["DISCOUNT"]["VALUE"] == false) {
								$sticker .= "<img class='discount' src='".SITE_TEMPLATE_PATH."/images/discount.png' alt='discount' />";
							}
						}?>

						<div class="catalog-item-card">
							<div class="catalog-item-info">
								<div class="item-all-title">
									<a class="item-title" href="<?=$arItem['DETAIL_PAGE_URL']?>" data-product-id="<?=$arItem['ID']?>" title="<?=$arItem['NAME']?>">
										<?=$arItem["NAME"]?>
									</a>
								</div>
								<div class="item-image">
									<?if($bPicture):?>
										<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
											<img class="item_img" src="<?=$arItem['PREVIEW_IMG']['SRC']?>" width="<?=$arItem['PREVIEW_IMG']['WIDTH']?>" height="<?=$arItem['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arItem['NAME']?>" />
											<span class="sticker">
												<?=$sticker?>
											</span>
											<?if(!empty($arItem["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])):?>
												<img class="manufacturer" src="<?=$arItem['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC']?>" width="<?=$arItem['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH']?>" height="<?=$arItem['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arItem['PROPERTIES']['MANUFACTURER']['NAME']?>" />
											<?endif;?>
										</a>
									<?else:?>
										<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
											<img class="item_img" src="<?=SITE_TEMPLATE_PATH?>/images/no-photo.jpg" width="150" height="150" alt="<?=$arItem['NAME']?>" />
											<span class="sticker">
												<?=$sticker?>
											</span>
											<?if(!empty($arItem["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])):?>
												<img class="manufacturer" src="<?=$arItem['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC']?>" width="<?=$arItem['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH']?>" height="<?=$arItem['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arItem['PROPERTIES']['MANUFACTURER']['NAME']?>" />
											<?endif;?>
										</a>
									<?endif?>
								</div>
								<div class="item-info">
									<div class="item-desc">
										<?=strip_tags($arItem["PREVIEW_TEXT"]);?>
									</div>
									<div class="rating_compare">
										<div class="rating">
											<?$APPLICATION->IncludeComponent("bitrix:iblock.vote", "ajax",
												Array(
													"DISPLAY_AS_RATING" => "vote_avg",
													"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
													"IBLOCK_ID" => $arParams["IBLOCK_ID"],
													"ELEMENT_ID" => $arItem["ID"],
													"ELEMENT_CODE" => "bigdata",
													"MAX_VOTE" => "5",
													"VOTE_NAMES" => array("1","2","3","4","5"),
													"SET_STATUS_404" => "N",
													"CACHE_TYPE" => $arParams["CACHE_TYPE"],
													"CACHE_TIME" => $arParams["CACHE_TIME"],
													"CACHE_NOTES" => "",
													"READ_ONLY" => "Y"
												),
												false,
												array("HIDE_ICONS" => "Y")
											);?>
										</div>
										<?if($arParams["DISPLAY_COMPARE"]=="Y"):?>
											<div class="add_to_compare">
												<a href="javascript:void(0)" class="catalog-item-compare" id="catalog_add2compare_link_bigdata_<?=$arItem['ID']?>" onclick="return addToCompare('<?=$arItem["COMPARE_URL"]?>', 'catalog_add2compare_link_bigdata_<?=$arItem["ID"]?>');" rel="nofollow">
													<span class="add"><?=GetMessage("CATALOG_ELEMENT_ADD_TO_COMPARE")?></span>
													<span class="added"><?=GetMessage("CATALOG_ELEMENT_ADDED")?></span>
												</a>
											</div>
										<?endif;?>
									</div>
									<?if(isset($arItem["OFFERS"]) && !empty($arItem["OFFERS"])):
										$price = CCurrencyLang::GetCurrencyFormat($arItem["OFFERS_MIN_PRICE"]["CURRENCY"], "ru");
										if(empty($price["THOUSANDS_SEP"])):
											$price["THOUSANDS_SEP"] = " ";
										endif;
										$currency = str_replace("#", " ", $price["FORMAT_STRING"]);
									
										if($arItem["OFFERS_MIN_PRICE"]["VALUE"] == 0):?>
											<div class="catalog-item-price-zero">
												<a class="ask_price_anch" id="ask_price_anch_bigdata_<?=$arItem['OFFERS_MIN_PRICE']['ID']?>" href="#"><?=GetMessage("CATALOG_ASK_PRICE")?></a>
												<?$properties = false;
												foreach($arItem["OFFERS_MIN_PRICE"]["DISPLAY_PROPERTIES"] as $propOffer) {
													$properties[] = $propOffer["NAME"].": ".strip_tags($propOffer["DISPLAY_VALUE"]);
												}
												$properties = implode("; ", $properties);
												if(!empty($properties)):
													$offer_name = $arItem["NAME"]." (".$properties.")";
												else:
													$offer_name = $arItem["NAME"];
												endif;?>
												<?$APPLICATION->IncludeComponent("altop:ask.price", "",
													Array(
														"ELEMENT_ID" => "bigdata_".$arItem["OFFERS_MIN_PRICE"]["ID"],		
														"ELEMENT_NAME" => $offer_name,
														"EMAIL_TO" => "",				
														"REQUIRED_FIELDS" => array("NAME", "EMAIL", "TEL"),
													),
													false,
													array("HIDE_ICONS" => "Y")
												);?>
												<?if(!empty($arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_NAME"])):?>
													<br />
													<span class="unit">
														<?=GetMessage("UNIT")." ".$arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_NAME"]?>
													</span>
												<?endif;?>
											</div>
										<?elseif($arItem["OFFERS_MIN_PRICE"]["DISCOUNT_VALUE"] < $arItem["OFFERS_MIN_PRICE"]["VALUE"]):?>
											<div class="catalog-item-price-full">
												<span class="catalog-item-price-old">
													<?=number_format($arItem["OFFERS_MIN_PRICE"]["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
													<?=$currency;?>
												</span>
												<br />
												<span class="catalog-item-price-percent">
													<?=GetMessage("CATALOG_ELEMENT_SKIDKA")." ".$arItem["OFFERS_MIN_PRICE"]["PRINT_DISCOUNT_DIFF"];?>
												</span>
												<br />
												<span class="catalog-item-price-new">
													<?=number_format($arItem["OFFERS_MIN_PRICE"]["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
													<?="<span style='font-size:12px; font-weight:normal;'>".$currency."</span>";?>
												</span>
												<?if(!empty($arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_NAME"])):?>
													<br />
													<span class="unit">
														<?=GetMessage("UNIT")." ".$arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_NAME"]?>
													</span>
												<?endif;?>
											</div>
										<?else:?>
											<div class="catalog-item-price-nofull">
												<span class="item-price">
													<?=number_format($arItem["OFFERS_MIN_PRICE"]["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
													<?="<span style='font-size:12px; font-weight:normal;'>".$currency."</span>";?>
												</span>
												<?if(!empty($arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_NAME"])):?>
													<br />
													<span class="unit">
														<?=GetMessage("UNIT")." ".$arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_NAME"]?>
													</span>
												<?endif;?>
											</div>
										<?endif;
									else:
										foreach($arItem["PRICES"] as $code=>$arPrice):
											if($arPrice["MIN_PRICE"] == "Y"):
												if($arPrice["CAN_ACCESS"]):
													
													$price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
													if(empty($price["THOUSANDS_SEP"])):
														$price["THOUSANDS_SEP"] = " ";
													endif;
													$currency = str_replace("#", " ", $price["FORMAT_STRING"]);

													if($arPrice["VALUE"] == 0):
														$arItem["ASK_PRICE"]=1;?>
														<div class="catalog-item-price-zero">
															<a class="ask_price_anch" id="ask_price_anch_bigdata_<?=$arItem['ID']?>" href="#"><?=GetMessage("CATALOG_ASK_PRICE")?></a>
															<?$APPLICATION->IncludeComponent("altop:ask.price", "",
																Array(
																	"ELEMENT_ID" => "bigdata_".$arItem["ID"],		
																	"ELEMENT_NAME" => $arItem["NAME"],
																	"EMAIL_TO" => "",				
																	"REQUIRED_FIELDS" => array("NAME", "EMAIL", "TEL")
																),
																false,
																array("HIDE_ICONS" => "Y")
															);?>
															<?if(!empty($arItem["CATALOG_MEASURE_NAME"])):?>
																<br />
																<span class="unit">
																	<?=GetMessage("UNIT")." ".$arItem["CATALOG_MEASURE_NAME"]?>
																</span>
															<?endif;?>
														</div>
													<?elseif($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
														<div class="catalog-item-price-full">
															<span class="catalog-item-price-old">
																<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																<?=$currency;?>
															</span>
															<br />
															<span class="catalog-item-price-percent">
																<?=GetMessage("CATALOG_ELEMENT_SKIDKA")." ".$arPrice["PRINT_DISCOUNT_DIFF"];?>
															</span>
															<br />
															<span class="catalog-item-price-new">
																<?=number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																<?="<span style='font-size:12px; font-weight:normal;'>".$currency."</span>";?>
															</span>
															<?if(!empty($arItem["CATALOG_MEASURE_NAME"])):?>
																<br />
																<span class="unit">
																	<?=GetMessage("UNIT")." ".$arItem["CATALOG_MEASURE_NAME"]?>
																</span>
															<?endif;?>
														</div>
													<?else:?>
														<div class="catalog-item-price-nofull">
															<span class="item-price">
																<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																<?="<span style='font-size:12px; font-weight:normal;'>".$currency."</span>";?>
															</span>
															<?if(!empty($arItem["CATALOG_MEASURE_NAME"])):?>
																<br />
																<span class="unit">
																	<?=GetMessage("UNIT")." ".$arItem["CATALOG_MEASURE_NAME"]?>
																</span>
															<?endif;?>
														</div>
													<?endif;
												endif;
											endif;
										endforeach;
									endif;?>
									<div class="buy_more">
										<?if((isset($arItem["OFFERS"]) && !empty($arItem["OFFERS"])) || (isset($arItem["SELECT_PROPS"]) && !empty($arItem["SELECT_PROPS"]))):
											if(isset($arItem["OFFERS"]) && !empty($arItem["OFFERS"])):?>
												<script type="text/javascript">
													$(document).ready(function() {
														$("#add2basket_bigdata_offer_form_<?=$arItem['ID']?>").submit(function() {
															var form = $(this);
															$(window).resize(function () {
																modalHeight = ($(window).height() - $("#<?=$arItemIDs['ID']?>").height()) / 2;
																$("#<?=$arItemIDs['ID']?>").css({
																	"top": modalHeight + "px"
																});
															});
															$(window).resize();
															$("#<?=$arItemIDs['ID']?>_body").css({"display":"block"});
															$("#<?=$arItemIDs['ID']?>").css({"display":"block"});
																	
															quantityItem = form.find("#quantity_bigdata_<?=$arItem['ID']?>").attr("value");
															$("#<?=$arItemIDs['ID']?> .quantity").attr("value", quantityItem);
															return false;
														});
														$("#<?=$arItemIDs['ID']?>_close, #<?=$arItemIDs['ID']?>_body").click(function(e){
															e.preventDefault();
															$("#<?=$arItemIDs['ID']?>_body").css({"display":"none"});
															$("#<?=$arItemIDs['ID']?>").css({"display":"none"});
														});
													});
												</script>
												<div class="add2basket_block">
													<form action="<?=$APPLICATION->GetCurPage()?>" id="add2basket_bigdata_offer_form_<?=$arItem['ID']?>">
														<a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_bigdata_<?=$arItem["ID"]?>').value > <?=$arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_RATIO"]?>) BX('quantity_bigdata_<?=$arItem["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arItem["ID"]?>').value)-<?=$arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_RATIO"]?>;"></a>
														<input type="text" id="quantity_bigdata_<?=$arItem['ID']?>" name="quantity" class="quantity" value="<?=$arItem['OFFERS_MIN_PRICE']['CATALOG_MEASURE_RATIO']?>"/>
														<a href="javascript:void(0)" class="plus" onclick="BX('quantity_bigdata_<?=$arItem["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arItem["ID"]?>').value)+<?=$arItem["OFFERS_MIN_PRICE"]["CATALOG_MEASURE_RATIO"]?>;"></a>
														<button type="submit" name="add2basket" class="btn_buy" value="<?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?>"><?=GetMessage("CATALOG_ELEMENT_ADD_TO_CART")?></button>
													</form>
												</div>
											<?else:?>
												<script type="text/javascript">
													$(document).ready(function() {
														$("#add2basket_bigdata_select_form_<?=$arItem['ID']?>").submit(function() {
															var form = $(this);
															$(window).resize(function () {
																modalHeight = ($(window).height() - $("#<?=$arItemIDs['ID']?>").height()) / 2;
																$("#<?=$arItemIDs['ID']?>").css({
																	"top": modalHeight + "px"
																});
															});
															$(window).resize();
															$("#<?=$arItemIDs['ID']?>_body").css({"display":"block"});
															$("#<?=$arItemIDs['ID']?>").css({"display":"block"});
																			
															quantityItem = form.find("#quantity_bigdata_<?=$arItem['ID']?>").attr("value");
															$("#<?=$arItemIDs['ID']?> .quantity").attr("value", quantityItem);
															return false;
														});
														$("#<?=$arItemIDs['ID']?>_close, #<?=$arItemIDs['ID']?>_body").click(function(e){
															e.preventDefault();
															$("#<?=$arItemIDs['ID']?>_body").css({"display":"none"});
															$("#<?=$arItemIDs['ID']?>").css({"display":"none"});
														});
													});
												</script>
												<?if($arItem["CAN_BUY"]):
													if($arItem["ASK_PRICE"]):?>
														<div class="btn_avl"><?=GetMessage("CATALOG_ELEMENT_AVAILABLE")?></div>
													<?elseif(!$arItem["ASK_PRICE"]):?>
														<div class="add2basket_block">
															<form action="<?=$APPLICATION->GetCurPage()?>" id="add2basket_bigdata_select_form_<?=$arItem['ID']?>">
																<a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_bigdata_<?=$arItem["ID"]?>').value > <?=$arItem["CATALOG_MEASURE_RATIO"]?>) BX('quantity_bigdata_<?=$arItem["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arItem["ID"]?>').value)-<?=$arItem["CATALOG_MEASURE_RATIO"]?>;"></a>
																<input type="text" id="quantity_bigdata_<?=$arItem['ID']?>" name="quantity" class="quantity" value="<?=$arItem['CATALOG_MEASURE_RATIO']?>"/>
																<a href="javascript:void(0)" class="plus" onclick="BX('quantity_bigdata_<?=$arItem["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arItem["ID"]?>').value)+<?=$arItem["CATALOG_MEASURE_RATIO"]?>;"></a>
																<button type="submit" name="add2basket" class="btn_buy" value="<?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?>"><?=GetMessage("CATALOG_ELEMENT_ADD_TO_CART")?></button>
															</form>
														</div>
													<?endif;
												elseif(!$arItem["CAN_BUY"]):?>
													<div id="not_available" class="btn_navl"><?=GetMessage("CATALOG_ELEMENT_NOT_AVAILABLE")?></div>
												<?endif;
											endif;
										else:
											if($arItem["CAN_BUY"]):?>
												<?if($arItem["ASK_PRICE"]):?>
													<div class="btn_avl"><?=GetMessage("CATALOG_ELEMENT_AVAILABLE")?></div>
												<?elseif(!$arItem["ASK_PRICE"]):?>
													<div class="add2basket_block">
														<form action="<?=SITE_DIR?>ajax/add2basket.php" class="add2basket_bigdata_form">
															<a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_bigdata_<?=$arItem["ID"]?>').value > <?=$arItem["CATALOG_MEASURE_RATIO"]?>) BX('quantity_bigdata_<?=$arItem["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arItem["ID"]?>').value)-<?=$arItem["CATALOG_MEASURE_RATIO"]?>;"></a>
															<input type="text" id="quantity_bigdata_<?=$arItem['ID']?>" name="quantity" class="quantity" value="<?=$arItem['CATALOG_MEASURE_RATIO']?>"/>
															<a href="javascript:void(0)" class="plus" onclick="BX('quantity_bigdata_<?=$arItem["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arItem["ID"]?>').value)+<?=$arItem["CATALOG_MEASURE_RATIO"]?>;"></a>
															<input type="hidden" name="ID" value="<?=$arItem['ID']?>" />
															<input type="hidden" name="item_image" class="item_image" value="&lt;img class='item_image' src='<?=$arItem["PREVIEW_IMG"]["SRC"]?>' alt='<?=$arItem["NAME"]?>'/&gt;"/>
															<input type="hidden" name="item_title" class="item_title" value="<?=$arItem['NAME']?>"/>
															<input type="hidden" name="item_desc" class="item_desc" value="<?=strip_tags($arItem['PREVIEW_TEXT']);?>"/>
															<input type="hidden" name="item_props" class="item_props" value="" />
															<button type="submit" name="add2basket" class="btn_buy" value="<?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?>"><?=GetMessage("CATALOG_ELEMENT_ADD_TO_CART")?></button>
															<small class="result hidden"><?=GetMessage("CATALOG_ELEMENT_ADDED")?></small>
														</form>
													</div>
												<?endif;
											elseif(!$arItem["CAN_BUY"]):?>
												<div class="btn_navl"><?=GetMessage("CATALOG_ELEMENT_NOT_AVAILABLE")?></div>
											<?endif;
										endif;?>
									</div>
								</div>
							</div>
						</div>
					<? }
				endforeach;

				foreach($arResult["ITEMS"] as $key => $arElement):
					if((isset($arElement["OFFERS"]) && !empty($arElement["OFFERS"])) || (isset($arElement["SELECT_PROPS"]) && !empty($arElement["SELECT_PROPS"]))):
						$strMainID = $this->GetEditAreaId($arElement["ID"]);
						$arItemIDs = array(
							"ID" => $strMainID."_bigdata",
							"PICT" => $strMainID."_bigdata_picture",
							"PRICE" => $strMainID."_bigdata_price",
							"BUY" => $strMainID."_bigdata_buy",
							"PROP_DIV" => $strMainID."_bigdata_sku_tree",
							"PROP" => $strMainID."_bigdata_prop_",
							"SELECT_PROP_DIV" => $strMainID."_bigdata_propdiv",
							"SELECT_PROP" => $strMainID."_bigdata_select_prop_"
						);
						$strObName = "ob".preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);?>
						
						<div class="pop-up-bg more_options_body" id="<?=$arItemIDs['ID']?>_body"></div>
						<div class="pop-up more_options" id="<?=$arItemIDs['ID']?>">
							<div class="pop-up-close more_options_close" id="<?=$arItemIDs['ID']?>_close"></div>
							<div class="h1"><?=GetMessage("CATALOG_MORE_OPTIONS")?></div>
							<div class="item_info">
								<div class="item_image" id="<?=$arItemIDs['PICT']?>">
									<?if(isset($arElement["OFFERS"]) && !empty($arElement["OFFERS"])):
										foreach($arElement["OFFERS"] as $key => $arOffer):?>
											<div id="img_bigdata_<?=$arElement['ID']?>_<?=$arOffer['ID']?>" class="img <?=$arElement['ID']?> hidden">
												<?if(isset($arOffer["PREVIEW_IMG"])):?>
													<img src="<?=$arOffer['PREVIEW_IMG']['SRC']?>" alt="<?=$arElement['NAME']?>" width="<?=$arOffer['PREVIEW_IMG']['WIDTH']?>" height="<?=$arOffer['PREVIEW_IMG']['HEIGHT']?>"/>
												<?else:?>
													<img src="<?=$arElement['PREVIEW_IMG']['SRC']?>" width="<?=$arElement['PREVIEW_IMG']['WIDTH']?>" height="<?=$arElement['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arElement['NAME']?>"/>
												<?endif;?>
											</div>
										<?endforeach;
									else:?>
										<div class="img">
											<?if(isset($arElement["PREVIEW_IMG"])):?>
												<img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>"/>
											<?else:?>
												<img src="<?=SITE_TEMPLATE_PATH?>/images/no-photo.jpg" width="150" height="150" alt="<?=$arElement['NAME']?>" />
											<?endif;?>
										</div>
									<?endif;?>
								</div>
								<div class="item_block">
									<div class="item_name">
										<?=$arElement["NAME"]?>
									</div>
									<div class="item_options">
										<?if(!empty($arElement["OFFERS_PROP"])):?>
											<table class="offer_block" id="<?=$arItemIDs['PROP_DIV'];?>">
												<?$arSkuProps = array();
												foreach($arResult['SKU_PROPS'] as $iblockId => $skuProps) {
													foreach($skuProps as &$arProp) {
														if(!isset($arElement["OFFERS_PROP"][$arProp["CODE"]]))
															continue;
														$arSkuProps[] = array(
															"ID" => $arProp["ID"],
															"SHOW_MODE" => $arProp["SHOW_MODE"]
														);?>
														<tr class="<?=$arProp['CODE']?>" id="<?=$arItemIDs['PROP'].$arProp['ID'];?>_cont">
															<td class="h3">
																<?=htmlspecialcharsex($arProp["NAME"]);?>:
															</td>
															<td class="props">
																<ul id="<?=$arItemIDs['PROP'].$arProp['ID'];?>_list" class="<?=$arProp['CODE']?>">
																	<?foreach($arProp["VALUES"] as $arOneValue) {
																		$arOneValue["NAME"] = htmlspecialcharsbx($arOneValue["NAME"]);?>
																		<li data-treevalue="<?=$arProp['ID'].'_'.$arOneValue['ID'];?>" data-onevalue="<?=$arOneValue['ID'];?>" style="display:none;">
																			<span title="<?=$arOneValue['NAME'];?>">
																				<?if("TEXT" == $arProp["SHOW_MODE"]) {
																					echo $arOneValue["NAME"];
																				} elseif("PICT" == $arProp["SHOW_MODE"]) {
																					if(!empty($arOneValue["PICT"]["src"])):?>
																						<img src="<?=$arOneValue['PICT']['src']?>" width="<?=$arOneValue['PICT']['width']?>" height="<?=$arOneValue['PICT']['height']?>" alt="<?=$arOneValue['NAME']?>" />
																					<?else:?>
																						<i style="background:#<?=$arOneValue['HEX']?>"></i>
																					<?endif;
																				}?>
																			</span>
																		</li>
																	<?}?>
																</ul>
																<div class="bx_slide_left" style="display:none;" id="<?=$arItemIDs['PROP'].$arProp['ID']?>_left" data-treevalue="<?=$arProp['ID']?>"></div>
																<div class="bx_slide_right" style="display:none;" id="<?=$arItemIDs['PROP'].$arProp['ID']?>_right" data-treevalue="<?=$arProp['ID']?>"></div>
															</td>
														</tr>
													<?}
													unset($arProp);
												}?>
											</table>
										<?endif;?>

										<?if(!empty($arElement["SELECT_PROPS"])):?>
											<table class="offer_block" id="<?=$arItemIDs['SELECT_PROP_DIV'];?>">
												<?$arSelProps = array();
												foreach($arElement["SELECT_PROPS"] as $key => $arProp):
													$arSelProps[] = array(
														"ID" => $arProp["ID"]
													);?>
													<tr class="<?=$arProp['CODE']?>" id="<?=$arItemIDs['SELECT_PROP'].$arProp['ID'];?>">
														<td class="h3"><?=htmlspecialcharsex($arProp["NAME"]);?></td>
														<td class="props">												
															<ul class="<?=$arProp['CODE']?>">
																<?$props = array();
																foreach($arProp["DISPLAY_VALUE"] as $arOneValue) {
																	$props[$key] = array(
																		"NAME" => $arProp["NAME"],
																		"CODE" => $arProp["CODE"],
																		"VALUE" => strip_tags($arOneValue)
																	);
																	$props[$key] = strtr(base64_encode(addslashes(gzcompress(serialize($props[$key]),9))), '+/=', '-_,');?>
																	<li data-select-onevalue="<?=$props[$key]?>">
																		<span title="<?=$arOneValue;?>"><?=$arOneValue?></span>
																	</li>
																<?}?>
															</ul>
														</td>
													</tr>
												<?endforeach;
												unset($arProp);?>
											</table>
										<?endif;?>

										<div class="price_buy">
											<div class="catalog_price" id="<?=$arItemIDs['PRICE'];?>">
												<?if(isset($arElement["OFFERS"]) && !empty($arElement["OFFERS"])):
													foreach($arElement["OFFERS"] as $key => $arOffer):
														foreach($arOffer["PRICES"] as $code => $arPrice):
															if($arPrice["MIN_PRICE"] == "Y"):
																if($arPrice["CAN_ACCESS"]):
																		
																	$price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
																	if(empty($price["THOUSANDS_SEP"])):
																		$price["THOUSANDS_SEP"] = " ";
																	endif;
																	$currency = str_replace("#", " ", $price["FORMAT_STRING"]);

																	if($arPrice["VALUE"]==0):?>
																		<div id="price_bigdata_<?=$arElement['ID']?>_<?=$arOffer['ID']?>" class="price <?=$arElement['ID']?> hidden">
																			<a class="ask_price_anch" id="ask_price_anch_bigdata_<?=$arElement['ID']?>_<?=$arOffer['ID']?>" href="#"><?=GetMessage("CATALOG_ASK_PRICE")?></a>
																			<?$properties = false;
																			foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
																				$properties[] = $propOffer["NAME"].": ".strip_tags($propOffer["DISPLAY_VALUE"]);
																			}
																			$properties = implode("; ", $properties);
																			if(!empty($properties)):
																				$offer_name = $arElement["NAME"]." (".$properties.")";
																			else:
																				$offer_name = $arElement["NAME"];
																			endif;?>
																			<?$APPLICATION->IncludeComponent("altop:ask.price", "",
																				Array(
																					"ELEMENT_ID" => "bigdata_".$arElement["ID"]."_".$arOffer["ID"],	
																					"ELEMENT_NAME" => $offer_name,
																					"EMAIL_TO" => "",				
																					"REQUIRED_FIELDS" => array("NAME", "EMAIL", "TEL"),
																				),
																				false
																			);?>
																			<?if(!empty($arOffer["CATALOG_MEASURE_NAME"])):?>
																				<span class="unit">
																					<?=GetMessage("UNIT")." ".$arOffer["CATALOG_MEASURE_NAME"]?>
																				</span>
																			<?endif;?>
																		</div>
																	<?elseif($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
																		<div id="price_bigdata_<?=$arElement['ID']?>_<?=$arOffer['ID']?>" class="price <?=$arElement['ID']?> hidden">
																			<span class="price-new">
																				<?=number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																				<?=$currency;?>
																			</span>
																			<?if(!empty($arOffer["CATALOG_MEASURE_NAME"])):?>
																				<span class="unit">
																					<?=GetMessage("UNIT")." ".$arOffer["CATALOG_MEASURE_NAME"]?>
																				</span>
																			<?endif;?>
																			<span class="price-old">
																				<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																				<?=$currency;?>
																			</span>
																			<span class="price-percent">
																				<?=GetMessage("CATALOG_ELEMENT_SKIDKA")." ".$arPrice["PRINT_DISCOUNT_DIFF"];?>
																			</span>
																		</div>
																	<?else:?>
																		<div id="price_bigdata_<?=$arElement['ID']?>_<?=$arOffer['ID']?>" class="price <?=$arElement['ID']?> hidden">
																			<span class="price-full">
																				<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																				<?=$currency;?>
																			</span>
																			<?if(!empty($arOffer["CATALOG_MEASURE_NAME"])):?>
																				<span class="unit">
																					<?=GetMessage("UNIT")." ".$arOffer["CATALOG_MEASURE_NAME"]?>
																				</span>
																			<?endif;?>
																		</div>
																	<?endif;
																	
																endif;
															endif;
														endforeach;
													endforeach;
												else:
													foreach($arElement["PRICES"] as $code => $arPrice):
														if($arPrice["MIN_PRICE"] == "Y"):
															if($arPrice["CAN_ACCESS"]):
																						
																$price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
																if(empty($price["THOUSANDS_SEP"])):
																	$price["THOUSANDS_SEP"] = " ";
																endif;
																$currency = str_replace("#", " ", $price["FORMAT_STRING"]);

																if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
																	<div class="price">
																		<span class="price-new">
																			<?=number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																			<?=$currency;?>
																		</span>
																		<?if(!empty($arElement["CATALOG_MEASURE_NAME"])):?>
																			<span class="unit">
																				<?=GetMessage("UNIT")." ".$arElement["CATALOG_MEASURE_NAME"]?>
																			</span>
																		<?endif;?>
																		<span class="price-old">
																			<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																			<?=$currency;?>
																		</span>
																		<span class="price-percent">
																			<?=GetMessage("CATALOG_ELEMENT_SKIDKA")." ".$arPrice["PRINT_DISCOUNT_DIFF"];?>
																		</span>
																	</div>
																<?else:?>
																	<div class="price">
																		<span class="price-full">
																			<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
																			<?=$currency;?>
																		</span>
																		<?if(!empty($arElement["CATALOG_MEASURE_NAME"])):?>
																			<span class="unit">
																				<?=GetMessage("UNIT")." ".$arElement["CATALOG_MEASURE_NAME"]?>
																			</span>
																		<?endif;?>
																	</div>
																<?endif;
															endif;
														endif;
													endforeach;
												endif;?>
											</div>

											<div class="catalog_buy_more" id="<?=$arItemIDs['BUY'];?>">
												<?if(isset($arElement["OFFERS"]) && !empty($arElement["OFFERS"])):
													foreach($arElement["OFFERS"] as $key => $arOffer):?>
														<div id="buy_more_bigdata_<?=$arElement['ID']?>_<?=$arOffer['ID']?>" class="buy_more <?=$arElement['ID']?> hidden">
															<?if($arOffer["CAN_BUY"]):
																foreach($arOffer["PRICES"] as $code => $arPrice):
																	if($arPrice["MIN_PRICE"] == "Y"):
																		if($arPrice["VALUE"]==0):?>
																			<div class="btn_avl"><?=GetMessage("CATALOG_ELEMENT_AVAILABLE")?></div>
																		<?else:?>
																			<div class="add2basket_block">
																				<form action="<?=SITE_DIR?>ajax/add2basket.php" class="add2basket_bigdata_form">
																					<a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_bigdata_<?=$arOffer["ID"]?>').value > <?=$arOffer["CATALOG_MEASURE_RATIO"]?>) BX('quantity_bigdata_<?=$arOffer["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arOffer["ID"]?>').value)-<?=$arOffer["CATALOG_MEASURE_RATIO"]?>;"></a>
																					<input type="text" id="quantity_bigdata_<?=$arOffer['ID']?>" name="quantity" class="quantity" value="<?=$arOffer['CATALOG_MEASURE_RATIO']?>"/>
																					<a href="javascript:void(0)" class="plus" onclick="BX('quantity_bigdata_<?=$arOffer["ID"]?>').value = parseFloat(BX('quantity_bigdata_<?=$arOffer["ID"]?>').value)+<?=$arOffer["CATALOG_MEASURE_RATIO"]?>;"></a>
																					<input type="hidden" name="ID" class="offer_id" value="<?=$arOffer['ID']?>" />
																					<?$props = array();
																					foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
																						$props[] = array(
																							"NAME" => $propOffer["NAME"],
																							"CODE" => $propOffer["CODE"],
																							"VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
																						);
																					}
																					$props = strtr(base64_encode(addslashes(gzcompress(serialize($props),9))), '+/=', '-_,');?>
																					<input type="hidden" name="PROPS" value="<?=$props?>" />
																					<?if(!empty($arElement["SELECT_PROPS"])):?>
																						<input type="hidden" name="SELECT_PROPS" id="select_props_bigdata_<?=$arOffer['ID']?>" value="" />
																						<input type="hidden" name="item_select_props" id="item_select_props_bigdata_<?=$arOffer['ID']?>" value="" />
																					<?endif;?>
																					<?if(!empty($arOffer["PREVIEW_IMG"]["SRC"])):?>
																						<input type="hidden" name="item_image" class="item_image" value="&lt;img class='item_image' src='<?=$arOffer["PREVIEW_IMG"]["SRC"]?>' alt='<?=$arElement["NAME"]?>'/&gt;"/>
																					<?else:?>
																						<input type="hidden" name="item_image" class="item_image" value="&lt;img class='item_image' src='<?=$arElement["PREVIEW_IMG"]["SRC"]?>' alt='<?=$arElement["NAME"]?>'/&gt;"/>
																					<?endif;?>
																					<input type="hidden" name="item_title" class="item_title" value="<?=$arElement['NAME']?>"/>
																					<input type="hidden" name="item_desc" class="item_desc" value="<?=strip_tags($arElement['PREVIEW_TEXT']);?>"/>
																					<input type="hidden" name="item_props" class="item_props" value="
																						<?foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer): 
																							echo '&lt;span&gt;'.$propOffer["NAME"].': '.strip_tags($propOffer["DISPLAY_VALUE"]).'&lt;/span&gt;';
																						endforeach;?>
																					"/>
																					<button type="submit" name="add2basket" class="btn_buy" value="<?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?>"><?=GetMessage("CATALOG_ELEMENT_ADD_TO_CART")?></button>
																					<small class="result hidden"><?=GetMessage("CATALOG_ELEMENT_ADDED")?></small>
																				</form>
																			</div>
																		<?endif;
																	endif;
																endforeach;
															elseif(!$arOffer["CAN_BUY"]):?>
																<div class="btn_navl"><?=GetMessage("CATALOG_ELEMENT_NOT_AVAILABLE")?></div>
															<?endif;?>
														</div>
													<?endforeach;
												else:?>
													<div class="buy_more">
														<?if($arElement["CAN_BUY"]):?>
															<div class="add2basket_block">
																<form action="<?=SITE_DIR?>ajax/add2basket.php" class="add2basket_bigdata_form">
																	<a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_bigdata_select_<?=$arElement["ID"]?>').value > <?=$arElement["CATALOG_MEASURE_RATIO"]?>) BX('quantity_bigdata_select_<?=$arElement["ID"]?>').value = parseFloat(BX('quantity_bigdata_select_<?=$arElement["ID"]?>').value)-<?=$arElement["CATALOG_MEASURE_RATIO"]?>;"></a>
																	<input type="text" id="quantity_bigdata_select_<?=$arElement['ID']?>" name="quantity" class="quantity" value="<?=$arElement['CATALOG_MEASURE_RATIO']?>"/>
																	<a href="javascript:void(0)" class="plus" onclick="BX('quantity_bigdata_select_<?=$arElement["ID"]?>').value = parseFloat(BX('quantity_bigdata_select_<?=$arElement["ID"]?>').value)+<?=$arElement["CATALOG_MEASURE_RATIO"]?>;"></a>
																	<input type="hidden" name="ID" class="id" value="<?=$arElement['ID']?>" />
																	<input type="hidden" name="SELECT_PROPS" id="select_props_bigdata_<?=$arElement['ID']?>" value="" />
																	<input type="hidden" name="item_select_props" id="item_select_props_bigdata_<?=$arElement['ID']?>" value="" />
																	<input type="hidden" name="item_image" class="item_image" value="&lt;img class='item_image' src='<?=$arElement["PREVIEW_IMG"]["SRC"]?>' alt='<?=$arElement["NAME"]?>'/&gt;"/>
																	<input type="hidden" name="item_title" class="item_title" value="<?=$arElement['NAME']?>"/>
																	<input type="hidden" name="item_desc" class="item_desc" value="<?=strip_tags($arElement['PREVIEW_TEXT']);?>"/>
																	<input type="hidden" name="item_props" class="item_props" value="" />
																	<button type="submit" name="add2basket" class="btn_buy" value="<?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?>"><?=GetMessage("CATALOG_ELEMENT_ADD_TO_CART")?></button>
																	<small class="result hidden"><?=GetMessage("CATALOG_ELEMENT_ADDED")?></small>
																</form>
															</div>
														<?endif;?>
													</div>
												<?endif;?>
											</div>
										</div>										
									</div>
								</div>
							</div>
						</div>
						<?if(isset($arElement["OFFERS"]) && !empty($arElement["OFFERS"])):
							$arJSParams = array(
								"PRODUCT_TYPE" => $arElement["CATALOG_TYPE"],
								"VISUAL" => array(
									"ID" => $arItemIDs["ID"],
									"PICT_ID" => $arItemIDs["PICT"],
									"PRICE_ID" => $arItemIDs["PRICE"],
									"BUY_ID" => $arItemIDs["BUY"],
									"TREE_ID" => $arItemIDs["PROP_DIV"],
									"TREE_ITEM_ID" => $arItemIDs["PROP"]
								),
								"PRODUCT" => array(
									"ID" => $arElement["ID"],
									"NAME" => $arElement["NAME"]
								),
								"OFFERS" => $arElement["JS_OFFERS"],
								"OFFER_SELECTED" => $arElement["OFFERS_SELECTED"],
								"TREE_PROPS" => $arSkuProps
							);
						else:
							$arJSParams = array(
								"PRODUCT_TYPE" => $arElement["CATALOG_TYPE"],
								"VISUAL" => array(
									"ID" => $arItemIDs["ID"]
								),
								"PRODUCT" => array(
									"ID" => $arElement["ID"],
									"NAME" => $arElement["NAME"]
								)
							);
						endif;
						if(isset($arElement["SELECT_PROPS"]) && !empty($arElement["SELECT_PROPS"])):
							$arJSParams["VISUAL"]["SELECT_PROP_ID"] = $arItemIDs["SELECT_PROP_DIV"];
							$arJSParams["VISUAL"]["SELECT_PROP_ITEM_ID"] = $arItemIDs["SELECT_PROP"];
							$arJSParams["SELECT_PROPS"] = $arSelProps;
						endif;?>
						<script type="text/javascript">
							var <?=$strObName;?> = new JCCatalogBigdataProducts(<?=CUtil::PhpToJSObject($arJSParams, false, true);?>);
						</script>
					<?endif;
				endforeach;?>
			</div>
			<div class="clr"></div>
		</div>
	</div>
<?endif;

$frame->end();?>