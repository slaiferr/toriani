<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->AddHeadScript('/bitrix/components/altop/ask.price/templates/.default/script.js');
$APPLICATION->SetAdditionalCSS('/bitrix/components/altop/ask.price/templates/.default/style.css');

$APPLICATION->AddHeadScript('/bitrix/components/altop/buy.one.click/templates/.default/script.js');
$APPLICATION->SetAdditionalCSS('/bitrix/components/altop/buy.one.click/templates/.default/style.css');

$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/components/bitrix/iblock.vote/ajax/style.css');

$APPLICATION->AddHeadScript('/bitrix/components/altop/catalog.comments/templates/.default/script.js');
$APPLICATION->SetAdditionalCSS('/bitrix/components/altop/catalog.comments/templates/.default/style.css');

if(isset($templateData['JS_OBJ'])):?>
	<script type="text/javascript">
		BX.ready(BX.defer(function(){
			if(!!window.<?=$templateData['JS_OBJ'];?>) {
				window.<?=$templateData['JS_OBJ'];?>.allowViewedCount(true);
			}
		}));
	</script>
<?endif;

if(!empty($arParams["IBLOCK_ID_REVIEWS"])):
	$arResult["REVIEWS"]["IBLOCK_ID"] = $arParams["IBLOCK_ID_REVIEWS"];
else:
	$res = CIBlock::GetList(
		Array(),
		Array("TYPE" => "catalog", "SITE_ID" => SITE_ID, "ACTIVE" => "Y", "CODE" => "comments_".SITE_ID),
		true
	);
	$reviews_iblock = $res->Fetch();
	$arResult["REVIEWS"]["IBLOCK_ID"] = $reviews_iblock["ID"];
endif?>

<div id="catalog-reviews-from" style="display:none;">
	<?$APPLICATION->IncludeComponent("altop:catalog.comments", "",
		Array(
			"OBJECT_ID" => $arResult["ID"],
			"OBJECT_NAME" => $arResult["NAME"],
			"IBLOCK_TYPE" => "catalog",
			"COMMENTS_IBLOCK_ID" => $arResult["REVIEWS"]["IBLOCK_ID"],
			"AUTH_PATH" => SITE_DIR."personal/profile/",
			"PROPERTY_OBJECT_ID" => "OBJECT_ID",
			"PROPERTY_USER_ID" => "USER_ID",
			"PROPERTY_IP_COMMENTOR" => "USER_IP",
			"PROPERTY_URL" => "COMMENT_URL",
			"SHOW_DATE" => "Y",
			"NO_FOLLOW" => "Y",
			"NON_AUTHORIZED_USER_CAN_COMMENT" => "Y",
			"PRE_MODERATION" => "Y",
			"USE_CAPTCHA" => "Y"
		),
		false
	);?>
</div>