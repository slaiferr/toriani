<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?><div class="pop-up-bg leaveapplication_body"></div>
			<div class="pop-up leaveapplication">
				<a href="#" class="pop-up-close leaveapplication_close"></a>
<div class="h1">Хотите дешевле?</div>
				<?$APPLICATION->IncludeComponent(
	"altop:leaveapplication", 
	".default", 
	array(
		"EMAIL_TO" => "info@toriani.ru",
		"REQUIRED_FIELDS" => array(
			0 => "FIRST_NAME",
			1 => "PHONE",
			2 => "EMAIL",
		)
	),
	false
);?>
			</div>
<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function() {
		$("#catalog-reviews-from").appendTo("#catalog-reviews-to").css({"display":"block"});
		$(".add2basket_form").submit(function() {
			var form = $(this);

			imageItem = form.find(".item_image").attr("value");
			$("#addItemInCart .item_image_full").html(imageItem);

			titleItem = form.find(".item_title").attr("value");
			$("#addItemInCart .item_title").text(titleItem);

			descItem = form.find(".item_desc").attr("value");
			$("#addItemInCart .item_desc").text(descItem);
			
			propsItem = form.find(".item_props").attr("value");
			offer_id = form.find(".offer_id").attr("value");
			if(!!offer_id && 0 < offer_id.length) {
				propsSelectItem = form.find("#item_select_props_"+offer_id).attr("value");
				if(!!propsSelectItem && 0 < propsSelectItem.length) {
					propsItem = propsItem + propsSelectItem;
				}
			}
			id = form.find(".id").attr("value");
			if(!!id && 0 < id.length) {
				propsSelectItem = form.find("#item_select_props_"+id).attr("value");
				if(!!propsSelectItem && 0 < propsSelectItem.length) {
					propsItem = propsItem + propsSelectItem;
				}
			}
			$("#addItemInCart .item_props").html(propsItem);
								
			countItem = form.find(".quantity").attr("value");
			$("#addItemInCart .item_count").text(countItem);

			var ModalName = $("#addItemInCart");
			CentriredModalWindow(ModalName);
			OpenModalWindow(ModalName);

			$.post($(this).attr("action"), $(this).serialize(), function(data) {
				try {
					$.post("/ajax/basket_line.php", function(data) {
						$("#cart_line").replaceWith(data);
					});
					$.post("/ajax/delay_line.php", function(data) {
						$("#delay").replaceWith(data);
					});
					form.children(".btn_buy").addClass("hidden");
					form.children(".result").removeClass("hidden");
				} catch (e) {}
			});
			return false;
		});
		$(function() {
			$("div.catalog-detail-pictures a").fancybox({
				"transitionIn": "elastic",
				"transitionOut": "elastic",
				"speedIn": 600,
				"speedOut": 200,
				"overlayShow": false,
				"cyclic" : true,
				"padding": 20,
				"titlePosition": "over",
				"onComplete": function() {
					$("#fancybox-title").css({"top":"100%", "bottom":"auto"});
				} 
			});
		});
	});
	//]]>
</script>

<?$strMainID = $this->GetEditAreaId($arResult["ID"]);
$arItemIDs = array(
	"ID" => $strMainID,
	"PICT" => $strMainID."_picture",
	"PRICE" => $strMainID."_price",
	"BUY" => $strMainID."_buy",
	"DELAY" => $strMainID."_delay",
	"STORE" => $strMainID."_store",
	"PROP_DIV" => $strMainID."_skudiv",
	"PROP" => $strMainID."_prop_",
	"SELECT_PROP_DIV" => $strMainID."_propdiv",
	"SELECT_PROP" => $strMainID."_select_prop_",
);
$strObName = "ob".preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData["JS_OBJ"] = $strObName;

$sticker = "";
if(array_key_exists("PROPERTIES", $arResult) && is_array($arResult["PROPERTIES"])) {
	if(array_key_exists("NEWPRODUCT", $arResult["PROPERTIES"]) && !$arResult["PROPERTIES"]["NEWPRODUCT"]["VALUE"] == false) {
		$sticker .= "<img class='new' src='".SITE_TEMPLATE_PATH."/images/new.png' alt='newproduct' />";
	}
	if(array_key_exists("SALELEADER", $arResult["PROPERTIES"]) && !$arResult["PROPERTIES"]["SALELEADER"]["VALUE"] == false) {
		$sticker .= "<img class='hit' src='".SITE_TEMPLATE_PATH."/images/hit.png' alt='saleleader' />";
	}
	if(array_key_exists("DISCOUNT", $arResult["PROPERTIES"]) && !$arResult["PROPERTIES"]["DISCOUNT"]["VALUE"] == false) {
		$sticker .= "<img class='discount' src='".SITE_TEMPLATE_PATH."/images/discount.png' alt='discount' />";
	}
}?>

<div id="<?=$arItemIDs['ID']?>" class="catalog-detail" itemscope itemtype="http://schema.org/Product">
	<meta content="<?=$arResult['NAME']?>" itemprop="name" />
	<meta content="<?=$arResult['NAME']?>" property="og:title" />
	<table class="catalog-detail">
		<tr>
			<td class="first">
				<div class="catalog-detail-pictures">
					<div class="catalog-detail-picture" id="<?=$arItemIDs['PICT']?>">
						<?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
							foreach($arResult["OFFERS"] as $key => $arOffer):?>
								<div id="detail_picture_<?=$arOffer['ID']?>" class="detail_picture <?=$arResult['ID']?> hidden">
									<?if(is_array($arOffer["DETAIL_IMG"])):?>
										<meta content="<?=$arOffer['DETAIL_PICTURE']['SRC']?>" property="og:image" />
										<a rel="" class="catalog-detail-images" id="catalog-detail-images-<?=$arOffer['ID']?>" href="<?=$arOffer['DETAIL_PICTURE']['SRC']?>"> 
											<img src="<?=$arOffer['DETAIL_IMG']['SRC']?>" width="<?=$arOffer['DETAIL_IMG']['WIDTH']?>" height="<?=$arOffer['DETAIL_IMG']['HEIGHT']?>" alt="<?=$arResult['NAME']?>" />
											<div class="sticker">
												<?=$sticker?>
											</div>
											<?if(!empty($arResult["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])):?>
												<img class="manufacturer" src="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC']?>" width="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH']?>" height="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arResult['PROPERTIES']['MANUFACTURER']['NAME']?>" />
											<?endif;?>
										</a>
									<?else:?>
										<meta content="<?=$arResult['DETAIL_PICTURE']['SRC']?>" property="og:image" />
										<a rel="" class="catalog-detail-images" id="catalog-detail-images-<?=$arOffer['ID']?>" href="<?=$arResult['DETAIL_PICTURE']['SRC']?>"> 
											<img src="<?=$arResult['DETAIL_IMG']['SRC']?>" width="<?=$arResult['DETAIL_IMG']['WIDTH']?>" height="<?=$arResult['DETAIL_IMG']['HEIGHT']?>" alt="<?=$arResult['NAME']?>" />
											<div class="sticker">
												<?=$sticker?>
											</div>
											<?if(!empty($arResult["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])):?>
												<img class="manufacturer" src="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC']?>" width="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH']?>" height="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arResult['PROPERTIES']['MANUFACTURER']['NAME']?>" />
											<?endif;?>
										</a>
									<?endif;?>
								</div>
							<?endforeach;
						else:
							if(is_array($arResult["DETAIL_IMG"])):?>
								<div class="detail_picture">
									<meta content="<?=$arResult['DETAIL_PICTURE']['SRC']?>" property="og:image" />
									<a rel="lightbox" class="catalog-detail-images" href="<?=$arResult['DETAIL_PICTURE']['SRC']?>"> 
										<img src="<?=$arResult['DETAIL_IMG']['SRC']?>" width="<?=$arResult['DETAIL_IMG']['WIDTH']?>" height="<?=$arResult['DETAIL_IMG']['HEIGHT']?>" alt="<?=$arResult['NAME']?>" />
										<div class="sticker">
											<?=$sticker?>
										</div>
										<?if(!empty($arResult["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])):?>
											<img class="manufacturer" src="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC']?>" width="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH']?>" height="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arResult['PROPERTIES']['MANUFACTURER']['NAME']?>" />
										<?endif;?>
									</a>
								</div>
							<?else:?>
								<div class="detail_picture">
									<meta content="<?=SITE_TEMPLATE_PATH?>/images/no-photo.jpg" property="og:image" />
									<div class="catalog-detail-images">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/no-photo.jpg" width="150" height="150" alt="<?=$arResult['NAME']?>" />
										<div class="sticker">
											<?=$sticker?>
										</div>
										<?if(!empty($arResult["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])):?>
											<img class="manufacturer" src="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC']?>" width="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH']?>" height="<?=$arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arResult['PROPERTIES']['MANUFACTURER']['NAME']?>" />
										<?endif;?>
									</div>
								</div>
							<?endif;
						endif;?>
					</div>
					<?if(!empty($arResult["PROPERTIES"]["VIDEO"]) || count($arResult["MORE_PHOTO"])>0):?>
						<div class="clr"></div>
						<div class="more_photo">
							<ul>
								<?if(!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])):?>
									<li class="catalog-detail-video">
										<a rel="lightbox" class="catalog-detail-images" href="#video">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/video.jpg" width="81" height="81" alt="<?=$arResult['NAME']?>" />
										</a>
										<div id="video">
											<?=$arResult["PROPERTIES"]["VIDEO"]["~VALUE"]["TEXT"];?>
										</div>
									</li>
								<?endif;
								if(count($arResult["MORE_PHOTO"]) > 0):
									foreach($arResult["MORE_PHOTO"] as $PHOTO):?>
										<li>
											<meta content="<?=$PHOTO['SRC']?>" property="og:image" />
											<a rel="lightbox" class="catalog-detail-images" href="<?=$PHOTO['SRC']?>">
												<img src="<?=$PHOTO['PREVIEW']['SRC']?>" width="<?=$PHOTO['PREVIEW']['WIDTH']?>" height="<?=$PHOTO['PREVIEW']['HEIGHT']?>" alt="<?=$arResult['NAME']?>" />
											</a>
										</li>
									<?endforeach;
								endif;?>
							</ul>
						</div>
					<?endif?>
				</div>
			</td>
			<td style="padding:0px 0px 0px 20px;">
				<div class="price_buy_detail" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
					<div class="catalog-detail-price" id="<?=$arItemIDs['PRICE'];?>">
						<?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
							foreach($arResult["OFFERS"] as $key => $arOffer):
								foreach($arOffer["PRICES"] as $code => $arPrice):
									if($arPrice["MIN_PRICE"] == "Y"):
										if($arPrice["CAN_ACCESS"]):
												
											$price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
											if(empty($price["THOUSANDS_SEP"])):
												$price["THOUSANDS_SEP"] = " ";
											endif;
											$currency = str_replace("#", " ", $price["FORMAT_STRING"]);

											if($arPrice["VALUE"] == 0):?>
												<meta content="<?=GetMessage('CATALOG_ASK_PRICE')?>" itemprop="price" />
												<div id="detail_price_<?=$arOffer['ID']?>" class="detail_price <?=$arResult['ID']?> hidden">
													<a class="ask_price_anch" id="ask_price_anch_<?=$arOffer['ID']?>" href="#"><?=GetMessage('CATALOG_ASK_PRICE')?></a>
													<?$properties = false;
													foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
														$properties[] = $propOffer["NAME"].": ".strip_tags($propOffer["DISPLAY_VALUE"]);
													}
													$properties = implode("; ", $properties);
													if(!empty($properties)):
														$offer_name = $arResult["NAME"]." (".$properties.")";
													else:
														$offer_name = $arResult["NAME"];
													endif;?>
													<?$APPLICATION->IncludeComponent("altop:ask.price", "",
														Array(
															"ELEMENT_ID" => $arOffer["ID"],		
															"ELEMENT_NAME" => $offer_name,
															"EMAIL_TO" => "",				
															"REQUIRED_FIELDS" => array("NAME", "EMAIL", "TEL")
														),
														false,
														array("HIDE_ICONS" => "Y")
													);?>
													<?if(!empty($arOffer["CATALOG_MEASURE_NAME"])):?>
														<span class="unit">
															<?=GetMessage('UNIT')." ".$arOffer["CATALOG_MEASURE_NAME"]?>
														</span>
													<?endif;?>
												</div>
											<?elseif($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
												<div id="detail_price_<?=$arOffer['ID']?>" class="detail_price <?=$arResult['ID']?> hidden">
													<span class="catalog-detail-item-price-new" itemprop="price">
														<?=number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
														<?="<span style='font-size:15px; font-weight:normal;'>".$currency."</span>";?>
													</span>
													<?if(!empty($arOffer["CATALOG_MEASURE_NAME"])):?>
														<span class="unit">
															<?=GetMessage('UNIT')." ".$arOffer["CATALOG_MEASURE_NAME"]?>
														</span>
													<?endif;?>
													<span class="catalog-detail-item-price-old">
														<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
														<?=$currency;?>
													</span>
													<span class="catalog-detail-item-price-percent">
														<?=GetMessage('CATALOG_ELEMENT_SKIDKA')." ".$arPrice["PRINT_DISCOUNT_DIFF"];?>
													</span>
												</div>
											<?else:?>
												<div id="detail_price_<?=$arOffer['ID']?>" class="detail_price <?=$arResult['ID']?> hidden">
													<span class="catalog-detail-item-price" itemprop="price">
														<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
														<?="<span style='font-size:15px; font-weight:normal;'>".$currency."</span>";?>
													</span>
													<?if(!empty($arOffer["CATALOG_MEASURE_NAME"])):?>
														<span class="unit">
															<?=GetMessage('UNIT')." ".$arOffer["CATALOG_MEASURE_NAME"]?>
														</span>
													<?endif;?>
												</div>
											<?endif;
										endif;
									endif;
								endforeach;
							endforeach;
						else:
							foreach($arResult["PRICES"] as $code => $arPrice):
								if($arPrice["MIN_PRICE"] == "Y"):
									if($arPrice["CAN_ACCESS"]):
												
										$price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
										if(empty($price["THOUSANDS_SEP"])):
											$price["THOUSANDS_SEP"] = " ";
										endif;
										$currency = str_replace("#", " ", $price["FORMAT_STRING"]);

										if($arPrice["VALUE"] == 0):
											$arResult["ASK_PRICE"]=1;?>
											<meta content="<?=GetMessage('CATALOG_ASK_PRICE')?>" itemprop="price" />
											<div style="float:left;">
												<a class="ask_price_anch" id="ask_price_anch_<?=$arResult['ID']?>" href="#"><?=GetMessage('CATALOG_ASK_PRICE')?></a>
												<?$APPLICATION->IncludeComponent("altop:ask.price", "",
													Array(
														"ELEMENT_ID" => $arResult["ID"],		
														"ELEMENT_NAME" => $arResult["NAME"],
														"EMAIL_TO" => "",				
														"REQUIRED_FIELDS" => array("NAME", "EMAIL", "TEL")
													),
													false
												);?>
												<?if(!empty($arResult["CATALOG_MEASURE_NAME"])):?>
													<span class="unit">
														<?=GetMessage('UNIT')." ".$arResult["CATALOG_MEASURE_NAME"]?>
													</span>
												<?endif;?>
<a class="leaveapplication_anch" id="leaveapplication_anch_<?=$arResult['ID']?>" href="#">Хотите дешевле?</a>
											</div>
										<?elseif($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
											<div style="float:left;">	
												<span class="catalog-detail-item-price-new" itemprop="price">
													<?=number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
													<?="<span style='font-size:15px; font-weight:normal;'>".$currency."</span>";?>
												</span>
												<?if(!empty($arResult["CATALOG_MEASURE_NAME"])):?>
													<span class="unit">
														<?=GetMessage('UNIT')." ".$arResult["CATALOG_MEASURE_NAME"]?>
													</span>
												<?endif;?>
												<span class="catalog-detail-item-price-old">
													<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
													<?=$currency;?>
												</span>
												<span class="catalog-detail-item-price-percent">
													<?=GetMessage('CATALOG_ELEMENT_SKIDKA')." ".$arPrice["PRINT_DISCOUNT_DIFF"];?>
												</span>
<a class="leaveapplication_anch" id="leaveapplication_anch_<?=$arResult['ID']?>" href="#">Хотите дешевле?</a>
											</div>
										<?else:?>
											<span class="catalog-detail-item-price" itemprop="price">
												<?=number_format($arPrice["VALUE"], $price["DECIMALS"], $price["DEC_POINT"], $price["THOUSANDS_SEP"]);?>
												<?="<span style='font-size:15px; font-weight:normal;'>".$currency."</span>";?>
											</span>
											<?if(!empty($arResult["CATALOG_MEASURE_NAME"])):?>
												<span class="unit">
													<?=GetMessage('UNIT')." ".$arResult["CATALOG_MEASURE_NAME"]?>
												</span>
											<?endif;?>
<a class="leaveapplication_anch" id="leaveapplication_anch_<?=$arResult['ID']?>" href="#">Хотите дешевле?</a>
										<?endif;
									endif;
								endif;
							endforeach;
						endif;?>
					</div>
					<div class="catalog-detail-buy" id="<?=$arItemIDs['BUY'];?>">
						<?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
							foreach($arResult["OFFERS"] as $key => $arOffer):?>
								<div id="buy_more_detail_<?=$arOffer['ID']?>" class="buy_more_detail <?=$arResult['ID']?> hidden">
									<?if($arOffer["CAN_BUY"]):?>
										<meta content="InStock" itemprop="availability" />
										<?foreach($arOffer["PRICES"] as $code => $arPrice):
											if($arPrice["MIN_PRICE"] == "Y"):
												if($arPrice["VALUE"] == 0):?>
													<div class="btn_avl detail"><?=GetMessage('CATALOG_ELEMENT_AVAILABLE')?></div>
												<?else:?>
													<div class="add2basket_block">
														<form action="<?=SITE_DIR?>ajax/add2basket.php" class="add2basket_form" id="add2basket_form_<?=$arOffer['ID']?>">
															<a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_<?=$arOffer["ID"]?>').value > <?=$arOffer["CATALOG_MEASURE_RATIO"]?>) BX('quantity_<?=$arOffer["ID"]?>').value = parseFloat(BX('quantity_<?=$arOffer["ID"]?>').value)-<?=$arOffer["CATALOG_MEASURE_RATIO"]?>;"></a>
															<input type="text" id="quantity_<?=$arOffer['ID']?>" name="quantity" class="quantity" value="<?=$arOffer['CATALOG_MEASURE_RATIO']?>"/>
															<a href="javascript:void(0)" class="plus" onclick="BX('quantity_<?=$arOffer["ID"]?>').value = parseFloat(BX('quantity_<?=$arOffer["ID"]?>').value)+<?=$arOffer["CATALOG_MEASURE_RATIO"]?>;"></a>
															<input type="hidden" name="ID" class="offer_id" value="<?=$arOffer['ID']?>" />
															<?$props = array();
															foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
																$props[] = array(
																	"NAME" => $propOffer["NAME"],
																	"CODE" => $propOffer["CODE"],
																	"VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
																);
															}
															$props = strtr(base64_encode(addslashes(gzcompress(serialize($props),9))), '+/=', '-_,');?>
															<input type="hidden" name="PROPS" value="<?=$props?>" />
															<?if(!empty($arResult["SELECT_PROPS"])):?>
																<input type="hidden" name="SELECT_PROPS" id="select_props_<?=$arOffer['ID']?>" value="" />
																<input type="hidden" name="item_select_props" id="item_select_props_<?=$arOffer['ID']?>" value="" />
															<?endif;?>
															<?if(!empty($arOffer["PREVIEW_IMG"]["SRC"])):?>							
																<input type="hidden" name="item_image" class="item_image" value="&lt;img class='item_image' src='<?=$arOffer["PREVIEW_IMG"]["SRC"]?>' alt='<?=$arResult["NAME"]?>'/&gt;"/>
															<?else:?>
																<input type="hidden" name="item_image" class="item_image" value="&lt;img class='item_image' src='<?=$arResult["PREVIEW_IMG"]["SRC"]?>' alt='<?=$arResult["NAME"]?>'/&gt;"/>
															<?endif;?>
															<input type="hidden" name="item_title" class="item_title" value="<?=$arResult['NAME']?>"/>
															<input type="hidden" name="item_desc" class="item_desc" value="<?=strip_tags($arResult['PREVIEW_TEXT']);?>"/>
															<input type="hidden" name="item_props" class="item_props" value="
																<?foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer): 
																	echo '&lt;span&gt;'.$propOffer["NAME"].': '.strip_tags($propOffer["DISPLAY_VALUE"]).'&lt;/span&gt;';
																endforeach;?>
															"/>
															<button type="submit" name="add2basket" class="btn_buy detail" value="<?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?>"><?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?></button>
															<small class="result detail hidden"><?=GetMessage('CATALOG_ELEMENT_ADDED')?></small>
														</form>
													</div>
													<div class="clr"></div>
													<button name="boc_anch" id="boc_anch_<?=$arOffer['ID']?>" class="btn_buy boc_anch" value="<?=GetMessage('CATALOG_ELEMENT_BOC')?>"><?=GetMessage('CATALOG_ELEMENT_BOC')?></button>
													<?$APPLICATION->IncludeComponent("altop:buy.one.click", ".default", 
														array(
															"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
															"IBLOCK_ID" => $arParams["IBLOCK_ID"],
															"ELEMENT_ID" => $arOffer["ID"],
															"ELEMENT_PROPS" => $props,
															"REQUIRED_ORDER_FIELDS" => array(
																0 => "NAME",
																1 => "TEL",
															),
															"DEFAULT_PERSON_TYPE" => "1",
															"DEFAULT_DELIVERY" => "0",
															"DEFAULT_PAYMENT" => "0",
															"DEFAULT_CURRENCY" => "RUB",
															"BUY_MODE" => "ONE",
															"PRICE_ID" => "1",
															"DUPLICATE_LETTER_TO_EMAILS" => array(
																0 => "a",
															),
														),
														false,
														array("HIDE_ICONS" => "Y")
													);?>
												<?endif;
											endif;
										endforeach;
									elseif(!$arOffer["CAN_BUY"]):?>
										<meta content="OutOfStock" itemprop="availability" />
										<div class="btn_navl detail"><?=GetMessage('CATALOG_ELEMENT_NOT_AVAILABLE')?></div>
									<?endif;?>
								</div>
							<?endforeach;
						else:?>
							<div class="buy_more_detail">
								<?if($arResult["CAN_BUY"]):?>
									<meta content="InStock" itemprop="availability" />
									<?if($arResult["ASK_PRICE"]):?>
										<div class="btn_avl detail"><?=GetMessage('CATALOG_ELEMENT_AVAILABLE')?></div>
									<?elseif(!$arResult["ASK_PRICE"]):?>
										<div class="add2basket_block">
											<form action="<?=SITE_DIR?>ajax/add2basket.php" class="add2basket_form" id="add2basket_form_<?=$arResult['ID']?>">
												<a href="javascript:void(0)" class="minus" onclick="if (BX('quantity_<?=$arResult["ID"]?>').value > <?=$arResult["CATALOG_MEASURE_RATIO"]?>) BX('quantity_<?=$arResult["ID"]?>').value = parseFloat(BX('quantity_<?=$arResult["ID"]?>').value)-<?=$arResult["CATALOG_MEASURE_RATIO"]?>;"></a>
												<input type="text" id="quantity_<?=$arResult['ID']?>" name="quantity" class="quantity" value="<?=$arResult['CATALOG_MEASURE_RATIO']?>"/>
												<a href="javascript:void(0)" class="plus" onclick="BX('quantity_<?=$arResult["ID"]?>').value = parseFloat(BX('quantity_<?=$arResult["ID"]?>').value)+<?=$arResult["CATALOG_MEASURE_RATIO"]?>;"></a>
												<input type="hidden" name="ID" class="id" value="<?=$arResult['ID']?>" />
												<?if(!empty($arResult["SELECT_PROPS"])):?>
													<input type="hidden" name="SELECT_PROPS" id="select_props_<?=$arResult['ID']?>" value="" />
													<input type="hidden" name="item_select_props" id="item_select_props_<?=$arResult['ID']?>" value="" />
												<?endif;?>												
												<input type="hidden" name="item_image" class="item_image" value="&lt;img class='item_image' src='<?=$arResult["PREVIEW_IMG"]["SRC"]?>' alt='<?=$arResult["NAME"]?>'/&gt;"/>
												<input type="hidden" name="item_title" class="item_title" value="<?=$arResult['NAME']?>"/>
												<input type="hidden" name="item_desc" class="item_desc" value="<?=strip_tags($arResult['PREVIEW_TEXT']);?>"/>
												<input type="hidden" name="item_props" class="item_props" value="" />
												<button type="submit" name="add2basket" class="btn_buy detail" value="<?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?>"><?=GetMessage('CATALOG_ELEMENT_ADD_TO_CART')?></button>
												<small class="result detail hidden"><?=GetMessage('CATALOG_ELEMENT_ADDED')?></small>
											</form>
										</div>
										<div class="clr"></div>
										<button name="boc_anch" id="boc_anch_<?=$arResult['ID']?>" class="btn_buy boc_anch" value="<?=GetMessage('CATALOG_ELEMENT_BOC')?>"><?=GetMessage('CATALOG_ELEMENT_BOC')?></button>
										<?$APPLICATION->IncludeComponent("altop:buy.one.click", ".default", 
											array(
												"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
												"IBLOCK_ID" => $arParams["IBLOCK_ID"],
												"ELEMENT_ID" => $arResult["ID"],
												"ELEMENT_PROPS" => "",
												"REQUIRED_ORDER_FIELDS" => array(
													0 => "NAME",
													1 => "TEL",
												),
												"DEFAULT_PERSON_TYPE" => "1",
												"DEFAULT_DELIVERY" => "0",
												"DEFAULT_PAYMENT" => "0",
												"DEFAULT_CURRENCY" => "RUB",
												"BUY_MODE" => "ONE",
												"PRICE_ID" => "1",
												"DUPLICATE_LETTER_TO_EMAILS" => array(
													0 => "a",
												),
											),
											false
										);?>
									<?endif;?>
								<?elseif(!$arResult["CAN_BUY"]):?>
									<meta content="OutOfStock" itemprop="availability" />
									<div class="btn_navl detail"><?=GetMessage('CATALOG_ELEMENT_NOT_AVAILABLE')?></div>
								<?endif;?>
							</div>
						<?endif;?>
					</div>
				</div>
				<div class="clr"></div>
				<div class="rating_compare_detail">
					<div class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
						<?$APPLICATION->IncludeComponent("bitrix:iblock.vote", "ajax",
							Array(
								"DISPLAY_AS_RATING" => "vote_avg",
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"ELEMENT_ID" => $arResult["ID"],
								"ELEMENT_CODE" => "",
								"MAX_VOTE" => "5",
								"VOTE_NAMES" => array("1","2","3","4","5"),
								"SET_STATUS_404" => "N",
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_NOTES" => "",
								"READ_ONLY" => "N"
							),
							false
						);?>
						<?if($arResult["PROPERTIES"]["vote_count"]["VALUE"]):?>
							<meta content="<?=round($arResult['PROPERTIES']['vote_sum']['VALUE']/$arResult['PROPERTIES']['vote_count']['VALUE'], 2);?>" itemprop="ratingValue" />
						<?else:?>
							<meta content="0" itemprop="ratingValue" />
						<?endif;?>
						<?if($arResult["PROPERTIES"]["vote_count"]["VALUE"]):?>
							<meta content="<?=$arResult['PROPERTIES']['vote_count']['VALUE']?>" itemprop="ratingCount" />
						<?else:?>
							<meta content="0" itemprop="ratingCount" />
						<?endif;?>
					</div>
					<?if($arParams["USE_COMPARE"]=="Y"):?>
						<div id="catalog-compare">
							<a href="javascript:void(0)" class="catalog-item-compare" id="catalog_add2compare_link_<?=$arResult['ID']?>" onclick="return addToCompare('<?=$arResult["COMPARE_URL"]?>', 'catalog_add2compare_link_<?=$arResult["ID"]?>');" rel="nofollow">
								<span class="add"><?=GetMessage('CATALOG_ELEMENT_ADD_TO_COMPARE')?></span>
								<span class="added"><?=GetMessage('CATALOG_ELEMENT_ADDED')?></span>
							</a>
						</div>
					<?endif;?>
					<div class="catalog-detail-delay" id="<?=$arItemIDs['DELAY']?>">
						<?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
							foreach($arResult["OFFERS"] as $key => $arOffer):
								if($arOffer["CAN_BUY"]):
									foreach($arOffer["PRICES"] as $code => $arPrice):
										if($arPrice["MIN_PRICE"] == "Y"):
											if($arPrice["VALUE"] > 0):
												$props = array();
												foreach($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
													$props[] = array(
														"NAME" => $propOffer["NAME"],
														"CODE" => $propOffer["CODE"],
														"VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
													);
												}
												$props = strtr(base64_encode(addslashes(gzcompress(serialize($props),9))), '+/=', '-_,');?>
												<div id="add_to_delay_<?=$arOffer['ID']?>" class="add_to_delay <?=$arResult['ID']?> hidden">
													<a href="javascript:void(0)" id="catalog-item-delay-<?=$arOffer['ID']?>" class="catalog-item-delay" onclick="return addToDelay('<?=$arOffer["ID"]?>', '<?=$arOffer["CATALOG_MEASURE_RATIO"]?>', '<?=$props?>', '', 'catalog-item-delay-<?=$arOffer["ID"]?>')" rel="nofollow">
														<span class="add"><?=GetMessage('CATALOG_ELEMENT_ADD_TO_DELAY')?></span>
														<span class="added"><?=GetMessage('CATALOG_ELEMENT_ADDED')?></span>
													</a>
												</div>
											<?endif;
										endif;
									endforeach;
								endif;
							endforeach;
						else:
							if($arResult["CAN_BUY"]):
								foreach($arResult["PRICES"] as $code=>$arPrice):
									if($arPrice["MIN_PRICE"] == "Y"):
										if($arPrice["VALUE"] > 0):?>
											<div class="add_to_delay">
												<a href="javascript:void(0)" id="catalog-item-delay-<?=$arResult['ID']?>" class="catalog-item-delay" onclick="return addToDelay('<?=$arResult["ID"]?>', '<?=$arResult["CATALOG_MEASURE_RATIO"]?>', '', '', 'catalog-item-delay-<?=$arResult["ID"]?>')" rel="nofollow">
													<span class="add"><?=GetMessage('CATALOG_ELEMENT_ADD_TO_DELAY')?></span>
													<span class="added"><?=GetMessage('CATALOG_ELEMENT_ADDED')?></span>
												</a>
											</div>
										<?endif;
									endif;
								endforeach;
							endif;
						endif?>
					</div>
<div class="price-down">
					</div>
				</div>
				<div class="catalog-detail-line"></div>
				<?if(!empty($arResult["PREVIEW_TEXT"])):?>
					<meta content="<?=strip_tags($arResult['PREVIEW_TEXT'])?>" property="og:description" />
					<div class="catalog-detail-preview-text" itemprop="description">
						<?=$arResult["PREVIEW_TEXT"]?>
					</div>
				<?endif;?>
				
				<?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"]) && !empty($arResult["OFFERS_PROP"])) {
					$arSkuProps = array();?>
					<div class="catalog-detail-offers" id="<?=$arItemIDs['PROP_DIV'];?>">
						<?foreach($arResult["SKU_PROPS"] as &$arProp) {
							if(!isset($arResult["OFFERS_PROP"][$arProp["CODE"]]))
								continue;
							$arSkuProps[] = array(
								"ID" => $arProp["ID"],
								"SHOW_MODE" => $arProp["SHOW_MODE"]
							);?>
							<div class="offer_block" id="<?=$arItemIDs['PROP'].$arProp['ID'];?>_cont">
								<div class="h3"><?=htmlspecialcharsex($arProp["NAME"]);?></div>
								<ul id="<?=$arItemIDs['PROP'].$arProp['ID'];?>_list" class="<?=$arProp['CODE']?>">
									<?foreach($arProp["VALUES"] as $arOneValue) {
										$arOneValue["NAME"] = htmlspecialcharsbx($arOneValue["NAME"]);?>
										<li data-treevalue="<?=$arProp['ID'].'_'.$arOneValue['ID'];?>" data-onevalue="<?=$arOneValue['ID'];?>" style="display:none;">
											<span title="<?=$arOneValue['NAME'];?>">
												<?if("TEXT" == $arProp["SHOW_MODE"]) {
													echo $arOneValue["NAME"];
												} elseif("PICT" == $arProp["SHOW_MODE"]) {
													if(!empty($arOneValue["PICT"]["src"])):?>
														<img src="<?=$arOneValue['PICT']['src']?>" width="<?=$arOneValue['PICT']['width']?>" height="<?=$arOneValue['PICT']['height']?>" alt="<?=$arOneValue['NAME']?>" />
													<?else:?>
														<i style="background:#<?=$arOneValue['HEX']?>"></i>
													<?endif;
												}?>
											</span>
										</li>
									<?}?>
								</ul>
								<div class="bx_slide_left" style="display:none;" id="<?=$arItemIDs['PROP'].$arProp['ID']?>_left" data-treevalue="<?=$arProp['ID']?>"></div>
								<div class="bx_slide_right" style="display:none;" id="<?=$arItemIDs['PROP'].$arProp['ID']?>_right" data-treevalue="<?=$arProp['ID']?>"></div>
							</div>
						<?}
						unset($arProp);?>
					</div>
				<?}?>
				
				<?if(isset($arResult["SELECT_PROPS"]) && !empty($arResult["SELECT_PROPS"])):
					$arSelProps = array();?>
					<div class="catalog-detail-offers" id="<?=$arItemIDs['SELECT_PROP_DIV'];?>">
						<?foreach($arResult["SELECT_PROPS"] as $key => $arProp):
							$arSelProps[] = array(
								"ID" => $arProp["ID"]
							);?>
							<div class="offer_block" id="<?=$arItemIDs['SELECT_PROP'].$arProp['ID'];?>">
								<div class="h3"><?=htmlspecialcharsex($arProp["NAME"]);?></div>
								<ul class="<?=$arProp['CODE']?>">
									<?$props = array();
									foreach($arProp["DISPLAY_VALUE"] as $arOneValue) {
										$props[$key] = array(
											"NAME" => $arProp["NAME"],
											"CODE" => $arProp["CODE"],
											"VALUE" => strip_tags($arOneValue)
										);
										$props[$key] = strtr(base64_encode(addslashes(gzcompress(serialize($props[$key]),9))), '+/=', '-_,');?>
										<li data-select-onevalue="<?=$props[$key]?>">
											<span title="<?=$arOneValue;?>"><?=$arOneValue?></span>
										</li>
									<?}?>
								</ul>
							</div>
						<?endforeach;
						unset($arProp);?>
					</div>
				<?endif;?>
										
				<?if(!empty($arResult["DISPLAY_PROPERTIES"])):?>
					<div class="catalog-detail-properties">
						<div class="h4"><?=GetMessage('CATALOG_PROPERTIES')?></div>
						<?foreach($arResult["DISPLAY_PROPERTIES"] as $k => $v):?>
							<div class="catalog-detail-property">
								<span><?=$v["NAME"]?></span> 
								<b>
									<?=is_array($v["DISPLAY_VALUE"]) ? implode(", ", $v["DISPLAY_VALUE"]) : $v["DISPLAY_VALUE"];?>
								</b>
							</div>
						<?endforeach;?>
					</div>
				<?endif;?>				
			</td>
		</tr>
	</table>
	<div class="section">
		<ul class="tabs">
			<li class="current">
				<a href="#tab1">
					<?=GetMessage("CATALOG_FULL_DESCRIPTION")?>
				</a>
			</li>
			<li style="<?if(empty($arResult["PROPERTIES"]["FREE_TAB"]["VALUE"])){ echo 'display:none;'; }?>">
				<a href="#tab2">
					<?=$arResult["PROPERTIES"]["FREE_TAB"]["NAME"]?>
				</a>
			</li>
			<li style="<?if(empty($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"])){ echo 'display:none;'; }?>">
				<a href="#tab3">
					<?=$arResult["PROPERTIES"]["ACCESSORIES"]["NAME"]?>
				</a>
			</li>
			<li>
				<a href="#tab4">
					<?=GetMessage("CATALOG_REVIEWS");?> <span class="reviews_count">(<?=$arResult["REVIEWS"]["COUNT"]?>)</span>
				</a>
			</li>
			<li>
				<a href="#tab5">
					<?=GetMessage("CATALOG_SHOPS")?>
				</a>
			</li>
		</ul>
		<div class="box visible">
			<div class="description">
				<?=$arResult["DETAIL_TEXT"];?>
			</div>
		</div>
		<div class="box" style="<?if(empty($arResult["PROPERTIES"]["FREE_TAB"]["VALUE"])){ echo 'display:none;'; }?>">
			<div class="tab-content">
				<?=$arResult["PROPERTIES"]["FREE_TAB"]["~VALUE"]["TEXT"];?>
			</div>
		</div>
		<div class="box" style="<?if(empty($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"])){ echo 'display:none;'; }?>">
			<div class="accessories">
				<?$arProperty = $arResult["PROPERTIES"]["ACCESSORIES"]["PROPERTY_VALUE_ID"];
				if($arProperty !=""): 
					global $arRecPrFilter;
					$arRecPrFilter["ID"] = $arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"];
					$APPLICATION->IncludeComponent("altop:catalog.top", "access",
						Array(
							"DISPLAY_IMG_WIDTH" => $arParams["DISPLAY_IMG_WIDTH"],
							"DISPLAY_IMG_HEIGHT" => $arParams["DISPLAY_IMG_HEIGHT"],
							"SHARPEN" => $arParams["SHARPEN"],
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => "rand",
							"ELEMENT_SORT_ORDER" => "asc",
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"ELEMENT_COUNT" => "8",
							"LINE_ELEMENT_COUNT" => "",
							"FILTER_NAME" => "arRecPrFilter",
							"PROPERTY_CODE" => array("NEWPRODUCT", "SALELEADER", "DISCOUNT", "MANUFACTURER"),
							"PROPERTY_CODE_MOD" => $arParams["PROPERTY_CODE_MOD"],
							"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"HIDE_NOT_AVAILABLE" => "N",
							"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
							"CURRENCY_ID" => $arParams['CURRENCY_ID'],
							"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"]
						),
						false,
						array("HIDE_ICONS" => "Y")
					);
					unset($arResult["PROPERTIES"]["ACCESSORIES"]);
				endif;?>
			</div>
		</div>
		<div class="box" id="catalog-reviews-to"></div>
		<div class="box">
			<div id="<?=$arItemIDs['STORE'];?>">
				<?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
					foreach($arResult["OFFERS"] as $key => $arOffer):?>
						<div id="catalog-detail-stores-<?=$arOffer['ID']?>" class="catalog-detail-stores <?=$arResult['ID']?> hidden">
							<?$APPLICATION->IncludeComponent("bitrix:catalog.store.amount",	".default",
								array(
									"ELEMENT_ID" => $arOffer["ID"],
									"STORE_PATH" => $arParams["STORE_PATH"],
									"CACHE_TYPE" => $arParams["CACHE_TYPE"],
									"CACHE_TIME" => $arParams["CACHE_TIME"],
									"MAIN_TITLE" => $arParams["MAIN_TITLE"],
									"USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
									"SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
									"USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
									"MIN_AMOUNT" => $arParams["MIN_AMOUNT"]
								),
								false,
								array("HIDE_ICONS" => "Y")
							);?>
						</div>
					<?endforeach;
				else:?>
					<div class="catalog-detail-stores">
						<?$APPLICATION->IncludeComponent("bitrix:catalog.store.amount",	".default",
							array(
								"ELEMENT_ID" => $arResult["ID"],
								"STORE_PATH" => $arParams["STORE_PATH"],
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"MAIN_TITLE" => $arParams["MAIN_TITLE"],
								"USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
								"SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
								"USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
								"MIN_AMOUNT" => $arParams["MIN_AMOUNT"]
							),
							false,
							array("HIDE_ICONS" => "Y")
						);?>
					</div>
				<?endif;?>
			</div>
		</div>
	</div>
	<div class="clr"></div>
</div>

<?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) {
	$arJSParams = array(
		"CONFIG" => array(
			"USE_CATALOG" => $arResult["CATALOG"],
		),
		"PRODUCT_TYPE" => $arResult["CATALOG_TYPE"],
		"VISUAL" => array(
			"ID" => $arItemIDs["ID"],
			"PICT_ID" => $arItemIDs["PICT"],
			"PRICE_ID" => $arItemIDs["PRICE"],
			"BUY_ID" => $arItemIDs["BUY"],
			"DELAY_ID" => $arItemIDs["DELAY"],
			"STORE_ID" => $arItemIDs["STORE"],
			"TREE_ID" => $arItemIDs["PROP_DIV"],
			"TREE_ITEM_ID" => $arItemIDs["PROP"],
		),
		"PRODUCT" => array(
			"ID" => $arResult["ID"],
			"NAME" => $arResult["~NAME"]
		),
		"OFFERS" => $arResult["JS_OFFERS"],
		"OFFER_SELECTED" => $arResult["OFFERS_SELECTED"],
		"TREE_PROPS" => $arSkuProps
	);
} else {
	$arJSParams = array(
		"CONFIG" => array(
			"USE_CATALOG" => $arResult["CATALOG"]
		),
		"PRODUCT_TYPE" => $arResult["CATALOG_TYPE"],	
		"VISUAL" => array(
			"ID" => $arItemIDs["ID"],
		),
		"PRODUCT" => array(
			"ID" => $arResult["ID"],
			"NAME" => $arResult["~NAME"]
		)
	);	
}

if(isset($arResult["SELECT_PROPS"]) && !empty($arResult["SELECT_PROPS"])) {
	$arJSParams["VISUAL"]["SELECT_PROP_ID"] = $arItemIDs["SELECT_PROP_DIV"];
	$arJSParams["VISUAL"]["SELECT_PROP_ITEM_ID"] = $arItemIDs["SELECT_PROP"];
	$arJSParams["SELECT_PROPS"] = $arSelProps;
}?>

<script type="text/javascript">
	var <?=$strObName;?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($arJSParams, false, true);?>);
	BX.message({
		SITE_ID: "<?=SITE_ID;?>"
	});
</script>
<?if(!empty($arResult["PROPERTIES"]["COLECTION"]["VALUE"])):?>
<div class="similar">
<?global $arRelPrFilter;
$arCol = $arResult["PROPERTIES"]["COLECTION"]["VALUE"];
$arRelPrFilter = array("PROPERTY_COLECTION_VALUE" => $arCol);
$APPLICATION->IncludeComponent("altop:catalog.top", "related",
	Array(
		"DISPLAY_IMG_WIDTH" => $arParams["DISPLAY_IMG_WIDTH"],
		"DISPLAY_IMG_HEIGHT" => $arParams["DISPLAY_IMG_HEIGHT"],
		"SHARPEN" => $arParams["SHARPEN"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => "rand",
		"ELEMENT_SORT_ORDER" => "asc",
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"ELEMENT_COUNT" => "4",
		"LINE_ELEMENT_COUNT" => "",
		"FILTER_NAME" => "arRelPrFilter",
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"PROPERTY_CODE_MOD" => $arParams["PROPERTY_CODE_MOD"],
		"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
		"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"]
	),
	false,
	array("HIDE_ICONS" => "Y")
);?>
</div>
<?endif;?>