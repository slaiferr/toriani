<?
$MESS ['CATALOG_CHARACTERISTICS_LABEL'] = "Показывать";
$MESS ['CATALOG_ONLY_DIFFERENT'] = "Только различающиеся";
$MESS ['CATALOG_ONLY_DIFFERENT_MOBILE'] = "Различающиеся";
$MESS ['CATALOG_ALL_CHARACTERISTICS'] = "Все характеристики";
$MESS ['CATALOG_ALL_CHARACTERISTICS_MOBILE'] = "Все";
$MESS ['CATALOG_REMOVE_PRODUCT'] = "Убрать из сравнения";
$MESS ['CATALOG_ELEMENT_SKIDKA'] = "Экономия";
$MESS ['UNIT'] = "за";
$MESS ['CATALOG_ASK_PRICE'] = "Запросить цену";
$MESS ['CATALOG_ELEMENT_ADD_TO_DELAY'] = "Отложить товар";
$MESS ['CATALOG_ELEMENT_ADD_TO_CART'] = "В корзину";
$MESS ['CATALOG_ELEMENT_ADDED'] = "Добавлено";
$MESS ['CATALOG_ELEMENT_AVAILABLE'] = "В наличии";
$MESS ['CATALOG_ELEMENT_NOT_AVAILABLE'] = "Нет в наличии";
$MESS ['CATALOG_DELETE_ALL'] = "Удалить все товары из сравнения";
$MESS ['CATALOG_MORE_OPTIONS'] = "Выберите дополнительные параметры товара";
?>