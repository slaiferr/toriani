<?
$MESS ['CATALOG_ELEMENT_SKIDKA'] = "Экономия";
$MESS ['UNIT'] = "за";
$MESS ['CATALOG_ASK_PRICE'] = "Запросить цену";
$MESS ['CATALOG_ELEMENT_ADD_TO_COMPARE'] = "Сравнить товар";
$MESS ['CATALOG_ELEMENT_ADD_TO_DELAY'] = "Отложить товар";
$MESS ['CATALOG_ELEMENT_ADD_TO_CART'] = "Купить";
$MESS ['CATALOG_ELEMENT_ADDED'] = "Добавлено";
$MESS ['CATALOG_ELEMENT_AVAILABLE'] = "В наличии";
$MESS ['CATALOG_ELEMENT_NOT_AVAILABLE'] = "Нет в наличии";
$MESS ['CATALOG_ELEMENT_BOC'] = "Купить в 1 клик";
$MESS ['CATALOG_PROPERTIES'] = "Характеристики";
$MESS ['CATALOG_FULL_DESCRIPTION'] = "Описание";
$MESS ['CATALOG_REVIEWS'] = "Отзывы";
$MESS ['CATALOG_SHOPS'] = "Наличие в <br /> магазинах";
?>