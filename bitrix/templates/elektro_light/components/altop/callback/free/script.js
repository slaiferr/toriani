function button_callback(path, template, email_to, required) {
    $.ajax({
        type: "POST",
        url : path+'/component.php',
        data: ({
            NAME			: $("#callback_name").val(),
            TEL				: $("#callback_tel").val(),
			URL			: $("#callback_url").val(),
			QUESTION		: $("#callback_question").val(),
			captcha_word	: $("#callback_captcha_word").val(),
            captcha_sid		: $("#callback_captcha_sid").val(),
			METHOD			: $("#callback_method").val(),
			PATH			: path,
            TEMPLATE		: template,
			EMAIL_TO		: email_to,
			REQUIRED		: required
        }),
        success: function (html) {
            $('#echo_callback_form_free').html(html);
        }
    });
}