<?
$MESS ['MFT_NAME'] = "Имя";
$MESS ['MFT_TEL'] = "Телефон";
$MESS ['MFT_URL'] = "Ссылка";
$MESS ['MFT_QUESTION'] = "Вопрос";
$MESS ['MFT_CAPTCHA'] = "Код с картинки";
$MESS ['MFT_ORDER'] = "Заказать";

$MESS ['NAME_NOT_FILLED'] = "Не заполнено поле «Имя»";
$MESS ['TEL_NOT_FILLED'] = "Не заполнено поле «Телефон»";
$MESS ['URL_NOT_FILLED'] = "Не заполнено поле «Сылка»";
$MESS ['QUESTION_NOT_FILLED'] = "Не заполнено поле «Вопрос»";
$MESS ['WRONG_CAPTCHA'] = "Неверно введен «Код с картинки»";

$MESS ['MF_MESSAGE_INFO'] = "Информационное сообщение сайта";
$MESS ['MF_MESSAGE_ZAKAZ'] = "Осуществлен заказ обратного звонка";
$MESS ['MF_MESSAGE_NAME'] = "Имя:";
$MESS ['MF_MESSAGE_TEL'] = "Телефон:";
$MESS ['MF_MESSAGE_URL'] = "Ссылка:";
$MESS ['MF_MESSAGE_QUESTION'] = "Вопрос:";
$MESS ['MF_MESSAGE_GENERAT'] = "Сообщение сгенерировано автоматически.";
$MESS ['MF_MESSAGE_CALLBACK_ZAKAZ'] = "Заказ обратного звонка";
$MESS ['MF_OK_MESSAGE'] = "Спасибо, ваше сообщение принято!";

$MESS["URL"] = "Укажите ссылку на страницу, где вы нашли товар дешевле";
?>