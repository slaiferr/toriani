<?php
function promo($items) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $promo = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < $items; $i++) {
        $n = rand(0, $alphaLength);
        $promo[] = $alphabet[$n];
    }
    return implode($promo);
}

if(isset($_SESSION['PROMO'])){
	$_SESSION['PROMO'] = $_SESSION['PROMO'];
}else{
	$_SESSION['PROMO'] = promo(6); // Передаем значение о количестве знаков в промо-коде
}
?>

<div class="overlay"></div>
	<div class="popup">
		<div class="close_window"><i class="fa fa-times"></i></div>
		<div class="popup_content">
			<p>Сообщите менеджеру свой уникальный код и узнайте размер Вашей скидки!</p>
			<p>Ваш Промо-код: <b class="promo">ториани</b></p>
			<p>Звоните по телефону 8 (812) 407-37-32</p>
		</div>
	</div>
<?/*=$_SESSION['PROMO']*/?>
<div class="open_window"></div>