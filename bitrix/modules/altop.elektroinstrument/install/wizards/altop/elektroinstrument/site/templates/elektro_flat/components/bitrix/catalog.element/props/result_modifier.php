<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Type\Collection,
	Bitrix\Iblock,
	Bitrix\Currency\CurrencyTable;

$arSetting = CElektroinstrument::GetFrontParametrsValues(SITE_ID);
	
//PREVIEW_PICTURE//	
if(is_array($arResult["PREVIEW_PICTURE"])) {
	if($arResult["PREVIEW_PICTURE"]["WIDTH"] > $arParams["DISPLAY_IMG_WIDTH"] || $arResult["PREVIEW_PICTURE"]["HEIGHT"] > $arParams["DISPLAY_IMG_HEIGHT"]) {
		$arFileTmp = CFile::ResizeImageGet(
			$arResult["PREVIEW_PICTURE"],
			array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true
		);
		$arResult["PREVIEW_PICTURE"] = array(
			"SRC" => $arFileTmp["src"],
			"WIDTH" => $arFileTmp["width"],
			"HEIGHT" => $arFileTmp["height"]
		);
	}
} elseif(is_array($arResult["DETAIL_PICTURE"])) {
	if($arResult["DETAIL_PICTURE"]["WIDTH"] > $arParams["DISPLAY_IMG_WIDTH"] || $arResult["DETAIL_PICTURE"]["HEIGHT"] > $arParams["DISPLAY_IMG_HEIGHT"]) {
		$arFileTmp = CFile::ResizeImageGet(
			$arResult["DETAIL_PICTURE"],
			array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true
		);
		$arResult["PREVIEW_PICTURE"] = array(
			"SRC" => $arFileTmp["src"],
			"WIDTH" => $arFileTmp["width"],
			"HEIGHT" => $arFileTmp["height"]
		);
	} else {
		$arResult["PREVIEW_PICTURE"] = $arResult["DETAIL_PICTURE"];
	}
}

//MIN_PRICE//
$arResult["MIN_PRICE"] = $arResult["ITEM_PRICES"][$arResult["ITEM_PRICE_SELECTED"]];

//CHECK_QUANTITY//
$arResult["CHECK_QUANTITY"] = $arResult["CATALOG_QUANTITY_TRACE"] == "Y" && $arResult["CATALOG_CAN_BUY_ZERO"] == "N";

//SELECT_PROPS//
if(is_array($arParams["PROPERTY_CODE_MOD"]) && !empty($arParams["PROPERTY_CODE_MOD"])) {
	$arResult["SELECT_PROPS"] = array();
	foreach($arParams["PROPERTY_CODE_MOD"] as $pid) {
		if(!isset($arResult["PROPERTIES"][$pid]))
			continue;
		$prop = &$arResult["PROPERTIES"][$pid];
		$boolArr = is_array($prop["VALUE"]);
		if($prop["MULTIPLE"] == "Y" && $boolArr && !empty($prop["VALUE"])) {
			$arResult["SELECT_PROPS"][$pid] = CIBlockFormatProperties::GetDisplayValue($arResult, $prop, "catalog_out");
			if(!is_array($arResult["SELECT_PROPS"][$pid]["DISPLAY_VALUE"]) && !empty($arResult["SELECT_PROPS"][$pid]["DISPLAY_VALUE"])) {
				$arTmp = $arResult["SELECT_PROPS"][$pid]["DISPLAY_VALUE"];
				unset($arResult["SELECT_PROPS"][$pid]["DISPLAY_VALUE"]);
				$arResult["SELECT_PROPS"][$pid]["DISPLAY_VALUE"][0] = $arTmp;
			}
		} elseif($prop["MULTIPLE"] == "N" && !$boolArr) {
			if($prop["PROPERTY_TYPE"] == "L") {
				$arResult["SELECT_PROPS"][$pid] = $prop;
				$property_enums = CIBlockPropertyEnum::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $pid));
				while($enum_fields = $property_enums->GetNext()) {
					$arResult["SELECT_PROPS"][$pid]["DISPLAY_VALUE"][] = $enum_fields["VALUE"];
				}
			}
		}
	}
}

//OFFERS//
if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) {	
	foreach($arResult["OFFERS"] as $keyOffer => $arOffer) {		
		//PREVIEW_PICTURE//
		if(is_array($arOffer["PREVIEW_PICTURE"])) {
			if($arOffer["PREVIEW_PICTURE"]["WIDTH"] > $arParams["DISPLAY_IMG_WIDTH"] || $arOffer["PREVIEW_PICTURE"]["HEIGHT"] > $arParams["DISPLAY_IMG_HEIGHT"]) {
				$arFileTmp = CFile::ResizeImageGet(
					$arOffer["PREVIEW_PICTURE"],
					array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					true
				);
				$arResult["OFFERS"][$keyOffer]["PREVIEW_PICTURE"] = array(
					"SRC" => $arFileTmp["src"],
					"WIDTH" => $arFileTmp["width"],
					"HEIGHT" => $arFileTmp["height"]
				);
			}
		} elseif(is_array($arOffer["DETAIL_PICTURE"])) {
			if($arOffer["DETAIL_PICTURE"]["WIDTH"] > $arParams["DISPLAY_IMG_WIDTH"] || $arOffer["DETAIL_PICTURE"]["HEIGHT"] > $arParams["DISPLAY_IMG_HEIGHT"]) {
				$arFileTmp = CFile::ResizeImageGet(
					$arOffer["DETAIL_PICTURE"],
					array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					true
				);
				$arResult["OFFERS"][$keyOffer]["PREVIEW_PICTURE"] = array(
					"SRC" => $arFileTmp["src"],
					"WIDTH" => $arFileTmp["width"],
					"HEIGHT" => $arFileTmp["height"]
				);
			} else {
				$arResult["OFFERS"][$keyOffer]["PREVIEW_PICTURE"] = $arOffer["DETAIL_PICTURE"];
			}
		}
		//PREVIEW_PICTURE//

		//MIN_PRICE//
		if(count($arOffer["ITEM_QUANTITY_RANGES"]) > 1 && $arSetting["OFFERS_VIEW"] == "LIST") {			
			$minPrice = false;
			foreach($arOffer["ITEM_PRICES"] as $itemPrice) {
				if($itemPrice["RATIO_PRICE"] == 0)
					continue;
				if($minPrice === false || $minPrice > $itemPrice["RATIO_PRICE"]) {								
					$minPrice = $itemPrice["RATIO_PRICE"];					
					$arResult["OFFERS"][$keyOffer]["MIN_PRICE"] = array(		
						"RATIO_BASE_PRICE" => $itemPrice["RATIO_BASE_PRICE"],
						"PRINT_RATIO_BASE_PRICE" => $itemPrice["PRINT_RATIO_BASE_PRICE"],
						"RATIO_PRICE" => $minPrice,						
						"PRINT_RATIO_DISCOUNT" => $itemPrice["PRINT_RATIO_DISCOUNT"],
						"PERCENT" => $itemPrice["PERCENT"],
						"CURRENCY" => $itemPrice["CURRENCY"],
						"PERCENT" => $arOffer["ITEM_PRICES"][$arOffer["ITEM_PRICE_SELECTED"]]["PERCENT"],
						"MIN_QUANTITY" => $arOffer["ITEM_PRICES"][$arOffer["ITEM_PRICE_SELECTED"]]["MIN_QUANTITY"]
					);
				}
			}
			if($minPrice === false) {
				$arResult["OFFERS"][$keyOffer]["MIN_PRICE"] = array(
					"RATIO_PRICE" => "0",
					"CURRENCY" => $arOffer["ITEM_PRICES"][$arOffer["ITEM_PRICE_SELECTED"]]["CURRENCY"]
				);
			}
		} else {
			$arResult["OFFERS"][$keyOffer]["MIN_PRICE"] = $arOffer["ITEM_PRICES"][$arOffer["ITEM_PRICE_SELECTED"]];
		}
	}	
}
//END_OFFERS//

//PROPERTIES_JS_OFFERS//
$arParams["OFFER_TREE_PROPS"] = $arParams["OFFERS_PROPERTY_CODE"];
if(!is_array($arParams["OFFER_TREE_PROPS"]))
	$arParams["OFFER_TREE_PROPS"] = array($arParams["OFFER_TREE_PROPS"]);
foreach($arParams["OFFER_TREE_PROPS"] as $key => $value) {
	$value = (string)$value;
	if("" == $value || "-" == $value)
		unset($arParams["OFFER_TREE_PROPS"][$key]);
}
if(empty($arParams["OFFER_TREE_PROPS"]) && isset($arParams["OFFERS_CART_PROPERTIES"]) && is_array($arParams["OFFERS_CART_PROPERTIES"])) {
	$arParams["OFFER_TREE_PROPS"] = $arParams["OFFERS_CART_PROPERTIES"];
	foreach($arParams["OFFER_TREE_PROPS"] as $key => $value) {
		$value = (string)$value;
		if("" == $value || "-" == $value)
			unset($arParams["OFFER_TREE_PROPS"][$key]);
	}
}

$arSKUPropList = array();
$arSKUPropIDs = array();
$arSKUPropKeys = array();
$boolSKU = false;

if($arResult["MODULES"]["catalog"]) {
	$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams["IBLOCK_ID"]);
	$boolSKU = !empty($arSKU) && is_array($arSKU);

	if($boolSKU && !empty($arParams["OFFER_TREE_PROPS"])) {
		$arSKUPropList = CIBlockPriceTools::getTreeProperties(
			$arSKU,
			$arParams["OFFER_TREE_PROPS"],
			array()
		);
		$arSKUPropIDs = array_keys($arSKUPropList);
	}
}

if($arResult["MODULES"]["catalog"]) {
	$arResult["CATALOG"] = true;
	if(!isset($arResult["CATALOG_TYPE"]))
		$arResult["CATALOG_TYPE"] = CCatalogProduct::TYPE_PRODUCT;
	if((CCatalogProduct::TYPE_PRODUCT == $arResult["CATALOG_TYPE"] || CCatalogProduct::TYPE_SKU == $arResult["CATALOG_TYPE"]) && !empty($arResult["OFFERS"])) {
		$arResult["CATALOG_TYPE"] = CCatalogProduct::TYPE_SKU;
	}
	switch($arResult["CATALOG_TYPE"]) {
		case CCatalogProduct::TYPE_SET:
			$arResult["OFFERS"] = array();
			break;
		case CCatalogProduct::TYPE_SKU:
			break;
		case CCatalogProduct::TYPE_PRODUCT:
		default:
			break;
	}
} else {
	$arResult["CATALOG_TYPE"] = 0;
	$arResult["OFFERS"] = array();
}

if($arResult["CATALOG"] && isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) {
	$arResultSKUPropIDs = array();
	$arFilterProp = array();
	$arNeedValues = array();
	foreach($arResult["OFFERS"] as $arOffer) {
		foreach($arSKUPropIDs as $strOneCode) {
			if(isset($arOffer["DISPLAY_PROPERTIES"][$strOneCode])) {
				$arResultSKUPropIDs[$strOneCode] = true;
				if(!isset($arNeedValues[$arSKUPropList[$strOneCode]["ID"]]))
					$arNeedValues[$arSKUPropList[$strOneCode]["ID"]] = array();
				$valueId = (
					$arSKUPropList[$strOneCode]["PROPERTY_TYPE"] == Iblock\PropertyTable::TYPE_LIST
					? $arOffer["DISPLAY_PROPERTIES"][$strOneCode]["VALUE_ENUM_ID"]
					: $arOffer["DISPLAY_PROPERTIES"][$strOneCode]["VALUE"]
				);
				$arNeedValues[$arSKUPropList[$strOneCode]["ID"]][$valueId] = $valueId;
				unset($valueId);
				if(!isset($arFilterProp[$strOneCode]))
					$arFilterProp[$strOneCode] = $arSKUPropList[$strOneCode];
			}
		}
		unset($strOneCode);
	}
	unset($arOffer);

	CIBlockPriceTools::getTreePropertyValues($arSKUPropList, $arNeedValues);
	
	if($arSetting["OFFERS_VIEW"] == "LIST") {
		 $propertyIterator = Iblock\PropertyTable::getList(array(
			"select" => array(
				"ID", "IBLOCK_ID", "CODE", "NAME", "SORT", "LINK_IBLOCK_ID", "PROPERTY_TYPE", "USER_TYPE", "USER_TYPE_SETTINGS"
			),
            "filter" => array(
                "=IBLOCK_ID" => $arSKU["IBLOCK_ID"],
                "=PROPERTY_TYPE" => array(
					Iblock\PropertyTable::TYPE_STRING
                ),
				"=ACTIVE" => "Y", "=MULTIPLE" => "N"
			),
			"order" => array(
				"SORT" => "ASC", "ID" => "ASC"
			)
        ));
        while($propInfo = $propertyIterator->fetch()) {			
			if(!in_array($propInfo["CODE"], $arParams["OFFER_TREE_PROPS"]))
				continue;			
			$arSKUPropList[$propInfo["CODE"]] = $propInfo;
			$arSKUPropList[$propInfo["CODE"]]["VALUES"] = array();
			$arSKUPropList[$propInfo["CODE"]]["SHOW_MODE"] = "TEXT";
			$arSKUPropList[$propInfo["CODE"]]["DEFAULT_VALUES"] = array(
				"PICT" => false,
				"NAME" => "-"
			);
		}
	}
	
	$arSKUPropIDs = array_keys($arSKUPropList);
	$arSKUPropKeys = array_fill_keys($arSKUPropIDs, false);

	$arMatrixFields = $arSKUPropKeys;
	$arMatrix = array();
	
	$arNewOffers = array();
	
	$arResult["OFFERS_PROP"] = false;
	
	$arDouble = array();	
	foreach($arResult["OFFERS"] as $keyOffer => $arOffer) {
		$arOffer["ID"] = (int)$arOffer["ID"];
		if(isset($arDouble[$arOffer["ID"]]))
			continue;
		$arRow = array();
		foreach($arSKUPropIDs as $propkey => $strOneCode) {			
			$arCell = array(
				"VALUE" => 0,
				"SORT" => PHP_INT_MAX,
				"NA" => true
			);			
			if(isset($arOffer["DISPLAY_PROPERTIES"][$strOneCode])) {
				$arMatrixFields[$strOneCode] = true;
				$arCell["NA"] = false;
				if("directory" == $arSKUPropList[$strOneCode]["USER_TYPE"]) {
					$intValue = $arSKUPropList[$strOneCode]["XML_MAP"][$arOffer["DISPLAY_PROPERTIES"][$strOneCode]["VALUE"]];
					$arCell["VALUE"] = $intValue;
				} elseif("L" == $arSKUPropList[$strOneCode]["PROPERTY_TYPE"]) {
					$arCell["VALUE"] = (int)$arOffer["DISPLAY_PROPERTIES"][$strOneCode]["VALUE_ENUM_ID"];
				} elseif("E" == $arSKUPropList[$strOneCode]["PROPERTY_TYPE"]) {
					$arCell["VALUE"] = (int)$arOffer["DISPLAY_PROPERTIES"][$strOneCode]["VALUE"];
				} elseif("S" == $arSKUPropList[$strOneCode]["PROPERTY_TYPE"]) {
					$arCell["VALUE"] = (int)$arOffer["DISPLAY_PROPERTIES"][$strOneCode]["PROPERTY_VALUE_ID"];					
				}
				$arCell["SORT"] = $arSKUPropList[$strOneCode]["VALUES"][$arCell["VALUE"]]["SORT"];
			}
			$arRow[$strOneCode] = $arCell;
		}
		$arMatrix[$keyOffer] = $arRow;
		
		$arDouble[$arOffer["ID"]] = true;
		$arNewOffers[$keyOffer] = $arOffer;
	}
	$arResult["OFFERS"] = $arNewOffers;
	
	$arUsedFields = array();
	$arSortFields = array();
	
	foreach($arSKUPropIDs as $propkey => $strOneCode) {
		$boolExist = $arMatrixFields[$strOneCode];
		foreach($arMatrix as $keyOffer => $arRow) {
			if($boolExist) {
				if(!isset($arResult["OFFERS"][$keyOffer]["TREE"]))
					$arResult["OFFERS"][$keyOffer]["TREE"] = array();
				$arResult["OFFERS"][$keyOffer]["TREE"]["PROP_".$arSKUPropList[$strOneCode]["ID"]] = $arMatrix[$keyOffer][$strOneCode]["VALUE"];
				$arResult["OFFERS"][$keyOffer]["SKU_SORT_".$strOneCode] = $arMatrix[$keyOffer][$strOneCode]["SORT"];
				$arUsedFields[$strOneCode] = true;
				$arSortFields["SKU_SORT_".$strOneCode] = SORT_NUMERIC;
			} else {
				unset($arMatrix[$keyOffer][$strOneCode]);
			}
		}
	}
	$arResult["OFFERS_PROP"] = $arUsedFields;
	
	if($arParams["OFFERS_SORT_FIELD3"] == "PROPERTIES" || $arSetting["OFFERS_VIEW"] != "LIST")
		Collection::sortByColumn($arResult["OFFERS"], $arSortFields);	
	
	$intSelected = -1;
	$minRatioPrice = false;
	foreach($arResult["OFFERS"] as $keyOffer => $arOffer) {
		foreach($arOffer["ITEM_PRICES"] as $itemPrice) {
			if($itemPrice["RATIO_PRICE"] == 0)
				continue;		
			if($minRatioPrice === false || $minRatioPrice > $itemPrice["RATIO_PRICE"]) {
				$intSelected = $keyOffer;
				$minRatioPrice = $itemPrice["RATIO_PRICE"];
			}
		}
	}
	$arMatrix = array();
	foreach($arResult["OFFERS"] as $keyOffer => $arOffer) {		
		$arOneRow = array(
			"ID" => $arOffer["ID"],
			"NAME" => $arOffer["~NAME"],
			"PREVIEW_IMG" => $arOffer["PREVIEW_IMG"],
			"TREE" => $arOffer["TREE"],
			"ITEM_PRICE_MODE" => $arOffer["ITEM_PRICE_MODE"],
			"ITEM_PRICES" => $arOffer["ITEM_PRICES"],
			"ITEM_PRICE_SELECTED" => $arOffer["ITEM_PRICE_SELECTED"],
			"ITEM_QUANTITY_RANGES" => $arOffer["ITEM_QUANTITY_RANGES"],
			"ITEM_QUANTITY_RANGE_SELECTED" => $arOffer["ITEM_QUANTITY_RANGE_SELECTED"],	
			"CHECK_QUANTITY" => $arOffer["CHECK_QUANTITY"],
			"MAX_QUANTITY" => $arOffer["CATALOG_QUANTITY"],
			"STEP_QUANTITY" => $arOffer["CATALOG_MEASURE_RATIO"],
			"QUANTITY_FLOAT" => is_double($arOffer["CATALOG_MEASURE_RATIO"]),
			"CAN_BUY" => $arOffer["CAN_BUY"]
		);
		$arMatrix[$keyOffer] = $arOneRow;		
	}
	if(-1 == $intSelected)
		$intSelected = 0;
	$arResult["JS_OFFERS"] = $arMatrix;
	$arResult["OFFERS_SELECTED"] = $intSelected;
	
	$arResult["OFFERS_IBLOCK"] = $arSKU["IBLOCK_ID"];
}

//SKU_PROPS_PICT//
$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_HEX", "PROPERTY_PICT");
foreach($arSKUPropList as $key => $arSKUProp) {
	if($arSKUProp["SHOW_MODE"] == "PICT") {		
		$arSkuID = array();
		foreach($arSKUProp["VALUES"] as $key2 => $arSKU) {
			if($arSKU["ID"] > 0)
				$arSkuID[] = $arSKU["ID"];
		}
		$arFilter = array("IBLOCK_ID" => $arSKUProp["LINK_IBLOCK_ID"], "ID" => $arSkuID);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if(!empty($arFields["PROPERTY_HEX_VALUE"]))
				$arSKUPropList[$key]["VALUES"][$arFields["ID"]]["HEX"] = $arFields["PROPERTY_HEX_VALUE"];
			if($arFields["PROPERTY_PICT_VALUE"] > 0) {
				$arFile = CFile::GetFileArray($arFields["PROPERTY_PICT_VALUE"]);
				if($arFile["WIDTH"] > 24 || $arFile["HEIGHT"] > 24) {
					$arFileTmp = CFile::ResizeImageGet(
						$arFile,
						array("width" => 24, "height" => 24),
						BX_RESIZE_IMAGE_PROPORTIONAL,
						true
					);
					$arSKUPropList[$key]["VALUES"][$arFields["ID"]]["PICT"] = array(
						"SRC" => $arFileTmp["src"],
						"WIDTH" => $arFileTmp["width"],
						"HEIGHT" => $arFileTmp["height"],
					);
				} else {
					$arSKUPropList[$key]["VALUES"][$arFields["ID"]]["PICT"] = $arFile;
				}
			}
		}			
	}
}

$arResult["SKU_PROPS"] = $arSKUPropList;

//CURRENCIES//
$arResult["CURRENCIES"] = array();
if($arResult["MODULES"]["currency"]) {
	if($boolConvert) {
		$currencyFormat = CCurrencyLang::GetFormatDescription($arResult["CONVERT_CURRENCY"]["CURRENCY_ID"]);
		$arResult["CURRENCIES"] = array(
			array(
				"CURRENCY" => $arResult["CONVERT_CURRENCY"]["CURRENCY_ID"],
				"FORMAT" => array(
					"FORMAT_STRING" => $currencyFormat["FORMAT_STRING"],
					"DEC_POINT" => $currencyFormat["DEC_POINT"],
					"THOUSANDS_SEP" => $currencyFormat["THOUSANDS_SEP"],
					"DECIMALS" => $currencyFormat["DECIMALS"],
					"THOUSANDS_VARIANT" => $currencyFormat["THOUSANDS_VARIANT"],
					"HIDE_ZERO" => $currencyFormat["HIDE_ZERO"]
				)
			)
		);
		unset($currencyFormat);
	} else {
		$currencyIterator = CurrencyTable::getList(array(
			"select" => array("CURRENCY")
		));
		while($currency = $currencyIterator->fetch()) {
			$currencyFormat = CCurrencyLang::GetFormatDescription($currency["CURRENCY"]);
			$arResult["CURRENCIES"][] = array(
				"CURRENCY" => $currency["CURRENCY"],
				"FORMAT" => array(
					"FORMAT_STRING" => $currencyFormat["FORMAT_STRING"],
					"DEC_POINT" => $currencyFormat["DEC_POINT"],
					"THOUSANDS_SEP" => $currencyFormat["THOUSANDS_SEP"],
					"DECIMALS" => $currencyFormat["DECIMALS"],
					"THOUSANDS_VARIANT" => $currencyFormat["THOUSANDS_VARIANT"],
					"HIDE_ZERO" => $currencyFormat["HIDE_ZERO"]
				)
			);
		}
		unset($currencyFormat, $currency, $currencyIterator);
	}
}?>