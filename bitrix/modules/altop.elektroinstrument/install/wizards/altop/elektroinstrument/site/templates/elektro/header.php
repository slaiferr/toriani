<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui-1.8.16.custom.min.js"></script>

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.scrollUp.js"></script>

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.css" type="text/css" />

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jCarousel/jquery.jcarousel.min.js"></script>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/jCarousel/skin.css" type="text/css" />

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/custom-forms/jquery.custom-forms.js"></script>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/custom-forms/custom-forms.css" type="text/css" />

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/script.js"></script>

	<?if($APPLICATION->GetCurPage(true)== SITE_DIR."index.php") { ?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/anythingslider/slider.css" type="text/css" media="screen" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/anythingslider/jquery.easing.1.2.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/anythingslider/jquery.anythingslider.js"></script>

		 <script type="text/javascript">
			$(function () {
				$('.anythingSlider').anythingSlider({
					easing: "easeInOutExpo",
				});
			});
		</script>
	<?}?>

	<script type="text/javascript">
		function mycarousel_initCallback(carousel) {
			// Disable autoscrolling if the user clicks the prev or next button.
			carousel.buttonNext.bind('click', function() {
				carousel.startAuto(0);
			});

			carousel.buttonPrev.bind('click', function() {
				carousel.startAuto(0);
			});

			// Pause autoscrolling if the user moves with the cursor over the clip.
			carousel.clip.hover(function() {
				carousel.stopAuto();
			}, function() {
				carousel.startAuto();
			});
		};

		$(document).ready(function() {
			$('#mycarousel_2').jcarousel({
				auto:3,
				wrap:'last',
				scroll: 1,
				initCallback: mycarousel_initCallback
			});
		});
	</script>
	<?$APPLICATION->ShowHead();?>
	<script type="text/javascript">if (document.documentElement) { document.documentElement.id = "js" }</script>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>
	<div id="page-wrapper">
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		<div class="callback_body"></div>
		<div class="callback">
			<a href="#" class="callback_close"></a>
			<div class="h1"><?=GetMessage("ALTOP_CALL_BACK");?></div>
			<?$APPLICATION->IncludeComponent("altop:callback", "",
				Array(
					"EMAIL_TO" => "",
					"REQUIRED_FIELDS" => array("NAME","TEL","TIME")
				)
			);?>
		</div>
		<div class="center">
			<div id="header">
				<div id="header_1">
					<div id="logo">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_logo.php"), false);?>
					</div>
				</div>
				<div id="header_2">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/header_search.php"), false);?>
				</div>
				<div id="header_3">
					<div class="schedule">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule.php"), false);?>
					</div>
				</div>
				<div id="header_4">
					<div class="telephone">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/telephone.php"), false);?>
						<a class="callback_anch" href="#"><?=GetMessage("ALTOP_CALL_BACK")?></a>
					</div>
				</div>
				<div id="top-menu">
					<?$APPLICATION->IncludeComponent('bitrix:menu', "horizontal_multilevel", array(
						"ROOT_MENU_TYPE" => "top",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "86400",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(),
						"MAX_LEVEL" => "2",
						"CHILD_MENU_TYPE" => "topchild",
						"USE_EXT" => "N",
						"ALLOW_MULTI_SELECT" => "N"
						)
					);?>
				</div>
			</div>
			<div class="clr"></div>
			<div id="content-wrapper">
				<div id="content">
					<div id="left-column">
						<?if($APPLICATION->GetDirProperty("PERSONAL_SECTION")):?>
							<div class="h3"><?=GetMessage("PERSONAL_HEADER");?></div>
							<?$APPLICATION->IncludeComponent("altop:user", ".default", array(), false);?>
						<?else:?>
							<div class="h3"><?=GetMessage("BASE_HEADER");?></div>
						<?endif;?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "tree", array(
							"ROOT_MENU_TYPE" => "left",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "86400",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(),
							"MAX_LEVEL" => "4",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							),
							false
						);?>
						<ul id="new_leader_disc">
							<li>
								<a class="new" href="<?=SITE_DIR?>catalog/newproduct/"><?=GetMessage("CR_TITLE_NEWPRODUCT");?></a>
							</li>
							<li>
								<a class="saleleader" href="<?=SITE_DIR?>catalog/saleleader/"><?=GetMessage("CR_TITLE_SALELEADER");?></a>
							</li>
							<li>
								<a class="discount" href="<?=SITE_DIR?>catalog/discount/"><?=GetMessage("CR_TITLE_DISCOUNT");?></a>
							</li>
						</ul>
						<?if (strpos($APPLICATION->GetCurDir(), '/catalog/')===false) {?>
							<div id="banner_left">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/banner_left.php"), false);?>
							</div>
						<? } ?>
						<?if (strpos($APPLICATION->GetCurDir(), '/catalog/')!==false) {?>
							<div id="discount_left">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/discount_left.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									Array('HIDE_ICONS' => 'Y')
								);?>
							</div>
						<? } ?>
						<div id="vendors">
							<div class="h3"><?=GetMessage("MANUFACTURERS");?></div>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/vendors_left.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "html",
								),
								false,
								Array('HIDE_ICONS' => 'Y')
							);?>
						</div>
						<div id="subscribe">
							<div class="h3"><?=GetMessage("SUBSCRIBE");?></div>
							<p><?=GetMessage("SUBSCRIBE_TEXT");?></p>
							<?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "left", Array(
								"USE_PERSONALIZATION" => "Y",	
								"PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
								"SHOW_HIDDEN" => "N",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_NOTES" => ""
								),
								false
							);?>
						</div>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
							Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/stati_left.php",
								"AREA_FILE_RECURSIVE" => "N",
								"EDIT_MODE" => "html",
							),
							false,
							Array('HIDE_ICONS' => 'Y')
						);?>
					</div>
					<div id="workarea">
						<?if ($APPLICATION->GetCurPage(true)== SITE_DIR."index.php") { ?>
							<div id="banner_top">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."include/slider.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "html",
									),
									false,
									Array('HIDE_ICONS' => 'Y')
								);?>
							</div>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/news_home.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "html",
								),
								false,
								Array('HIDE_ICONS' => 'Y')
							);?>
							<div class="ndl_tabs">
								<div class="section">
									<ul class="tabs">
										<li class="new current">
											<img class="nohover" src="<?=SITE_TEMPLATE_PATH?>/images/new_grey.png"/>
											<img class="hover" src="<?=SITE_TEMPLATE_PATH?>/images/new_color.png"/>
											<span><?=GetMessage("CR_TITLE_NEWPRODUCT")?></span>
										</li>
										<li class="hit">
											<img class="nohover" src="<?=SITE_TEMPLATE_PATH?>/images/hit_grey.png"/>
											<img class="hover" src="<?=SITE_TEMPLATE_PATH?>/images/hit_color.png"/>
											<span><?=GetMessage("CR_TITLE_SALELEADER")?></span>
										</li>
										<li class="discount">
											<img class="nohover" src="<?=SITE_TEMPLATE_PATH?>/images/discount_grey.png"/>
											<img class="hover" src="<?=SITE_TEMPLATE_PATH?>/images/discount_color.png"/>
											<span><?=GetMessage("CR_TITLE_DISCOUNT")?></span>
										</li>
									</ul>
									<div class="new box visible">
										<div class="catalog-top">
											<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
												Array(
													"AREA_FILE_SHOW" => "file",
													"PATH" => SITE_DIR."include/newproduct.php",
													"AREA_FILE_RECURSIVE" => "N",
													"EDIT_MODE" => "html",
												),
												false,
												Array('HIDE_ICONS' => 'Y')
											);?>
											<a class="all" href="<?=SITE_DIR?>catalog/newproduct/"><?=GetMessage("CR_TITLE_ALL_NEWPRODUCT");?></a>
										</div>
									</div>
									<div class="hit box">
										<div class="catalog-top">
											<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
												Array(
													"AREA_FILE_SHOW" => "file",
													"PATH" => SITE_DIR."include/saleleader.php",
													"AREA_FILE_RECURSIVE" => "N",
													"EDIT_MODE" => "html",
												),
												false,
												Array('HIDE_ICONS' => 'Y')
											);?>
											<a class="all" href="<?=SITE_DIR?>catalog/saleleader/"><?=GetMessage("CR_TITLE_ALL_SALELEADER");?></a>
										</div>
									</div>
									<div class="discount box">
										<div class="catalog-top">
											<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
												Array(
													"AREA_FILE_SHOW" => "file",
													"PATH" => SITE_DIR."include/discount.php",
													"AREA_FILE_RECURSIVE" => "N",
													"EDIT_MODE" => "html",
												),
												false,
												Array('HIDE_ICONS' => 'Y')
											);?>
											<a class="all" href="<?=SITE_DIR?>catalog/discount/"><?=GetMessage("CR_TITLE_ALL_DISCOUNT");?></a>
										</div>
									</div>
								</div>
							</div>
							<div class="clr"></div>
						<? } ?>
						<div id="body_text" style="<?if ($APPLICATION->GetCurPage(true)== SITE_DIR."index.php"): echo 'padding:0px 15px;'; else: echo 'padding:0px;'; endif;?>">
							<?if ($APPLICATION->GetCurPage(true)!= SITE_DIR."index.php") {?>
								<div id="breadcrumb-search">
									<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", 
										array(
											"START_FROM" => "1",
											"PATH" => "",
											"SITE_ID" => "-"
										),
										false,
										Array('HIDE_ICONS' => 'Y')
									);?>
									<div id="podelitsya">
										<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
										<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,lj,odnoklassniki"></div> 
									</div>
								</div>
								<h1><?=$APPLICATION->ShowTitle(false);?></h1>
							<? } ?>		