<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гарантия и сервис");
?> 
<p>В нашем магазине мы продаем только сертифицированный товар с гарантией производителя.</p>

<p>Данный сайт является демо-версией готового интернет-магазина электроинструментов для 1С-Битрикс. Вся информация на сайте не является офертой, а служит лишь примером наполнения для ознакомления с возможностями решения.
  <br />

  <br />
Дополнительную информацию и контакты разработчика вы можете найти на сайте <a href="http://altop.ru" target="_blank" >ALTOP.RU</a></p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>