<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
if(defined("ERROR_404") && ERROR_404 == "Y" && $APPLICATION->GetCurPage(true) !='/404.php') LocalRedirect('/404.php');?>
							</div>
						</div>
						<?if($APPLICATION->GetCurPage(true)== SITE_DIR."index.php"):?>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/vendors_bottom.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "html",
								),
								false,
								Array('HIDE_ICONS' => 'Y')
							);?>
						<?endif;?>
						<?if(!CSite::InDir('/stati/')):?>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/stati_bottom.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "html",
								),
								false,
								Array('HIDE_ICONS' => 'Y')
							);?>
						<?endif;?>
					</div>
					<?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "bottom", 
						array(
							"USE_PERSONALIZATION" => "Y",	
							"PAGE" => "/personal/subscribe/subscr_edit.php",
							"SHOW_HIDDEN" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "86400",
							"CACHE_NOTES" => ""
						),
						false
					);?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/viewed_products.php"), false);?>
				</div>
				<div id="footer">
					<div id="footer_left">
						<div id="copyright">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright.php"), false);?>
						</div>
					</div>
					<div id="footer_center">
						<div id="footer-links">
							<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", 
								array(
									"ROOT_MENU_TYPE" => "bottom",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "86400",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "",
									"USE_EXT" => "N",
									"ALLOW_MULTI_SELECT" => "N"
								)
							);?>
						</div>
					</div>
					<div id="footer_right">
						<div id="counters">
							<div id="counter_1">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/counter_1.php"), false);?>
							</div>
							<div id="counter_2">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/counter_2.php"), false);?>
							</div>
						</div>
						<div id="footer-design">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/developer.php"), false);?>
						</div>
					</div>
					<div id="foot_panel">
						<div id="foot_panel_1">
							<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "login",
								Array(
									"REGISTER_URL" => SITE_DIR."personal/profile/",
									"FORGOT_PASSWORD_URL" => SITE_DIR."personal/profile/",
									"PROFILE_URL" => SITE_DIR."personal/profile/",
									"SHOW_ERRORS" => "N" 
								 )
							);?>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
								Array(
									"AREA_FILE_SHOW" => "file", 
									"PATH" => SITE_DIR."include/footer_compare.php"
								),
								false
							);?>
							<?$APPLICATION->IncludeComponent("altop:sale.basket.delay", ".default", 
								Array(
									"PATH_TO_DELAY" => SITE_DIR."personal/cart/?delay=Y",
								),
								false
							);?>
						</div>
						<div id="foot_panel_2">
							<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", 
								Array(
									"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
									"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
								),
								false
							);?>
						</div>
					</div>
				</div>
				<div id="foot_panel_poloska"></div>
				<div class="pop-up-bg" id="bgmod"></div>
				<div class="pop-up modal" id="addItemInCart">
					<div class="h1">
						<?=GetMessage("FOOTER_ADD_TO_BASKET")?>
					</div>
					<table>
						<tr>
							<td class="item_image_full"></td>
							<td class="item_title_desc">
								<div class="item_title"></div>
								<div class="item_desc"></div>
								<div class="item_props"></div>
							</td>
							<td class="item_count_full">
								<div class="item_count"></div>
								<?=GetMessage("FOOTER_COUNT")?>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="item_links">
								<button name="close" class="btn_buy ppp close" value="<?=GetMessage("FOOTER_CLOSE")?>"><?=GetMessage("FOOTER_CLOSE")?></button>					
								<form action="<?=SITE_DIR.'personal/cart/'?>" method="post">
									<button name="order" class="btn_buy popdef order" value="<?=GetMessage("FOOTER_ORDER")?>"><?=GetMessage("FOOTER_ORDER")?></button>
								</form>
							</td>
						</tr>
					</table>
					<div class="pop-up-close close button"></div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
</body>
</html>