<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(count($arResult) < 1)
	return;?>

<ul id="left-menu">
	<?$previousLevel = 0;	
	foreach($arResult as $key => $arItem):		
		if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):
			echo str_repeat("</div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
		endif;
		if($arItem["DEPTH_LEVEL"] == 1):
			if($arItem["IS_PARENT"]):?>
				<li id="id<?=$key?>" class="parent<?if($arItem["SELECTED"]):?> selected<?endif?>">
					<a href="<?=$arItem['LINK']?>"><?=$arItem["TEXT"]?><span class="arrow"></span></a>
					<div class="catalog-section-childs">
			<?else:?>
				<li id="id<?=$key?>"<?if($arItem["SELECTED"]):?> class="selected"<?endif?>>
					<a href="<?=$arItem['LINK']?>"><?=$arItem["TEXT"]?></a>
				</li>
			<?endif;
		elseif($arItem["DEPTH_LEVEL"] == 2):?>
			<div class="catalog-section-child">
				<a href="<?=$arItem['LINK']?>">
					<span class="child">
						<span class="image">
							<?if(is_array($arItem["PICTURE"])):?>
								<img src="<?=$arItem['PICTURE']['SRC']?>" width="<?=$arItem['PICTURE']['WIDTH']?>" height="<?=$arItem['PICTURE']['HEIGHT']?>" alt="<?=$arItem['TEXT']?>" />
							<?else:?>
								<img src="<?=SITE_TEMPLATE_PATH?>/images/no-photo.jpg" width="50" height="50" alt="<?=$arItem['TEXT']?>" />
							<?endif;?>
						</span>
						<span class="text"><?=$arItem["TEXT"]?></span>
					</span>
				</a>
			</div>
		<?else:
			continue;
		endif;
		$previousLevel = $arItem["DEPTH_LEVEL"];		
	endforeach;	
	if($previousLevel > 1):
		echo str_repeat("</div></li>", ($previousLevel-1));
	endif?>
</ul>

<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function(){
		$("ul#left-menu li.parent").hover(function() {
			var uid = $(this).attr("id"),
				pos = $(this).offset(),				
				top = pos.top - 5,
				left = pos.left + $(this).width() + 9;
            			
			eval("timeIn"+uid+" = setTimeout(function(){ $('#'+uid+' > .catalog-section-childs').show(15).css({'top': top + 'px', 'left': left + 'px'}); }, 200);");
            eval("clearTimeout(timeOut"+uid+")");        
		}, function(){
			var uid = $(this).attr("id");
            
			eval("clearTimeout(timeIn"+uid+")");
			eval("timeOut"+uid+" = setTimeout(function(){ $('#'+uid+' > .catalog-section-childs').hide(15); }, 200);");
        });
	});
	//]]>
</script>