$(function(){
	$(".close").live('click', function() {
		CloseModalWindow()
	});
	$(document).keyup(function(event){
		if (event.keyCode == 27) {
			CloseModalWindow()
		}
	});
});

function CentriredModalWindow(ModalName){
	$(ModalName).css({"display":"block","opacity":0});
	var modalH = $(ModalName).height();
	var modalW = $(ModalName).width();
}

function OpenModalWindow(ModalName){
	$(ModalName).animate({"opacity":1},300);	
	$("#bgmod").css("display","block");
}

function CloseModalWindow(){
	$("#bgmod").css("display","none");
	$(".modal").css({"opacity":1});
	$(".modal").animate({"opacity":0},300);
	setTimeout(function() { $(".modal").css({"display":"none"}); }, 500)	
}

function addToCompare(element) {
	if (!element || !element.href) 
		return;

	var href = element.href;
	var button = $(element);

	button.removeClass("catalog-item-compare").addClass("catalog-item-compared").unbind('click').removeAttr("href").css("cursor", "default");
	
	$.get(
		href + '&ajax_compare=1&backurl=' + decodeURIComponent(window.location.pathname),
		$.proxy(
			function(data) {
				$.post("/ajax/compare_line.php", function(data) {
					$("#compare").replaceWith(data);
				});
			}, button
		)
	);
    return false;
}

function addToDelay(id, qnt, btn) {
	$.ajax({
		type: "POST",
		url: "/ajax/add2delay.php",
		data: "id=" + id + "&qnt=" + qnt,
		success: function(html){
			$.post("/ajax/delay_line.php", function(data) {
				$("#delay").replaceWith(data);
			});
			$.post("/ajax/basket_line.php", function(data) {
				$("span#cart_line").replaceWith(data);
			});
			$("#" + btn).removeClass("catalog-item-delay").addClass("catalog-item-delayed").unbind('click').removeAttr("href").css("cursor", "default");
		}
	});
	return false;
}