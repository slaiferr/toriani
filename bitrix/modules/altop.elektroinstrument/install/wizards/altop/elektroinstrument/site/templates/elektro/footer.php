<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				<?$APPLICATION->IncludeComponent("bitrix:sale.viewed.product", "",
					Array(
						"VIEWED_COUNT" => "15",
						"VIEWED_NAME" => "Y",
						"VIEWED_IMAGE" => "Y",
						"VIEWED_PRICE" => "N",
						"VIEWED_CANBUY" => "N",
						"VIEWED_CANBUSKET" => "N",
						"VIEWED_IMG_HEIGHT" => "68",
						"VIEWED_IMG_WIDTH" => "68",
						"BASKET_URL" => "/personal/cart/",
						"ACTION_VARIABLE" => "action",
						"PRODUCT_ID_VARIABLE" => "id",
						"SET_TITLE" => "N"
					)
				);?>
				<div class="clr"></div>
			</div>
			<div id="footer">
				<div id="footer_left">
					<div id="copyright">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright.php"), false);?>
					</div>
				</div>
				<div id="footer_center">
					<div id="footer-links">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", 
							array(
								"ROOT_MENU_TYPE" => "bottom",
								"MAX_LEVEL" => "1",
							),
							false
						);?>
					</div>
				</div>
				<div id="footer_right">
					<div id="counters">
						<div id="counter_1">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/counter_1.php"), false);?>
						</div>
						<div id="counter_2">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/counter_2.php"), false);?>
						</div>
					</div>
					<div id="footer-design">
						<?=GetMessage("FOOTER_DISIGN")?>
					</div>
				</div>
				<div id="foot_panel">
					<div id="foot_panel_1">
						<div class="kabinet">
							<?if(!$USER->IsAuthorized()):?>
								<a class="login_anch" href="#"><?=GetMessage("LOGIN")?></a>
								<a class="register" href="<?=SITE_DIR?>personal/profile/?register=yes"><?=GetMessage("REGISTRATION")?></a>
							<?else:?>
								<a class="personal" href="<?=SITE_DIR?>personal/"><?=GetMessage("PERSONAL")?></a>
							<?endif;?>
						</div>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer_compare.php"), false);?>
						<?$APPLICATION->IncludeComponent("altop:sale.basket.delay", ".default", 
							Array(
								"PATH_TO_DELAY" => SITE_DIR."personal/cart/?delay=Y",
							),
							false
						);?>
					</div>
					<div id="foot_panel_2">
						<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", 
							Array(
								"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
								"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
							),
							false
						);?>
					</div>
					<div class="login_body"></div>
					<div class="login">
						<a href="#" class="login_close"></a>
						<img class="login_arrow" src="<?=SITE_TEMPLATE_PATH?>/images/login_arrow.png"/>
						<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "login",
							Array(
								"REGISTER_URL" => SITE_DIR."personal/profile/",
								"FORGOT_PASSWORD_URL" => SITE_DIR."personal/profile/",
								"PROFILE_URL" => SITE_DIR."personal/profile/",
								"SHOW_ERRORS" => "N" 
							 )
						);?>
					</div>
				</div>
			</div>
			<div id="foot_panel_poloska"></div>
			<div id="bgmod"></div>
			<div class="modal" id="addItemInCart">
				<div class="item_added">
					<?=GetMessage("FOOTER_ADD_TO_BASKET")?>
				</div>
				<div class="item_image_full">
					<div class="item_image_table">
						<img class="item_image" src=""/>
					</div>
				</div>
				<div class="item_title_desc">
					<div class="item_title"></div>
					<div class="item_desc"></div>
				</div>
				<div class="item_count_full">
					<div class="item_count_table">
						<div class="item_count"></div>
						<?=GetMessage("FOOTER_COUNT")?>
					</div>
				</div>
				<div class="item_links">
					<a href="javascript:void(0)" class="close"><?=GetMessage("FOOTER_CLOSE")?></a>
					<a href="<?=SITE_DIR."personal/cart/"?>" class="order"><?=GetMessage("FOOTER_ORDER")?></a>
				</div>
				<div class="close button"></div>
			</div>
		</div>
	</div>
</body>
</html>