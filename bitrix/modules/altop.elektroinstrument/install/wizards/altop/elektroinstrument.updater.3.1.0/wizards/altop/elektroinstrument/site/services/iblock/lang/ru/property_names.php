<?
$MESS["UF_BROWSER_TITLE"] = "Заголовок окна браузера";
$MESS["UF_KEYWORDS"] = "Ключевые слова";
$MESS["UF_META_DESCRIPTION"] = "Мета-описание";
$MESS["UF_SECTION_TITLE"] = "Заголовок раздела";
$MESS["UF_BANNER"] = "Баннер";
$MESS["UF_BANNER_URL"] = "URL баннера";
$MESS["UF_ADVANTAGES"] = "Преимущества магазина";
$MESS["UF_BACKGROUND_IMAGE"] = "Фон сайта";
$MESS["UF_PREVIEW"] = "Анонс";
$MESS["UF_VIEW"] = "Вид отображения";
?>