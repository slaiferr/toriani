<?
$MESS["WIZARD_STEP_0"] = "Мастер обновления сайта «Интернет-магазин электроинструмента ЭЛЕКТРОСИЛА»";
$MESS["WIZARD_STEP_0_DESCR"] = "Мастер в автоматическом режиме производит следующие операции:<br />1. Обновляет мастер создания сайта «Интернет-магазин электроинструмента ЭЛЕКТРОСИЛА» до последней версии 3.1.0;<br />2. Копирует файлы в публичную часть сайта (в папке /include/ будут заменены следующие файлы: company_logo.php, developer.php, discount.php, newproduct.php, saleleader.php);<br />3. Создает информационные блоки и свойства, необходимые для корректной работы сайта";
$MESS["WIZARD_STEP_INSTALL"] = "Выбор сайта";
$MESS["WIZARD_STEP_INSTALL_1"] = "Установить обновление для:";
$MESS["WIZARD_STEP_FINAL"] = "Обновление установлено";
$MESS["WIZARD_STEP_FINAL_DESCR"] = "Сайт «#SITE_NAME#» (#SITE_ID#) успешно обновлен.";
$MESS["WIZARD_STEP_CANCEL"] = "Работа мастера остановлена";
$MESS["WIZARD_STEP_CANCEL_DESCR"] = "Работа мастера была остановлена. Обновление не установлено.";
$MESS["WIZARD_CLOSE"] = "Закрыть";
$MESS["PROPERTY_BACKGROUND_IMAGE"] = "Фон сайта";
$MESS["UF_BACKGROUND_IMAGE"] = "Фон сайта";
$MESS["UF_PREVIEW"] = "Анонс";
$MESS["UF_VIEW"] = "Вид отображения";
$MESS["UF_VIEW_DEFAULT"] = "По умолчанию";
$MESS["UF_VIEW_TABLE"] = "Плитка";
$MESS["UF_VIEW_LIST"] = "Список";
$MESS["UF_VIEW_PRICE"] = "Прайс";
$MESS["CONTENT_TYPE_NAME"] = "Контент";
$MESS["CONTENT_SECTION_NAME"] = "Разделы";
$MESS["CONTENT_ELEMENT_NAME"] = "Элементы";
$MESS["FORMS_TYPE_NAME"] = "Формы";
$MESS["FORMS_SECTION_NAME"] = "Разделы";
$MESS["FORMS_ELEMENT_NAME"] = "Элементы";
$MESS["PROPERTY_LINKED"] = "Связанные товары";
?>