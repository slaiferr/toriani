<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

class Step0 extends CWizardStep {
	function InitStep() {
		$this->SetTitle(GetMessage("WIZARD_STEP_0"));
		$this->SetStepID("step0");		
		$this->SetNextStep("install");
		$this->SetCancelStep("cancel");
	}

	function ShowStep() {
		$this->content = GetMessage("WIZARD_STEP_0_DESCR");
	}
}

class Install extends CWizardStep {
	function InitStep() {
		$this->SetTitle(GetMessage("WIZARD_STEP_INSTALL"));
		$this->SetStepID("install");		
		$this->SetNextStep("final");
		$this->SetCancelStep("cancel");
		
		$dbSite = CSite::GetDefList();
		if($arSite = $dbSite->Fetch()) {			
			$wizard = &$this->GetWizard();
			$wizard->SetDefaultVars(
				array(
					"siteID" => $arSite["ID"],
					"siteDir" => $arSite["DIR"]
				)
			);
		}
	}

	function OnPostForm() {
		$wizard = &$this->GetWizard();		
		if($wizard->IsNextButtonClick()) {
			$path = $wizard->package->path;
			$arResult = $wizard->GetVars(true);
			
			if(COption::GetOptionString("elektroinstrument", "site_updated", "N", $arResult["siteID"]) == "Y")
				return;
			
			//SITE_DIR//
			$dbSite = CSite::GetList($b = "SORT", $o = "ASC", array("ID" => $arResult["siteID"]));
			if($arSite = $dbSite->Fetch())
				$arResult["siteDir"] = $arSite["DIR"];

			if(empty($arResult["siteDir"]))
				$arResult["siteDir"] = "/";

			//SITE_PATH//
			$sitePath = $_SERVER["DOCUMENT_ROOT"].$arResult["siteDir"];

			//REQUARE_WIZARD_UTILS//
			require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/wizard.php");
			require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/install/wizard_sol/utils.php");
			
			//COPY_WIZARD//
			if(file_exists($_SERVER["DOCUMENT_ROOT"].$path."/wizards/")) {
				if(CWizardUtil::DeleteWizard("altop:elektroinstrument"))
					CopyDirFiles(
						$_SERVER["DOCUMENT_ROOT"].$path."/wizards/",
						$_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/",
						$rewrite = true,
						$recursive = true,
						$delete_after_copy = true
					);
			}

			//COPY_FILES//
			if(file_exists($_SERVER["DOCUMENT_ROOT"].$path."/public/".LANGUAGE_ID."/")) {
				CopyDirFiles(
					$_SERVER["DOCUMENT_ROOT"].$path."/public/".LANGUAGE_ID."/",
					$sitePath,
					$rewrite = true,
					$recursive = true,
					$delete_after_copy = false
				);
			}
			
			//URLREWRITE//
			$arUrlRewrite = array();
			if(file_exists($_SERVER["DOCUMENT_ROOT"]."/urlrewrite.php")) {
				include($_SERVER["DOCUMENT_ROOT"]."/urlrewrite.php");
			}
			$arNewUrlRewrite = array(
				"CONDITION" => "#^".$arResult["siteDir"]."promotions/#",
				"RULE" => "",
				"ID" => "bitrix:news",
				"PATH" => $arResult["siteDir"]."promotions/index.php",
			);
			if(!in_array($arNewUrlRewrite, $arUrlRewrite))
				CUrlRewriter::Add($arNewUrlRewrite);
			
			if(!CModule::IncludeModule("iblock"))
				return;

			//CATALOG//
			$iblockCatalogID = false;
			$rsIblock = CIBlock::GetList(array(), array("CODE" => "catalog_".$arResult["siteID"], "XML_ID" => "catalog_".$arResult["siteID"], "TYPE" => "catalog"));
			if($arIblock = $rsIblock->Fetch())
				$iblockCatalogID = $arIblock["ID"];

			//OFFERS//
			$iblockOffersID = false;
			$rsIblock = CIBlock::GetList(array(), array("CODE" => "offers_".$arResult["siteID"], "XML_ID" => "offers_".$arResult["siteID"], "TYPE" => "catalog"));
			if($arIblock = $rsIblock->Fetch())
				$iblockOffersID = $arIblock["ID"];

			if($iblockCatalogID > 0) {
				//PROPERTY_BACKGROUND_IMAGE//
				$dbProperty = CIBlockProperty::GetList(array(), array("CODE" => "BACKGROUND_IMAGE", "IBLOCK_ID" => $iblockCatalogID));
				if(!$dbProperty->Fetch()) {
					$arFieldsAdd = array(
						"CODE" => "BACKGROUND_IMAGE",
						"IBLOCK_ID" => $iblockCatalogID,
						"NAME" => GetMessage("PROPERTY_BACKGROUND_IMAGE"),
						"ACTIVE" => "Y",
						"IS_REQUIRED" => "N",
						"SORT" => "1",
						"PROPERTY_TYPE" => "F",
						"MULTIPLE" => "N",
						"FILE_TYPE" => "jpg, gif, bmp, png, jpeg"
					);					
					$ibp = new CIBlockProperty;
					if(!$ibp->Add($arFieldsAdd))
						return;
				}

				//SECTION_USER_FIELDS//
				$oUserTypeEntity = new CUserTypeEntity();

				//UF_BACKGROUND_IMAGE//
				$dbRes = CUserTypeEntity::GetList(array(), array("ENTITY_ID" => "IBLOCK_".$iblockCatalogID."_SECTION", "FIELD_NAME" => "UF_BACKGROUND_IMAGE"));
				if(!$dbRes->Fetch()) {
					$aUserFields = array(
						"ENTITY_ID" => "IBLOCK_".$iblockCatalogID."_SECTION",
						"FIELD_NAME" => "UF_BACKGROUND_IMAGE",
						"USER_TYPE_ID" => "file",
						"XML_ID" => "UF_BACKGROUND_IMAGE",
						"SORT" => 100,
						"MULTIPLE" => "N",
						"MANDATORY" => "N",
						"SHOW_FILTER" => "N",
						"SHOW_IN_LIST" => "",
						"EDIT_IN_LIST" => "",
						"IS_SEARCHABLE" => "N",
						"SETTINGS" => array(
							"EXTENSIONS" => "jpg, gif, bmp, png, jpeg"
						),
						"EDIT_FORM_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_BACKGROUND_IMAGE")
						),
						"LIST_COLUMN_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_BACKGROUND_IMAGE")
						),
						"LIST_FILTER_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_BACKGROUND_IMAGE")
						)
					);
					if(!$oUserTypeEntity->Add($aUserFields))
						return;					
				}
				unset($aUserFields, $dbRes);
				
				//UF_PREVIEW//
				$dbRes = CUserTypeEntity::GetList(array(), array("ENTITY_ID" => "IBLOCK_".$iblockCatalogID."_SECTION", "FIELD_NAME" => "UF_PREVIEW"));
				if(!$dbRes->Fetch()) {
					$aUserFields = array(
						"ENTITY_ID" => "IBLOCK_".$iblockCatalogID."_SECTION",
						"FIELD_NAME" => "UF_PREVIEW",
						"USER_TYPE_ID" => "string",
						"XML_ID" => "UF_PREVIEW",
						"SORT" => 100,
						"MULTIPLE" => "N",
						"MANDATORY" => "N",
						"SHOW_FILTER" => "N",
						"SHOW_IN_LIST" => "",
						"EDIT_IN_LIST" => "",
						"IS_SEARCHABLE" => "N",
						"SETTINGS" => array(
							"SIZE" => "100",
							"ROWS" => "10"
						),
						"EDIT_FORM_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_PREVIEW")
						),
						"LIST_COLUMN_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_PREVIEW")
						),
						"LIST_FILTER_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_PREVIEW")
						)
					);
					if(!$oUserTypeEntity->Add($aUserFields))
						return;
				}
				unset($aUserFields, $dbRes);

				//UF_VIEW//
				$dbRes = CUserTypeEntity::GetList(array(), array("ENTITY_ID" => "IBLOCK_".$iblockCatalogID."_SECTION", "FIELD_NAME" => "UF_VIEW"));
				if(!$dbRes->Fetch()) {
					$aUserFields = array(
						"ENTITY_ID" => "IBLOCK_".$iblockCatalogID."_SECTION",
						"FIELD_NAME" => "UF_VIEW",
						"USER_TYPE_ID" => "enumeration",
						"XML_ID" => "UF_VIEW",
						"SORT" => 100,
						"MULTIPLE" => "N",
						"MANDATORY" => "N",
						"SHOW_FILTER" => "N",
						"SHOW_IN_LIST" => "",
						"EDIT_IN_LIST" => "",
						"IS_SEARCHABLE" => "N",
						"SETTINGS" => array(
							"DISPLAY" => "LIST",
							"LIST_HEIGHT" => "1",
							"CAPTION_NO_VALUE" => GetMessage("UF_VIEW_DEFAULT")
						),
						"EDIT_FORM_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_VIEW")
						),
						"LIST_COLUMN_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_VIEW")
						),
						"LIST_FILTER_LABEL" => array(
							LANGUAGE_ID => GetMessage("UF_VIEW")
						)
					);
					$iUserFieldId = $oUserTypeEntity->Add($aUserFields);
					if($iUserFieldId > 0) {				
						$obEnum = new CUserFieldEnum();
						$arAddEnum = array(
							"n1" => array(
								"XML_ID" => "table",
								"VALUE" => GetMessage("UF_VIEW_TABLE"),
								"DEF" => "Y",
								"SORT" => 1
							),
							"n2" => array(
								"XML_ID" => "list",
								"VALUE" => GetMessage("UF_VIEW_LIST"),
								"DEF" => "N",
								"SORT" => 2
							),
							"n3" => array(
								"XML_ID" => "price",
								"VALUE" => GetMessage("UF_VIEW_PRICE"),
								"DEF" => "N",
								"SORT" => 3
							)
						);
						if(!$obEnum->SetEnumValues($iUserFieldId, $arAddEnum))
							return;
					}
				}
				unset($arAddEnum, $iUserFieldId, $aUserFields, $dbRes);

				//REPLACE_MACROS//				
				CWizardUtil::ReplaceMacros($sitePath."include/discount.php", array("ITEMS_IBLOCK_ID" => $iblockCatalogID, "SITE_DIR" => $arResult["siteDir"]));				
				CWizardUtil::ReplaceMacros($sitePath."include/linked.php", array("ITEMS_IBLOCK_ID" => $iblockCatalogID, "SITE_DIR" => $arResult["siteDir"]));
				CWizardUtil::ReplaceMacros($sitePath."include/newproduct.php", array("ITEMS_IBLOCK_ID" => $iblockCatalogID, "SITE_DIR" => $arResult["siteDir"]));
				CWizardUtil::ReplaceMacros($sitePath."include/promotions_products.php", array("ITEMS_IBLOCK_ID" => $iblockCatalogID, "SITE_DIR" => $arResult["siteDir"]));
				CWizardUtil::ReplaceMacros($sitePath."include/recommend.php", array("ITEMS_IBLOCK_ID" => $iblockCatalogID, "SITE_DIR" => $arResult["siteDir"]));
				if($iblockOffersID > 0)
					CWizardUtil::ReplaceMacros($sitePath."include/recommend.php", array("OFFERS_IBLOCK_ID" => $iblockOffersID));
				CWizardUtil::ReplaceMacros($sitePath."include/saleleader.php", array("ITEMS_IBLOCK_ID" => $iblockCatalogID, "SITE_DIR" => $arResult["siteDir"]));				
				CWizardUtil::ReplaceMacros($sitePath."include/slider_left.php", array("ITEMS_IBLOCK_ID" => $iblockCatalogID, "SITE_DIR" => $arResult["siteDir"]));				
			}
			
			//IBLOCK_TYPES//
			$arTypes = array(
				array(
					"ID" => "content",
					"SECTIONS" => "Y",
					"IN_RSS" => "N",
					"SORT" => 20,
					"LANG" => array()
				),
				array(
					"ID" => "forms",
					"SECTIONS" => "Y",
					"IN_RSS" => "N",
					"SORT" => 30,
					"LANG" => array()
				)
			);
			
			GLOBAL $DB;
			$iblockType = new CIBlockType;	

			foreach($arTypes as $arType) {
				$dbType = CIBlockType::GetList(array(), array("=ID" => $arType["ID"]));				
				if($dbType->Fetch())
					continue;
				
				$code = strtoupper($arType["ID"]);
					
				$arType["LANG"][LANGUAGE_ID]["NAME"] = GetMessage($code."_TYPE_NAME");
				$arType["LANG"][LANGUAGE_ID]["ELEMENT_NAME"] = GetMessage($code."_ELEMENT_NAME");
				if($arType["SECTIONS"] == "Y")
					$arType["LANG"][LANGUAGE_ID]["SECTION_NAME"] = GetMessage($code."_SECTION_NAME");				
				
				$DB->StartTransaction();
					
				$res = $iblockType->Add($arType);				
				if(!$res) {
					$DB->Rollback();
					$this->SetError("Error: ".$obBlocktype->LAST_ERROR);
				} else
					$DB->Commit();							
			};
			
			//CALLBACK//
			$iblockXMLFile = $path."/xml/".LANGUAGE_ID."/callback.xml";
			$iblockCode = "callback_".$arResult["siteID"];
			$iblockType = "forms";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];

			if($iblockID == false) {
				$permissions = array(
					"1" => "X",
					"2" => "R"
				);

				$iblockID = WizardServices::ImportIBlockFromXML(
					$iblockXMLFile,
					$iblockCode,
					$iblockType,
					$arResult["siteID"],
					$permissions
				);

				if($iblockID < 1)
					return;

				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
				);
				$iblock->Update($iblockID, $arFields);

				//IBLOCK_PROPERTIES//
				$arProperty = array();
				$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
				while($arProp = $dbProperty->Fetch()) {
					$arProperty[$arProp["CODE"]] = $arProp["ID"];
				}

				//LIST_USER_OPTIONS//
				CUserOptions::SetOption("list", "tbl_iblock_element_".md5($iblockType.".".$iblockID), array(
					"columns" => "NAME, PROPERTY_".$arProperty["NAME"].", PROPERTY_".$arProperty["PHONE"].", PROPERTY_".$arProperty["TIME"].", PROPERTY_".$arProperty["MESSAGE"].", ACTIVE, SORT, TIMESTAMP_X, ID",
					"by" => "timestamp_x",
					"order" => "desc",
					"page_size" => "20"
				));
			} else {	
				$arSites = array(); 
				$db_res = CIBlock::GetSite($iblockID);
				while($res = $db_res->Fetch())
					$arSites[] = $res["LID"]; 
				if(!in_array($arResult["siteID"], $arSites)) {
					$arSites[] = $arResult["siteID"];
					$iblock = new CIBlock;
					$iblock->Update($iblockID, array("LID" => $arSites));
				}
			}
			
			//REPLACE_MACROS//
			CWizardUtil::ReplaceMacros($sitePath."include/form_callback.php", array("CALLBACK_IBLOCK_ID" => $iblockID));
			
			unset($iblockID, $iblockType, $iblockCode, $iblockXMLFile);

			//UNDER_ORDER//
			$iblockXMLFile = $path."/xml/".LANGUAGE_ID."/under_order.xml";
			$iblockCode = "under_order_".$arResult["siteID"];
			$iblockType = "forms";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];
			
			if($iblockID == false) {
				$permissions = array(
					"1" => "X",
					"2" => "R"
				);

				$iblockID = WizardServices::ImportIBlockFromXML(
					$iblockXMLFile,
					$iblockCode,
					$iblockType,
					$arResult["siteID"],
					$permissions
				);

				if($iblockID < 1)
					return;

				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
				);
				$iblock->Update($iblockID, $arFields);

				//IBLOCK_PROPERTIES//
				$arProperty = array();
				$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
				while($arProp = $dbProperty->Fetch()) {
					$arProperty[$arProp["CODE"]] = $arProp["ID"];
				}

				//LIST_USER_OPTIONS//
				CUserOptions::SetOption("list", "tbl_iblock_element_".md5($iblockType.".".$iblockID), array(
					"columns" => "NAME, PROPERTY_".$arProperty["PRODUCT"].", PROPERTY_".$arProperty["NAME"].", PROPERTY_".$arProperty["PHONE"].", PROPERTY_".$arProperty["TIME"].", PROPERTY_".$arProperty["MESSAGE"].", ACTIVE, SORT, TIMESTAMP_X, ID",
					"by" => "timestamp_x",
					"order" => "desc",
					"page_size" => "20"
				));
			} else {	
				$arSites = array(); 
				$db_res = CIBlock::GetSite($iblockID);
				while($res = $db_res->Fetch())
					$arSites[] = $res["LID"]; 
				if(!in_array($arResult["siteID"], $arSites)) {
					$arSites[] = $arResult["siteID"];
					$iblock = new CIBlock;
					$iblock->Update($iblockID, array("LID" => $arSites));
				}
			}

			//REPLACE_MACROS//
			CWizardUtil::ReplaceMacros($sitePath."include/form_under_order.php", array("UNDER_ORDER_IBLOCK_ID" => $iblockID));
			
			unset($iblockID, $iblockType, $iblockCode, $iblockXMLFile);
			
			//ASK_PRICE//
			$iblockXMLFile = $path."/xml/".LANGUAGE_ID."/ask_price.xml";
			$iblockCode = "ask_price_".$arResult["siteID"];
			$iblockType = "forms";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];
			
			if($iblockID == false) {
				$permissions = array(
					"1" => "X",
					"2" => "R"
				);

				$iblockID = WizardServices::ImportIBlockFromXML(
					$iblockXMLFile,
					$iblockCode,
					$iblockType,
					$arResult["siteID"],
					$permissions
				);

				if($iblockID < 1)
					return;

				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
				);
				$iblock->Update($iblockID, $arFields);

				//IBLOCK_PROPERTIES//
				$arProperty = array();
				$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
				while($arProp = $dbProperty->Fetch()) {
					$arProperty[$arProp["CODE"]] = $arProp["ID"];
				}

				//LIST_USER_OPTIONS//
				CUserOptions::SetOption("list", "tbl_iblock_element_".md5($iblockType.".".$iblockID), array(
					"columns" => "NAME, PROPERTY_".$arProperty["PRODUCT"].", PROPERTY_".$arProperty["NAME"].", PROPERTY_".$arProperty["PHONE"].", PROPERTY_".$arProperty["TIME"].", PROPERTY_".$arProperty["MESSAGE"].", ACTIVE, SORT, TIMESTAMP_X, ID",
					"by" => "timestamp_x",
					"order" => "desc",
					"page_size" => "20"
				));
			} else {
				$arSites = array();
				$db_res = CIBlock::GetSite($iblockID);
				while($res = $db_res->Fetch())
					$arSites[] = $res["LID"];
				if(!in_array($arResult["siteID"], $arSites)) {
					$arSites[] = $arResult["siteID"];
					$iblock = new CIBlock;
					$iblock->Update($iblockID, array("LID" => $arSites));
				}
			}

			//REPLACE_MACROS//
			CWizardUtil::ReplaceMacros($sitePath."include/form_ask_price.php", array("ASK_PRICE_IBLOCK_ID" => $iblockID));

			unset($iblockID, $iblockType, $iblockCode, $iblockXMLFile);

			//CHEAPER//
			$iblockXMLFile = $path."/xml/".LANGUAGE_ID."/cheaper.xml";
			$iblockCode = "cheaper_".$arResult["siteID"];
			$iblockType = "forms";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"]; 
			
			if($iblockID == false) {
				$permissions = array(
					"1" => "X",
					"2" => "R"
				);

				$iblockID = WizardServices::ImportIBlockFromXML(
					$iblockXMLFile,
					$iblockCode,
					$iblockType,
					$arResult["siteID"],
					$permissions
				);

				if($iblockID < 1)
					return;

				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
				);
				$iblock->Update($iblockID, $arFields);

				//IBLOCK_PROPERTIES//
				$arProperty = array();
				$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
				while($arProp = $dbProperty->Fetch()) {
					$arProperty[$arProp["CODE"]] = $arProp["ID"];
				}

				//LIST_USER_OPTIONS//
				CUserOptions::SetOption("list", "tbl_iblock_element_".md5($iblockType.".".$iblockID), array(
					"columns" => "NAME, PROPERTY_".$arProperty["PRODUCT"].", PROPERTY_".$arProperty["PRODUCT_PRICE"].", PROPERTY_".$arProperty["PRICE"].", PROPERTY_".$arProperty["LINK"].", PROPERTY_".$arProperty["NAME"].", PROPERTY_".$arProperty["PHONE"].", PROPERTY_".$arProperty["MESSAGE"].", ACTIVE, SORT, TIMESTAMP_X, ID",
					"by" => "timestamp_x",
					"order" => "desc",
					"page_size" => "20"
				));
			} else {	
				$arSites = array(); 
				$db_res = CIBlock::GetSite($iblockID);
				while($res = $db_res->Fetch())
					$arSites[] = $res["LID"]; 
				if(!in_array($arResult["siteID"], $arSites)) {
					$arSites[] = $arResult["siteID"];
					$iblock = new CIBlock;
					$iblock->Update($iblockID, array("LID" => $arSites));
				}
			}

			//REPLACE_MACROS//
			CWizardUtil::ReplaceMacros($sitePath."include/form_cheaper.php", array("CHEAPER_IBLOCK_ID" => $iblockID));

			unset($iblockID, $iblockType, $iblockCode, $iblockXMLFile);

			//BANNERS_LEFT//
			$iblockXMLFile = $path."/xml/".LANGUAGE_ID."/banners_left.xml";
			$iblockCode = "banners_left_".$arResult["siteID"];
			$iblockType = "content";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];
			
			if($iblockID == false) {
				$permissions = array(
					"1" => "X",
					"2" => "R"
				);

				$iblockID = WizardServices::ImportIBlockFromXML(
					$iblockXMLFile,
					$iblockCode,
					$iblockType,
					$arResult["siteID"],
					$permissions
				);

				if($iblockID < 1)
					return;

				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
				);
				$iblock->Update($iblockID, $arFields);

				//IBLOCK_PROPERTIES//
				$arProperty = array();
				$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
				while($arProp = $dbProperty->Fetch()) {
					$arProperty[$arProp["CODE"]] = $arProp["ID"];
				}

				//LIST_USER_OPTIONS//
				CUserOptions::SetOption("list", "tbl_iblock_element_".md5($iblockType.".".$iblockID), array(
					"columns" => "NAME, PREVIEW_PICTURE, PROPERTY_".$arProperty["URL"].", ACTIVE, SORT, TIMESTAMP_X, ID",
					"by" => "timestamp_x",
					"order" => "desc",
					"page_size" => "20"
				));
			} else {	
				$arSites = array(); 
				$db_res = CIBlock::GetSite($iblockID);
				while($res = $db_res->Fetch())
					$arSites[] = $res["LID"]; 
				if(!in_array($arResult["siteID"], $arSites)) {
					$arSites[] = $arResult["siteID"];
					$iblock = new CIBlock;
					$iblock->Update($iblockID, array("LID" => $arSites));
				}
			}

			//REPLACE_MACROS//
			CWizardUtil::ReplaceMacros($sitePath."include/banners_left.php", array("BANNERS_LEFT_IBLOCK_ID" => $iblockID));

			unset($iblockID, $iblockType, $iblockCode, $iblockXMLFile);

			//CONTACTS//
			$iblockXMLFile = $path."/xml/".LANGUAGE_ID."/contacts.xml";
			$iblockCode = "contacts_".$arResult["siteID"];
			$iblockType = "content";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];
			
			if($iblockID == false) {
				$arPermissions = array(
					"1" => "X",
					"2" => "R"
				);

				$iblockID = WizardServices::ImportIBlockFromXML(
					$iblockXMLFile,
					$iblockCode,
					$iblockType,
					$arResult["siteID"],
					$arPermissions
				);

				if($iblockID < 1)
					return;

				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode
				);
				$iblock->Update($iblockID, $arFields);

				//IBLOCK_PROPERTIES//
				$arProperty = array();
				$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
				while($arProp = $dbProperty->Fetch()) {
					$arProperty[$arProp["CODE"]] = $arProp["ID"];
				}

				//LIST_USER_OPTIONS//
				CUserOptions::SetOption("list", "tbl_iblock_element_".md5($iblockType.".".$iblockID), array(
					"columns" => "NAME, PROPERTY_".$arProperty["LOCATION"].", ACTIVE, SORT, TIMESTAMP_X, ID",
					"by" => "timestamp_x",
					"order" => "desc",
					"page_size" => "20"
				));
			} else {	
				$arSites = array(); 
				$db_res = CIBlock::GetSite($iblockID);
				while($res = $db_res->Fetch())
					$arSites[] = $res["LID"]; 
				if(!in_array($arResult["siteID"], $arSites)) {
					$arSites[] = $arResult["siteID"];
					$iblock = new CIBlock;
					$iblock->Update($iblockID, array("LID" => $arSites));
				}
			}

			//REPLACE_MACROS//
			CWizardUtil::ReplaceMacros($sitePath."include/geolocation.php", array("CONTACTS_IBLOCK_ID" => $iblockID));

			unset($iblockID, $iblockType, $iblockCode, $iblockXMLFile);

			//PROMOTIONS//
			$iblockXMLFile = $path."/xml/".LANGUAGE_ID."/promotions.xml";
			$iblockCode = "promotions_".$arResult["siteID"];
			$iblockType = "content";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];
			
			if($iblockID == false) {
				$permissions = array(
					"1" => "X",
					"2" => "R"
				);

				if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back")) {
					copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back", $_SERVER["DOCUMENT_ROOT"].$iblockXMLFile);
				}
				copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile, $_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back");
				
				CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile, array("PROMO_1_ACTIVE_TO" => date("Y-m-d H:i:s", time() + 86400 * 47), "PROMO_3_ACTIVE_TO" => date("Y-m-d H:i:s", time() + 86400 * 306), "PROMO_4_ACTIVE_TO" => date("Y-m-d H:i:s", time() + 86400 * 130)));
				
				$iblockID = WizardServices::ImportIBlockFromXML(
					$iblockXMLFile,
					$iblockCode,
					$iblockType,
					$arResult["siteID"],
					$permissions
				);

				if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back")) {
					copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back", $_SERVER["DOCUMENT_ROOT"].$iblockXMLFile);
				}

				if($iblockID < 1)
					return;

				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",		
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
					"FIELDS" => array(
						"PREVIEW_PICTURE" => array(
							"IS_REQUIRED" => "N",
							"DEFAULT_VALUE" => array(
								"FROM_DETAIL" => "Y",					
								"SCALE" => "Y",
								"WIDTH" => "480",
								"HEIGHT" => "255",
								"IGNORE_ERRORS" => "N",
								"METHOD" => "resample",
								"COMPRESSION" => 95,
								"DELETE_WITH_DETAIL" => "N",
								"UPDATE_WITH_DETAIL" => "N"
							)
						),
						"DETAIL_PICTURE" => array(
							"IS_REQUIRED" => "N",
							"DEFAULT_VALUE" => array(
								"SCALE" => "Y",
								"WIDTH" => "959",
								"HEIGHT" => "304",
								"IGNORE_ERRORS" => "N",
								"METHOD" => "resample",
								"COMPRESSION" => 95,
							)
						),
						"CODE" => array(
							"IS_REQUIRED" => "Y",
							"DEFAULT_VALUE" => array(
								"UNIQUE" => "Y",
								"TRANSLITERATION" => "Y",
								"TRANS_LEN" => 100,
								"TRANS_CASE" => "L",
								"TRANS_SPACE" => "-",
								"TRANS_OTHER" => "-",
								"TRANS_EAT" => "Y",
								"USE_GOOGLE" => "N"
							)
						)
					)
				);
				$iblock->Update($iblockID, $arFields);

				//IBLOCK_PROPERTIES//
				$arProp4Link = array(
					"SECTIONS" => "catalog",
					"BRANDS" => "vendors",
					"PRODUCTS" => "catalog"
				);

				$dbProp = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));
				while($arProp = $dbProp->Fetch()){
					if(!array_key_exists($arProp["CODE"], $arProp4Link)) continue;

					$rsIblock = CIBlock::GetList(array(), array("CODE" => $arProp4Link[$arProp["CODE"]]."_".WIZARD_SITE_ID, "XML_ID" => $arProp4Link[$arProp["CODE"]]."_".WIZARD_SITE_ID, "TYPE" => "catalog"));
					if($arIblock = $rsIblock->Fetch()){
						$arFieldsUpdate = array(
							"LINK_IBLOCK_ID" => $arIblock["ID"],
							"IBLOCK_ID" => $iblockID
						);

						$ibp = new CIBlockProperty;
						if(!$ibp->Update($arProp["ID"], $arFieldsUpdate))
							return;
					}
				}

				//LIST_USER_OPTIONS//
				CUserOptions::SetOption("list", "tbl_iblock_element_".md5($iblockType.".".$iblockID), array(
					"columns" => "NAME, PREVIEW_PICTURE, DETAIL_PICTURE, ACTIVE, SORT, TIMESTAMP_X, ID",
					"by" => "timestamp_x",
					"order" => "desc",
					"page_size" => "20"
				));
			} else {	
				$arSites = array(); 
				$db_res = CIBlock::GetSite($iblockID);
				while($res = $db_res->Fetch())
					$arSites[] = $res["LID"]; 
				if(!in_array($arResult["siteID"], $arSites)) {
					$arSites[] = $arResult["siteID"];
					$iblock = new CIBlock;
					$iblock->Update($iblockID, array("LID" => $arSites));
				}
			}

			//REPLACE_MACROS//
			CWizardUtil::ReplaceMacros($sitePath."include/promotions.php", array("PROMOTIONS_IBLOCK_ID" => $iblockID));
			CWizardUtil::ReplaceMacros($sitePath."promotions/index.php", array("PROMOTIONS_IBLOCK_ID" => $iblockID, "SITE_DIR" => $arResult["siteDir"]));

			unset($iblockID, $iblockType, $iblockCode, $iblockXMLFile);

			//NEWS//
			$iblockCode = "news_".$arResult["siteID"];
			$iblockType = "content";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];

			if($iblockID > 0) {
				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",		
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
					"FIELDS" => array(
						"PREVIEW_PICTURE" => array(
							"IS_REQUIRED" => "N",
							"DEFAULT_VALUE" => array(
								"FROM_DETAIL" => "Y",					
								"SCALE" => "Y",
								"WIDTH" => "208",
								"HEIGHT" => "140",
								"IGNORE_ERRORS" => "N",
								"METHOD" => "resample",
								"COMPRESSION" => 95,
								"DELETE_WITH_DETAIL" => "N",
								"UPDATE_WITH_DETAIL" => "N"
							)
						),
						"DETAIL_PICTURE" => array(
							"IS_REQUIRED" => "N",
							"DEFAULT_VALUE" => array(
								"SCALE" => "Y",
								"WIDTH" => "958",
								"HEIGHT" => "304",
								"IGNORE_ERRORS" => "N",
								"METHOD" => "resample",
								"COMPRESSION" => 95,
							)
						),
						"CODE" => array(
							"IS_REQUIRED" => "Y",
							"DEFAULT_VALUE" => array(
								"UNIQUE" => "Y",
								"TRANSLITERATION" => "Y",
								"TRANS_LEN" => 100,
								"TRANS_CASE" => "L",
								"TRANS_SPACE" => "-",
								"TRANS_OTHER" => "-",
								"TRANS_EAT" => "Y",
								"USE_GOOGLE" => "N"
							)
						)
					)
				);
				$iblock->Update($iblockID, $arFields);
				
				//PROPERTY_LINKED//
				$dbProperty = CIBlockProperty::GetList(array(), array("CODE" => "LINKED", "IBLOCK_ID" => $iblockID));
				if(!$dbProperty->Fetch()) {
					$arFieldsAdd = array(
						"CODE" => "LINKED",
						"IBLOCK_ID" => $iblockID,
						"NAME" => GetMessage("PROPERTY_LINKED"),
						"ACTIVE" => "Y",
						"IS_REQUIRED" => "N",
						"SORT" => "500",
						"PROPERTY_TYPE" => "E",
						"MULTIPLE" => "Y",
						"LINK_IBLOCK_ID" => $iblockCatalogID > 0 ? $iblockCatalogID : ""
					);
					$ibp = new CIBlockProperty;
					if(!$ibp->Add($arFieldsAdd))
						return;
				}

				//REPLACE_MACROS//
				CWizardUtil::ReplaceMacros($sitePath."include/news_bottom.php", array("NEWS_IBLOCK_ID" => $iblockID));
				CWizardUtil::ReplaceMacros($sitePath."include/news_left.php", array("NEWS_IBLOCK_ID" => $iblockID));
			}
			unset($arFieldsAdd, $dbProperty, $arFields, $iblockID, $iblockType, $iblockCode);
			
			//REVIEWS//
			$iblockCode = "stati_".$arResult["siteID"];
			$iblockType = "content";

			$iblockID = false;
			$rsIblock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
			if($arIblock = $rsIblock->Fetch())
				$iblockID = $arIblock["ID"];

			if($iblockID > 0) {
				//IBLOCK_FIELDS//
				$iblock = new CIBlock;
				$arFields = array(
					"ACTIVE" => "Y",		
					"CODE" => $iblockCode, 
					"XML_ID" => $iblockCode,
					"FIELDS" => array(
						"PREVIEW_PICTURE" => array(
							"IS_REQUIRED" => "N",
							"DEFAULT_VALUE" => array(
								"FROM_DETAIL" => "Y",					
								"SCALE" => "Y",
								"WIDTH" => "208",
								"HEIGHT" => "140",
								"IGNORE_ERRORS" => "N",
								"METHOD" => "resample",
								"COMPRESSION" => 95,
								"DELETE_WITH_DETAIL" => "N",
								"UPDATE_WITH_DETAIL" => "N"
							)
						),
						"DETAIL_PICTURE" => array(
							"IS_REQUIRED" => "N",
							"DEFAULT_VALUE" => array(
								"SCALE" => "Y",
								"WIDTH" => "958",
								"HEIGHT" => "304",
								"IGNORE_ERRORS" => "N",
								"METHOD" => "resample",
								"COMPRESSION" => 95,
							)
						),
						"CODE" => array(
							"IS_REQUIRED" => "Y",
							"DEFAULT_VALUE" => array(
								"UNIQUE" => "Y",
								"TRANSLITERATION" => "Y",
								"TRANS_LEN" => 100,
								"TRANS_CASE" => "L",
								"TRANS_SPACE" => "-",
								"TRANS_OTHER" => "-",
								"TRANS_EAT" => "Y",
								"USE_GOOGLE" => "N"
							)
						),
						"SECTION_PICTURE" => array(
							"IS_REQUIRED" => "N",
							"DEFAULT_VALUE" => array(
								"FROM_DETAIL" => "N",
								"SCALE" => "Y",
								"WIDTH" => "50",
								"HEIGHT" => "50",
								"IGNORE_ERRORS" => "N",
								"METHOD" => "resample",
								"COMPRESSION" => 95,
								"DELETE_WITH_DETAIL" => "N",
								"UPDATE_WITH_DETAIL" => "N",
							)
						),
						"SECTION_CODE" => array(
							"IS_REQUIRED" => "Y",
							"DEFAULT_VALUE" => array(
								"UNIQUE" => "Y",
								"TRANSLITERATION" => "Y",
								"TRANS_LEN" => 100,
								"TRANS_CASE" => "L",
								"TRANS_SPACE" => "-",
								"TRANS_OTHER" => "-",
								"TRANS_EAT" => "Y",
								"USE_GOOGLE" => "N",
							)
						)
					)
				);
				$iblock->Update($iblockID, $arFields);
				
				//PROPERTY_LINKED//
				$dbProperty = CIBlockProperty::GetList(array(), array("CODE" => "LINKED", "IBLOCK_ID" => $iblockID));
				if(!$dbProperty->Fetch()) {
					$arFieldsAdd = array(
						"CODE" => "LINKED",
						"IBLOCK_ID" => $iblockID,
						"NAME" => GetMessage("PROPERTY_LINKED"),
						"ACTIVE" => "Y",
						"IS_REQUIRED" => "N",
						"SORT" => "500",
						"PROPERTY_TYPE" => "E",
						"MULTIPLE" => "Y",
						"LINK_IBLOCK_ID" => $iblockCatalogID > 0 ? $iblockCatalogID : ""
					);					
					$ibp = new CIBlockProperty;
					if(!$ibp->Add($arFieldsAdd))
						return;
				}

				//REPLACE_MACROS//
				CWizardUtil::ReplaceMacros($sitePath."include/reviews_bottom.php", array("STATI_IBLOCK_ID" => $iblockID));
				CWizardUtil::ReplaceMacros($sitePath."include/reviews_left.php", array("STATI_IBLOCK_ID" => $iblockID));
			}
			unset($arFieldsAdd, $dbProperty, $arFields, $iblockID, $iblockType, $iblockCode);

			COption::SetOptionString("elektroinstrument", "site_updated", "Y", false, $arResult["siteID"]);
		}
	}
	
	function ShowStep() {
		$arSites = array();
		$dbSite = CSite::GetList($b = "SORT", $o = "ASC", array("ACTIVE" => "Y"));
		while($arSite = $dbSite->Fetch()) {
			$arSites[$arSite["ID"]] = $arSite["NAME"]." (".$arSite["ID"].")";
			if($arSite["DEF"] == "Y")
				$defSite = $arSite["ID"];
		}
		
		$this->content .= "<b>".GetMessage("WIZARD_STEP_INSTALL_1")."</b><br /><br />";
		$this->content .= $this->ShowSelectField("siteID", $arSites);
	}
}

class FinalStep extends CWizardStep {
	function InitStep() {
		$this->SetTitle(GetMessage("WIZARD_STEP_FINAL"));
		$this->SetStepID("final");
		$this->SetCancelCaption(GetMessage("WIZARD_CLOSE"));
		$this->SetCancelStep("final");
	}

	function ShowStep() {
		$wizard = &$this->GetWizard();
		$arResult = $wizard->GetVars(true);

		$arSite = CSite::GetList($b = "SORT", $o = "ASC", array("ID" => $arResult["siteID"]))->Fetch();
		
		$this->content .= GetMessage("WIZARD_STEP_FINAL_DESCR", array("#SITE_NAME#" => $arSite["NAME"], "#SITE_ID#" => $arSite["ID"]));
	}
}

class CancelStep extends CWizardStep {
	function InitStep() {
		$this->SetTitle(GetMessage("WIZARD_STEP_CANCEL"));
		$this->SetStepID("cancel");
		$this->SetCancelCaption(GetMessage("WIZARD_CLOSE"));
		$this->SetCancelStep("cancel");
	}

	function ShowStep() {
		$this->content .= GetMessage("WIZARD_STEP_CANCEL_DESCR");
	}
}?>