<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

use Bitrix\Main\Loader,
	Bitrix\Iblock,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\ModuleManager;
	
if(!Loader::includeModule("iblock"))
	return;

Loc::loadMessages(__FILE__); 

//CURRENT_VENDOR//
$arFilter = array(
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"ACTIVE" => "Y"
);
if(0 < intval($arResult["VARIABLES"]["ELEMENT_ID"])) {
	$arFilter["ID"] = $arResult["VARIABLES"]["ELEMENT_ID"];
} elseif("" != $arResult["VARIABLES"]["ELEMENT_CODE"]) {
	$arFilter["CODE"] = $arResult["VARIABLES"]["ELEMENT_CODE"];
}

$arSelect = array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT");

$cache_id = md5(serialize($arFilter));
$cache_dir = "/catalog/vendor";
$obCache = new CPHPCache();
if($obCache->InitCache($arParams["CACHE_TIME"], $cache_id, $cache_dir)) {
	$arCurVendor = $obCache->GetVars();	
} elseif($obCache->StartDataCache()) {
	$rsElement = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	global $CACHE_MANAGER;
	$CACHE_MANAGER->StartTagCache($cache_dir);
	$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
	if($arElement = $rsElement->GetNext()) {	
		$arCurVendor["ID"] = $arElement["ID"];
		$arCurVendor["NAME"] = $arElement["NAME"];
		if($arElement["PREVIEW_PICTURE"] > 0)
			$arCurVendor["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
		$arCurVendor["PREVIEW_TEXT"] = $arElement["PREVIEW_TEXT"];
		$ipropValues = new Iblock\InheritedProperty\ElementValues($arElement["IBLOCK_ID"], $arElement["ID"]);
		$arCurVendor["IPROPERTY_VALUES"] = $ipropValues->getValues();
		$CACHE_MANAGER->EndTagCache();
		$obCache->EndDataCache($arCurVendor);
	} else {
		$CACHE_MANAGER->abortTagCache();
		Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ? : GetMessage("T_NEWS_DETAIL_NF")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
	}
}

//ELEMENTS//
global $arVendorFilter;
$arVendorFilter = array(	
	"PROPERTY_MANUFACTURER" => $arCurVendor["ID"]
);?>
<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "sections",
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE_CATALOG"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID_CATALOG"],
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
		"PROPERTY_CODE" => "",
		"SET_META_KEYWORDS" => "N",		
		"SET_META_DESCRIPTION" => "N",		
		"SET_BROWSER_TITLE" => "N",		
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",		
		"SECTION_ID_VARIABLE" => "SECTION_ID",		
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"FILTER_NAME" => "arVendorFilter",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => "N",
		"MESSAGE_404" => "",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"FILE_404" => "",
		"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
		"PAGE_ELEMENT_COUNT" => "900",
		"LINE_ELEMENT_COUNT" => "",
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"USE_PRODUCT_QUANTITY" => "Y",
		"ADD_PROPERTIES_TO_BASKET" => "",
		"PARTIAL_PRODUCT_PROPERTIES" => "",
		"PRODUCT_PROPERTIES" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "",
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"USE_MAIN_ELEMENT_SECTION" => "Y",
		"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"],
		"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
		"ADD_SECTIONS_CHAIN" => "N",		
		"COMPARE_PATH" => "",
		"BACKGROUND_IMAGE" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "",
		"DISPLAY_IMG_WIDTH"	 =>	$arParams["DISPLAY_IMG_WIDTH"],
		"DISPLAY_IMG_HEIGHT" =>	$arParams["DISPLAY_IMG_HEIGHT"],
		"PROPERTY_CODE_MOD" => $arParams["PROPERTY_CODE_MOD"]
	),
	false,
	array("HIDE_ICONS" => "Y")
);?>

<?//DESCRIPTION//
if(!empty($arCurVendor["PREVIEW_TEXT"])):?>
	<div class="catalog_description">
		<?=$arCurVendor["PREVIEW_TEXT"];?>
	</div>
<?endif;

//BIGDATA_ITEMS//
$arRecomData = array();
$recomCacheID = array("IBLOCK_ID" => $arParams["IBLOCK_ID_CATALOG"]);
$obCache = new CPHPCache();
if($obCache->InitCache($arParams["CACHE_TIME"], serialize($recomCacheID), "/catalog/recommended")) {
	$arRecomData = $obCache->GetVars();	
} elseif($obCache->StartDataCache()) {
	if(Loader::includeModule("catalog")) {
		$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams["IBLOCK_ID_CATALOG"]);
		$arRecomData["OFFER_IBLOCK_ID"] = (!empty($arSKU) ? $arSKU["IBLOCK_ID"] : 0);
	}
	$obCache->EndDataCache($arRecomData);
}
if(!empty($arRecomData)):
	if(ModuleManager::isModuleInstalled("sale") && (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')):?>
		<?$APPLICATION->IncludeComponent("bitrix:catalog.bigdata.products", ".default", 
			array(
				"DISPLAY_IMG_WIDTH" => $arParams["DISPLAY_IMG_WIDTH"],
				"DISPLAY_IMG_HEIGHT" => $arParams["DISPLAY_IMG_HEIGHT"],
				"SHARPEN" => $arParams["SHARPEN"],
				"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
				"SHOW_POPUP" => "Y",
				"LINE_ELEMENT_COUNT" => "4",
				"TEMPLATE_THEME" => "",
				"DETAIL_URL" => "",
				"BASKET_URL" => "/personal/cart/",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PARTIAL_PRODUCT_PROPERTIES" => "",
				"SHOW_OLD_PRICE" => "",
				"SHOW_DISCOUNT_PERCENT" => "",
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"SHOW_PRICE_COUNT" => "1",
				"PRODUCT_SUBSCRIPTION" => "",
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"USE_PRODUCT_QUANTITY" => "Y",
				"SHOW_NAME" => "Y",
				"SHOW_IMAGE" => "Y",
				"MESS_BTN_BUY" => "",
				"MESS_BTN_DETAIL" => "",
				"MESS_BTN_SUBSCRIBE" => "",
				"MESS_NOT_AVAILABLE" => "",
				"PAGE_ELEMENT_COUNT" => "4",
				"SHOW_FROM_SECTION" => "N",
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE_CATALOG"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID_CATALOG"],
				"DEPTH" => "2",
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SHOW_PRODUCTS_".$arParams["IBLOCK_ID_CATALOG"] => "Y",
				"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID_CATALOG"] => "",
				"LABEL_PROP_".$arParams["IBLOCK_ID_CATALOG"] => "",
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
				"CURRENCY_ID" => $arParams["CURRENCY_ID"],
				"SECTION_ID" => "",
				"SECTION_CODE" => "",
				"SECTION_ELEMENT_ID" => "",
				"SECTION_ELEMENT_CODE" => "",
				"ID" => "",
				"PROPERTY_CODE_".$arParams["IBLOCK_ID_CATALOG"] => "",
				"PROPERTY_CODE_MOD" => $arParams["PROPERTY_CODE_MOD"],
				"CART_PROPERTIES_".$arParams["IBLOCK_ID_CATALOG"] => "",
				"RCM_TYPE" => $arParams["BIG_DATA_RCM_TYPE"],
				"OFFER_TREE_PROPS_".$arRecomData["OFFER_IBLOCK_ID"] => $arParams["OFFERS_PROPERTY_CODE"],
				"ADDITIONAL_PICT_PROP_".$arRecomData["OFFER_IBLOCK_ID"] => ""
			),
			false,
			array("HIDE_ICONS" => "Y")
		);?>
	<?endif;
endif;

//PAGE_TITLE//
$APPLICATION->SetTitle(!empty($arCurVendor["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) ? $arCurVendor["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] : $arCurVendor["NAME"]);
$APPLICATION->SetPageProperty("title", !empty($arCurVendor["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"]) ? $arCurVendor["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"] : $arCurVendor["NAME"]);
$APPLICATION->SetPageProperty("keywords", !empty($arCurVendor["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"]) ? $arCurVendor["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"] : "");
$APPLICATION->SetPageProperty("description", !empty($arCurVendor["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"]) ? $arCurVendor["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"] : "");

//META_PROPERTY//
$APPLICATION->AddHeadString("<meta property='og:title' content='".(!empty($arCurVendor["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) ? $arCurVendor["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] : $arCurVendor["NAME"])."' />", true);
if(!empty($arCurVendor["PREVIEW_TEXT"])):
	$APPLICATION->AddHeadString("<meta property='og:description' content='".strip_tags($arCurVendor["PREVIEW_TEXT"])."' />", true);
endif;
$APPLICATION->AddHeadString("<meta property='og:url' content='http://".SITE_SERVER_NAME.$APPLICATION->GetCurPage()."' />", true);
if(is_array($arCurVendor["PREVIEW_PICTURE"])):
	$APPLICATION->AddHeadString("<meta property='og:image' content='http://".SITE_SERVER_NAME.$arCurVendor["PREVIEW_PICTURE"]["SRC"]."' />", true);
	$APPLICATION->AddHeadString("<meta property='og:image:width' content='".$arCurVendor["PREVIEW_PICTURE"]["WIDTH"]."' />", true);
	$APPLICATION->AddHeadString("<meta property='og:image:height' content='".$arCurVendor["PREVIEW_PICTURE"]["HEIGHT"]."' />", true);
	$APPLICATION->AddHeadString("<link rel='image_src' href='http://".SITE_SERVER_NAME.$arCurVendor["PREVIEW_PICTURE"]["SRC"]."' />", true);
endif;

//BREADCRUMBS//
if($arParams["ADD_ELEMENT_CHAIN"] != "N"):
	$APPLICATION->AddChainItem(!empty($arCurVendor["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) ? $arCurVendor["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] : $arCurVendor["NAME"]);
endif;?>