<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Отзывы об итальянской мебели и компании Ториани");
$APPLICATION->SetPageProperty("keywords", "Отзывы об итальянской мебели, итальянская мебель отзывы");
$APPLICATION->SetTitle("Отзывы об итальянской мебели");

$APPLICATION->IncludeComponent(
    "lifenetwork:reviews",
    "",
    Array(
		'PAGE_LIMIT' => '10',
		'IBLOCK_ID' => '25',
		'SEF_FOLDER' => '/reviews/'
    )
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
