<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("ROBOTS", "index, follow");
$APPLICATION->SetPageProperty("keywords", "итальянская мебель в санкт-петербурге, салон итальянской мебели, элитная итальянская мебель, элитная мебель из италии, салон элитной мебели");
$APPLICATION->SetPageProperty("description", "Наш салон элитной мебели представлен такими мировыми брендами, как Ashley, Calligaris, Angelo Cappellini, Prama и т.д. Итальянская мебель в нашем интернет-магазине ...");
$APPLICATION->SetTitle("Итальянская мебель в Санкт-Петербурге. Салон элитной мебели из Италии и США");
?><h1>Салон элитной мебели из Италии и США</h1>
<p>
	 Компания Toriani специализируется на прямых поставках мебели и предметов интерьера из Италии. Все, что производится итальянскими мебельными фабриками, Вы можете приобрести по стоимости, указанной в европейских прайсах и с дополнительной скидкой до 20% при заказе от 100 000 рублей.
</p>
<h2>Итальянская мебель в Санкт-Петербурге</h2>
<p>
	 На сайте интернет-магазина Toriani в каталоге итальянской мебели представлена продукция самых востребованных фабрик, выпускающих элитную мебель как среднего ценового диапазона:</p>
<ul>
<li><a href="/vendors/calligaris/" title="Мебель Calligaris (Калигарис)">Calligaris</a></li>
<li><a href="/vendors/giorgio-casa/" title="Giorgio Сasa">Giorgio Сasa</a></li>
<li><a href="/vendors/tomasella/" title="Tomasella">Tomasella</a></li>
<li><a href="/vendors/seven-sedie/" title="Seven Sedie">Seven Sedie</a></li>
</ul>
<p>так и премиум сегмента:</p>
<ul>
<li><a href="/vendors/angelo-cappellini/" title="Мебель Angelo Cappellini">Angelo Cappellini</a></li>
<li><a href="/vendors/mascheroni/" title="Mascheroni">Mascheroni</a></li>
<li><a href="/vendors/poltrona-frau/" title="Мебель poltrona frau">Poltrona Frau</a></li>
<li><a href="/vendors/medea/" title="Мебель Medea">Medea</a></li>
</ul>
<p>Все это - компании мирового масштаба, давно заработавшие безупречную репутацию производителей высококачественной, оригинальной мебели на самый притязательный вкус!
</p>
<h3>
	 На нашем сайте и в нашем салоне элитной мебели вы найдете:</h3>
<ul>
<li>как мебель для дома (<a href="/catalog/spalni/" title="Спальни из Италии">спальни из Италии</a>, <a href="/catalog/divany/" title="Купить итальнский диван">итальянские диваны</a>, <a href="/catalog/obedennye-stoly/" title="Обеденные столы Италия">обеденные столы</a> и пр.),</li>
<li>так и мебель и детали интерьера для ресторана или другого статусного общественного заведения (<a href="/catalog/stulya-barnye/" title="Барные стулья Италия">барные стулья из Италии</a> и пр.)</li>
</ul>
<p>Итальянская мебель в нашем интернет-магазине – эталон красоты и изящества. А фабрики-производители, с которыми мы работаем, давно стали настоящими иконами стиля в мебельном бизнесе! С ними работают и другие мебельные салоны, однако именно мы собрали самую полную картину для вашего выбора!
</p>
<h2>Интерьерный салон Ториани - лучшая мебель по лучшим ценам!</h2>
<p>
	 Такие популярные коллекции как: <a href="/vendors/prama/" title="Prama Palazzo Ducale">Palazzo Ducale от Prama</a>, <a href="/vendors/camelgroup/" title="Мебель Camelgroup Nostalgia">Nostalgia от Camelgroup</a> или <a href="/vendors/maronese/" title="Afrodita Maronese">Afrodita от Maronese</a> имеются на складе. Если Вы проживаете в Санкт-Петербурге, то уже в течение 5 дней элитная итальянская мебель может украсить Вашу квартиру или загородный дом! А для жителей других регионов действует доставка транспортными компаниями.
</p>
 <br>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>