<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Моя корзина");?>

<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", ".default", array(
	"COLUMNS_LIST" => array(
		0 => "NAME",
		1 => "DISCOUNT",
		2 => "PROPS",
		3 => "DELETE",
		4 => "DELAY",
		5 => "PRICE",
		6 => "QUANTITY",
	),
	"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
	"HIDE_COUPON" => "N",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
	"USE_PREPAYMENT" => "N",
	"QUANTITY_FLOAT" => "N",
	"SET_TITLE" => "Y",
	"ACTION_VARIABLE" => "action",
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "3",
	"OFFERS_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_PROPERTY_CODE" => array(
		0 => "COLOR",
		1 => "PROP2",
		2 => "",
	),
	"OFFERS_SORT_FIELD" => "sort",
	"OFFERS_SORT_ORDER" => "asc",
	"OFFERS_SORT_FIELD2" => "id",
	"OFFERS_SORT_ORDER2" => "asc",
	"OFFERS_LIMIT" => "",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"PRICE_VAT_INCLUDE" => "Y",
	"CONVERT_CURRENCY" => "N",
	"OFFERS_CART_PROPERTIES" => array(
		0 => "COLOR",
		1 => "PROP2",
	)
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>