<? $this->setFrameMode(true); ?>
	<div class="row">
		<div class="col-xs-12">
			<form class="contact_form" method="post" action="/reviews/save/" name="contact_form">
				<ul>
					<li>
						<label for="name">Ваше имя:</label>
						<input type="text" name="PROPERTY[USER_NAME]" placeholder="Иванов Иван" required />
					</li>
					<li>
						<label for="message">Напишите ваш отзыв:</label>
						<textarea name="PROPERTY[MESSAGES]" cols="40" rows="6" required ></textarea>
					</li>
					<li>
						<label for="reviewPDA" style="float:none;width:inherit">
							<input style="width:inherit" class="private_data_agreement" type="checkbox" id="reviewPDA" name="reviewPDA"> Я согласен на <a href="/license/" target="_blank">обработку персональных данных</a>
						</label>
					</li>
					<li>
						<button class="submit" type="submit">Оставить отзыв</button>
					</li>
				</ul>
			</form>
		</div>
	</div>
	<div class="reviews">
		<div class="cd-testimonials-all-wrapper">
			<ul>
		<?php

			foreach ($arResult['REVIEWS'] as $review){
		
		?>
		
		<li class="cd-testimonials-item">
			<p><?=$review['PROP']['MESSAGES']['VALUE']?></p>
			
			<div class="cd-author">
				<img src="/local/components/lifenetwork/reviews/templates/.default/images/avatar-1.png" alt="Отзыв">
				<ul class="cd-author-info">
					<li><?=$review['PROP']['USER_NAME']['VALUE']?></li>
					<li><?=$review['PROP']['DATE']['VALUE']?></li>
				</ul>
			</div> <!-- cd-author -->
		</li>

			<? } ?>
			</ul>
		</div>   
    </div>

	<div class="pagination">
		<?= $arResult["NAV_STRING"] ?>
	</div>
	<div class="reviewsSlider">
	<h2>Отзывы об итальянской мебели</h2>
		<div class="comments_products">
		<? foreach ($arResult['REVIEWS_PROD'] as $item){ ?>
			<div class="slide">
				<div class="slideImg">
					<? if ($item['PROP']['PRODUCT_IMAGE']){ ?>
			    		<a href="<?=$item['PROP']['COMMENT_URL']?>"><img src="<?=$item['PROP']['PRODUCT_IMAGE']?>"></a>	
			    	<?}?>
				</div>
				<div class="slideText">
					<p class="slideTextUserName"><?=$item['PROP']['USER_NAME']?></p>
					<!-- <span><?=$item['PROP']['OBJECT_ID']?></span> -->
					<p class="slideTextUserText">
						<?
						$str = $item['FIELDS']['DETAIL_TEXT'];
						$strLength = iconv_strlen($str, 'UTF-8');
							if ($strLength > 250) {
								echo mb_strimwidth($str, 0, 250,'<a class="linkMore" href="'.$item['PROP']['COMMENT_URL'].'">Читать подробнее...</a>');
							} else {
								echo $str;
							}
						?>
					</p>
				</div>
	  		</div>
		<? }?>
		</div>
	</div>
<?	
	$APPLICATION->SetAdditionalCSS($templateFolder."/jquery.bxslider.css");
	$APPLICATION->addHeadScript($templateFolder."/jquery.bxslider.min.js");
?>