<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) {
    die();
}

	$arDefaultUrlTemplates404 = array(
		"index" => "index.php",
		"save" => "save/",
	);

	$arDefaultVariableAliases404 = array();
	$arDefaultVariableAliases = array();
	$arComponentVariables = array("IBLOCK_ID", "ELEMENT_ID");
	$arUrlTemplates = array();
	$arVariables = array();
 
 
	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);
	$componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"],$arUrlTemplates,$arVariables);
	
	if (StrLen($componentPage) < 0){
		header("Location: /reviews/"); 
	}

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
	
	$arResult = array();
	$arResult['IBLOCK_ID'] = $arVariables;

	$arResult['ALL_LIST_TEMLATE'] = $arParams['ALL_LIST_TEMLATE'] ?  $arParams['ALL_LIST_TEMLATE']: '.default';
	$arResult['SECTION_LIST_TEMLATE'] = $arParams['SECTION_LIST_TEMLATE'] ?  $arParams['SECTION_LIST_TEMLATE']: '.default';
	$arResult['DETAIL_TEMLATE'] = $arParams['DETAIL_TEMLATE'] ?  $arParams['DETAIL_TEMLATE']: '.default';
	$arResult['DETAIL_EDIT_TEMLATE'] = $arParams['DETAIL_EDIT_TEMLATE'] ?  $arParams['DETAIL_EDIT_TEMLATE']: '.default';
	
	CModule::IncludeModule('iblock');
	$el = new CIBlockElement();
	$arSelect = array ();
	$arFilter = array("IBLOCK_CODE" => 'REVIEWS', "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
	$res = $el->GetList(array("NAME" => "ASC"), $arFilter, false, array("nPageSize"=>$arParams['PAGE_LIMIT']), $arSelect);
	while ($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$properties = $ob->GetProperties();
		$arResult['REVIEWS'][$fields['ID']]['PROP'] = $properties;
	}

	$arResult['NAV_STRING'] =  $res->GetPageNavStringEx($navComponentObject,"");
	
	$arSelectProd = array ();
	$arFilterProd = array("IBLOCK_CODE" => 'comments_s1', "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
	$resProd = $el->GetList(array("NAME" => "ASC"), $arFilterProd, false, false, $arSelectProd);
	while ($obProd = $resProd->GetNextElement()){
		$fieldsProd = $obProd->GetFields();
		$propertiesProd = $obProd->GetProperties();
		$arResult['REVIEWS_PROD'][$fieldsProd['ID']]['PROP']['USER_NAME'] = $propertiesProd['USER_ID']['VALUE'];
		$arResult['REVIEWS_PROD'][$fieldsProd['ID']]['PROP']['COMMENT_URL'] = $propertiesProd['COMMENT_URL']['VALUE'];
		$arResult['REVIEWS_PROD'][$fieldsProd['ID']]['PROP']['OBJECT_ID'] = $propertiesProd['OBJECT_ID']['VALUE'];
    $resP = $el->GetByID($propertiesProd['OBJECT_ID']['VALUE']);
    if($arP = $resP->GetNextElement()){
		  $fProd = $arP->GetFields();
      $urlSrc =CFile::GetPath($fProd['PREVIEW_PICTURE']);
      }
      if($urlSrc){
        $arResult['REVIEWS_PROD'][$fieldsProd['ID']]['PROP']['PRODUCT_IMAGE'] = $urlSrc;
      }
		$arResult['REVIEWS_PROD'][$fieldsProd['ID']]['FIELDS'] = $fieldsProd;
	}
	
	if ($componentPage == 'save'){
		$_POST['PROPERTY']['DATE'] = date('Y.m.d h:i:s');
		$arFields = array(
		   "ACTIVE" => "N", 
		   "IBLOCK_ID" => $arParams['IBLOCK_ID'],
		   "NAME" => mb_substr($_POST['PROPERTY']['MESSAGES'], 0, 50, 'UTF-8'),
		   "PROPERTY_VALUES" => $_POST['PROPERTY'],
		);
		
		$id = $el->Add($arFields);
		if($id) {
			$arResult['SAVE_MSG']['STATUS'] =  'success';
			$arResult['SAVE_MSG']['MESSAGE'] =  'Отзыв успешно добавлен!';
		} else {
			$arResult['SAVE_MSG']['STATUS'] =  'danger';
			$arResult['SAVE_MSG']['MESSAGE'] =  $el->LAST_ERROR;
			$arResult['SAVE_MSG']['URL'] =  'javascript:history.back()';
		}
	}

	$this->IncludeComponentTemplate($componentPage);
?>