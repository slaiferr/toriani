<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) {
    die();
}

	$arDefaultUrlTemplates404 = array(
		"index" => "index.php",
		"section" => "#ELEMENT_ID#/",
	);

	$arDefaultVariableAliases404 = array();
	$arDefaultVariableAliases = array();
	$arComponentVariables = array("IBLOCK_ID", "ELEMENT_ID");
	$arUrlTemplates = array();
	$arVariables = array();
 
 
	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);
	$componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"],$arUrlTemplates,$arVariables);
	
	if (StrLen($componentPage) < 0){
		header("Location: /rss/"); 
	}

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
	
	$arResult = array();
	$arResult['IBLOCK_ID'] = $arVariables;

	$arResult['ALL_LIST_TEMLATE'] = $arParams['ALL_LIST_TEMLATE'] ?  $arParams['ALL_LIST_TEMLATE']: '.default';
	$arResult['SECTION_LIST_TEMLATE'] = $arParams['SECTION_LIST_TEMLATE'] ?  $arParams['SECTION_LIST_TEMLATE']: '.default';
	$arResult['DETAIL_TEMLATE'] = $arParams['DETAIL_TEMLATE'] ?  $arParams['DETAIL_TEMLATE']: '.default';
	$arResult['DETAIL_EDIT_TEMLATE'] = $arParams['DETAIL_EDIT_TEMLATE'] ?  $arParams['DETAIL_EDIT_TEMLATE']: '.default';
	
	CModule::IncludeModule('iblock');
	$el = new CIBlockElement();

	
	if ($componentPage == 'section'){
		$arResult['SECTION'] = $arResult['IBLOCK_ID']['ELEMENT_ID'];
		$componentPage = 'index';
	}
	
	if (!$arResult['SECTION']){
		$arResult['SECTION'] = 'news';
	}
	
	$arSelect = Array();
	$arFilter = Array(
		"IBLOCK_CODE" => strtoupper($arResult['SECTION']), 
		"ACTIVE" => "Y",
		"<=DATE_ACTIVE_FROM" => array(false, ConvertTimeStamp(false, "FULL")),
		">=DATE_ACTIVE_TO" => array(false, ConvertTimeStamp(false, "FULL"))
	);
	$res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, FALSE, array("nPageSize"=>$arParams['LIMIT_NEWS']), $arSelect);
	while ($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$properties = $ob->GetProperties();
		$arResult['RSS_ITEMS'][$fields['ID']]['FIELDS'] = $fields;
		$arResult['RSS_ITEMS'][$fields['ID']]['PROP'] = $properties;
	}
	
	$this->IncludeComponentTemplate($componentPage);
?>