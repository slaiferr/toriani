<?
	header('Content-Type: text/xml; charset=utf-8', true);
	$xml = new DOMDocument("1.0", "UTF-8");

	$rss = $xml->createElement("rss"); 
	$rss_node = $xml->appendChild($rss);
	$rss_node->setAttribute("version","2.0");
	$rss_node->setAttribute("xmlns:dc","http://purl.org/dc/elements/1.1/"); 
	$rss_node->setAttribute("xmlns:content","http://purl.org/rss/1.0/modules/content/");
	$rss_node->setAttribute("xmlns:atom","http://www.w3.org/2005/Atom");
	$rss_node->setAttribute("xmlns:image","http://purl.org/rss/1.0/modules/image/");
	$rss_node->setAttribute("xmlns:media","http://search.yahoo.com/mrss/");

	$date_f = date("D, d M Y H:i:s T", time());
	$build_date = gmdate(DATE_RFC2822, strtotime($date_f));

	$channel = $xml->createElement("channel");  
	$channel_node = $rss_node->appendChild($channel);
	 
	$channel_atom_link = $xml->createElement("atom:link");  
	$channel_atom_link->setAttribute("href",$arParams['SITE_URL']); //url of the feed
	$channel_atom_link->setAttribute("rel","self");
	$channel_atom_link->setAttribute("type","application/rss+xml");
	$channel_node->appendChild($channel_atom_link); 

	$channel_node->appendChild($xml->createElement("title", $arParams['SITE_TITLE'])); //title
	$channel_node->appendChild($xml->createElement("description", $arParams['SITE_TITLE']));  //description
	$channel_node->appendChild($xml->createElement("link", $arParams['SITE_URL'])); //website link 
	$channel_node->appendChild($xml->createElement("language", $arParams['LANG']));  //language
	$channel_node->appendChild($xml->createElement("lastBuildDate", $build_date));  //last build date
	$channel_node->appendChild($xml->createElement("generator", $arParams['GENERATOR'])); //generator

	if($arResult['RSS_ITEMS']){
		foreach($arResult['RSS_ITEMS'] as $item)
		{		
			$description = mb_substr($item['FIELDS']['PREVIEW_TEXT'],0,300, 'UTF-8'); //140 это кол. знаков

			$item_node = $channel_node->appendChild($xml->createElement("item"));
			$title_node = $item_node->appendChild($xml->createElement("title", $item['FIELDS']['NAME']));
			$link_node = $item_node->appendChild($xml->createElement("link", $arParams['SITE_URL'].'/'.$arResult['SECTION'].'/'. $item['FIELDS']['CODE'].'/'));

			$guid_link = $xml->createElement("guid", $arParams['SITE_URL'].'/'.$arResult['SECTION'].'/'. $item['FIELDS']['CODE']);  
			$guid_link->setAttribute("isPermaLink","false");
			$guid_node = $item_node->appendChild($guid_link); 
		
			$creator = $item_node->appendChild($xml->createElement("dc:creator", 'Admin')); 
			
			$image = $xml->createElement("enclosure");  
				$image->setAttribute("url",$arParams['SITE_URL'].CFile::GetPath($item['FIELDS']['PREVIEW_PICTURE']));
				$image->setAttribute("type","image/jpeg");
				$image->setAttribute("length","38587");
			$image_node = $item_node->appendChild($image); 

			$description_node = $item_node->appendChild($xml->createElement("description"));  

			$description_contents = $xml->createCDATASection($description.'...');  
			$description_node->appendChild($description_contents); 

			$date_rfc = gmdate(DATE_RFC2822, strtotime($item['FIELDS']['TIMESTAMP_X']));
			$pub_date = $xml->createElement("pubDate", $date_rfc);  
			$pub_date_node = $item_node->appendChild($pub_date); 
		}
	}
	echo $xml->saveXML();
?>