<?
set_time_limit(0); 
define("SITE_ID", "s1");
define("BX_UTF", true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_BUFFER_USED", true);
define('NO_AGENT_CHECK', true);
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../");
chdir($_SERVER["DOCUMENT_ROOT"]);

if ( strpos($_SERVER["DOCUMENT_ROOT"], '\\') !== false ) {
    $_SERVER["DOCUMENT_ROOT"] = str_replace('\\', '/', substr( $_SERVER["DOCUMENT_ROOT"], 2 ));
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

while (ob_get_level()) {
    ob_end_flush();
}

$APPLICATION->IncludeComponent(
    "lifenetwork:rss",
    "",
    Array(
        'FILTER_NAME' => 'arrFilter',
        "SORT_BY1" => "timestamp_x",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "DESC",
        "SEF_FOLDER" => '/rss/',
		'SITE_URL' => 'https://toriani.ru',
		'SITE_TITLE' => 'Итальянская мебель в Санкт-Петербурге. Салон элитной мебели из Италии и США',
		'DESCRIPTION' => 'Наш салон элитной мебели представлен такими мировыми брендами, как Ashley, Calligaris, Angelo Cappellini, Prama и т.д. Итальянская мебель в нашем интернет-магазине ...',
		'GENERATOR' => 'PHP DOMDocument',
		'LANG' => 'ru-ru',
		'LIMIT_NEWS' => '10',
    )
);

?>
